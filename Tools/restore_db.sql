/* Identify the list of the database & log files contained within the backup set */
USE MASTER
GO
RESTORE FILELISTONLY 
FROM DISK = N'E:\00_BACKUP\2011.11.01.10.56.18\PROD.BAK'
GO

--/* Restore the Full Backup with NORECOVERY option */
--RESTORE DATABASE prod
--FROM DISK = N'E:\00_BACKUP\2011.11.01.10.56.18\PROD.BAK'
--WITH NORECOVERY, FILE =1
--GO

RESTORE DATABASE [prod] 
FROM  DISK = N'E:\00_BACKUP\2011.11.01.10.56.18\PROD.BAK' 
WITH  FILE = 1,  
MOVE N'db' TO N'D:\SQL2008\prod.mdf',  
MOVE N'log' TO N'D:\SQL2008\prod_log.ldf',  
MOVE N'filestream' TO N'D:\SQL2008\prodfilestream',  
NOUNLOAD,  REPLACE,  STATS = 10
GO

--/*Restore Tail Log Backup with RECOVERY option */
--RESTORE DATABASE prod
--FROM DISK = N'E:\00_BACKUP\2011.11.01.10.56.18\PROD.TRN'
--WITH RECOVERY, FILE =1
--GO