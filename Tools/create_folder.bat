@echo OFF

:: Use date /t and time /t from command line to get format of your date and
:: time; change substring below as needed.

:: This will create a timestamp like yyyy-mm-yy-hh-mm-ss.
set TIMESTAMP=%DATE:~10,4%.%DATE:~4,2%.%DATE:~7,2%.%TIME:~0,2%.%TIME:~3,2%.%TIME:~6,2%

@echo TIMESTAMP=%TIMESTAMP%

:: Create new directory
md "%CD%\%TIMESTAMP%"