﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TheMovieDB;
using TheMovieDBTestApp.Properties;
using System.Diagnostics;

namespace TheMovieDBTestApp
{
    /// <summary>
    /// Interaction logic for PersonControl.xaml
    /// </summary>
    public partial class PersonControl : UserControl
    {
        private TmdbAPI api;
        public PersonControl()
        {
            InitializeComponent();

            api = new TmdbAPI(Settings.Default.ApiKey);
            api.PersonSearchCompleted += new TmdbAPI.PersonSearchAsyncCompletedEventHandler(api_PersonSearchCompleted);
            api.GetPersonInfoCompleted += new TmdbAPI.PersonInfoAsyncCompletedEventHandler(api_GetPersonInfoCompleted);

            personSearchButton.Click += new RoutedEventHandler(personSearchButton_Click);
            personSearchTextBox.KeyUp += new KeyEventHandler(personSearchTextBox_KeyUp);
            personSearchResultsListBox.SelectionChanged += new SelectionChangedEventHandler(personSearchResultsListBox_SelectionChanged);

            Loaded += new RoutedEventHandler(PersonControl_Loaded);
        }

        void api_GetPersonInfoCompleted(object sender, ImdbPersonInfoCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("There was an error searcing for movie: " + e.Error.Message, "Error");
            }
            else
            {
                personInfoGrid.DataContext = e.Person;
                personInfoGrid.Visibility = Visibility.Visible;
            }
            this.Cursor = Cursors.Arrow;
        }

        void api_PersonSearchCompleted(object sender, ImdbPersonSearchCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("There was an error searcing for movie: " + e.Error.Message, "Error");
            }
            else
            {
                personSearchResultsListBox.ItemsSource = e.People;
                personInfoGrid.DataContext = null;
                personInfoGrid.Visibility = Visibility.Collapsed;
            }
            this.Cursor = Cursors.Arrow;
        }

        void personSearchResultsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TmdbPerson person = personSearchResultsListBox.SelectedItem as TmdbPerson;
            if (person != null)
            {
                this.Cursor = Cursors.Wait;
                api.GetPersonInfoAsync(person.Id);                
            }
        }

        void PersonControl_Loaded(object sender, RoutedEventArgs e)
        {
            personSearchTextBox.Focus();
        }

        void personSearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                personSearchButton_Click(null, null);
        }

        void personSearchButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(personSearchTextBox.Text))
            {
                MessageBox.Show("Please enter a movie title to search.");
                return;
            }

            this.Cursor = Cursors.Wait;
            api.PersonSearchAsync(personSearchTextBox.Text);                        
        }

        private void TextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
                return;
            string url = tb.Tag.ToString();
            if (string.IsNullOrEmpty(url))
                return;
            try
            {
                Process.Start(url);
            }
            catch (Exception ex)
            {
                string mes = ex.Message;
            }
        }
    }
}
