﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using TheMovieDB;

namespace TheMovieDBTestApp.Code
{
    [ValueConversion(typeof(object), typeof(string))]
    public class ThumbnailConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            TmdbImage[] images = value as TmdbImage[];
            if (images == null)
                return null;
            TmdbImageType imageType = (TmdbImageType)Enum.Parse(typeof(TmdbImageType), parameter.ToString());
            TmdbImage image = images.FirstOrDefault(i => i.Type == imageType && i.Size == TmdbImageSize.thumb);
            if (image != null)
                return image.Url;
            else
                return null;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            // we don't intend this to ever be called
            return null;
        }
    }
}
