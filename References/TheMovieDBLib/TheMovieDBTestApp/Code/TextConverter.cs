﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace TheMovieDBTestApp.Code
{
    [ValueConversion(typeof(object), typeof(string))]
    public class TextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            string text = value.ToString();
            if (text.Length > 100)
                return text.Substring(0, 97) + "...";
            else
                return text;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            // we don't intend this to ever be called
            return null;
        }
    }
}
