﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using TheMovieDB;

namespace TheMovieDBTestApp.Code
{    
    [ValueConversion(typeof(object), typeof(string))]
    public class PersonConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            TmdbCastPerson person = value as TmdbCastPerson;
            if (person == null)
                return "";
            string p = string.Format("{0}: {1}", person.Job, person.Name);
            if (!string.IsNullOrEmpty(person.Character))
                p += " as " + person.Character;
            return p;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            // we don't intend this to ever be called
            return null;
        }
    }
}
