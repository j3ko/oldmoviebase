﻿This is a library to access movie and actor information from http://www.themoviedb.org/ as well as a sample application.

In order to use the library and the sample application, you will need a tmdb api key.  
Information on how to acquire an api key can be found at http://forums.themoviedb.org/topic/94/how-to-request-your-api-key/