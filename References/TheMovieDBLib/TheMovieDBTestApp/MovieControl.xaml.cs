﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TheMovieDBTestApp.Properties;
using TheMovieDB;
using System.Diagnostics;

namespace TheMovieDBTestApp
{
    /// <summary>
    /// Interaction logic for MovieControl.xaml
    /// </summary>
    public partial class MovieControl : UserControl
    {
        private TmdbAPI api;
        public MovieControl()
        {
            InitializeComponent();

            api = new TmdbAPI(Settings.Default.ApiKey);
            api.MovieSearchCompleted += new TmdbAPI.MovieSearchAsyncCompletedEventHandler(api_MovieSearchCompleted);
            api.MovieSearchByImdbCompleted += new TmdbAPI.MovieSearchAsyncCompletedEventHandler(api_MovieSearchCompleted);
            api.GetMovieInfoCompleted += new TmdbAPI.MovieInfoAsyncCompletedEventHandler(api_GetMovieInfoCompleted);

            movieSearchButton.Click += new RoutedEventHandler(movieSearchButton_Click);
            movieSearchTextBox.KeyUp += new KeyEventHandler(movieSearchTextBox_KeyUp);
            movieSearchResultsListBox.SelectionChanged += new SelectionChangedEventHandler(movieSearchResultsListBox_SelectionChanged);

            Loaded += new RoutedEventHandler(MovieControl_Loaded);
        }

        void api_GetMovieInfoCompleted(object sender, ImdbMovieInfoCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("There was an error searcing for movie: " + e.Error.Message, "Error");
            }
            else
            {
                api.GetMovieImages(e.Movie.ImdbId);
                movieInfoGrid.DataContext = e.Movie;
                movieInfoGrid.Visibility = Visibility.Visible;
            }
            this.Cursor = Cursors.Arrow;
        }

        void api_MovieSearchCompleted(object sender, ImdbMovieSearchCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("There was an error searcing for movie: " + e.Error.Message, "Error");
            }
            else
            {
                movieSearchResultsListBox.ItemsSource = e.Movies;
                movieInfoGrid.DataContext = null;
                movieInfoGrid.Visibility = Visibility.Collapsed;
            }
            this.Cursor = Cursors.Arrow;
        }

        void movieSearchResultsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TmdbMovie movie = movieSearchResultsListBox.SelectedItem as TmdbMovie;
            if (movie != null)
            {
                this.Cursor = Cursors.Wait;
                api.GetMovieInfoAsync(movie.Id);
                //movie = api.GetMovieInfo(movie.Id);
                //movieInfoGrid.DataContext = movie;
                //movieInfoGrid.Visibility = Visibility.Visible;
                //this.Cursor = Cursors.Arrow;
            }
        }

        void movieSearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                movieSearchButton_Click(null, null);
        }

        void MovieControl_Loaded(object sender, RoutedEventArgs e)
        {
            movieSearchTextBox.Focus();
        }

        void movieSearchButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(movieSearchTextBox.Text))
            {
                MessageBox.Show("Please enter a movie title to search.");
                return;
            }

            this.Cursor = Cursors.Wait;

            TmdbMovie[] movies = null;
            if (movieSearchTextBox.Text.StartsWith("tt"))
            {
                //movies = api.MovieSearchByImdb(movieSearchTextBox.Text);
                //movieSearchResultsListBox.ItemsSource = movies;
            }

            if(movies == null || movies.Length == 0)
            {
                api.MovieSearchAsync(movieSearchTextBox.Text);
                //movies = api.MovieSearch(movieSearchTextBox.Text);
                //movieSearchResultsListBox.ItemsSource = movies;
            }
            //this.Cursor = Cursors.Arrow;
        }

        private void TextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
                return;
            string url = tb.Tag.ToString();
            if (string.IsNullOrEmpty(url))
                return;
            try
            {
                Process.Start(url);
            }
            catch (Exception ex)
            {
                string mes = ex.Message;
            }
        }
    }
}
