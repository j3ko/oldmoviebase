﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Foundation
{
    public static class BootStrapper
    {
        public static string DefaultConnectionString
        {
            get
            {
                return Properties.Settings.Default.j3koConnectionString;
            }
        }
    }
}
