﻿IF NOT EXISTS
(
	SELECT 1 FROM [dbo].[schema_version]
		  WHERE [modified] = (SELECT MAX([modified]) FROM [dbo].[schema_version])
		  AND [version] >= 4
)
BEGIN
	PRINT 'Starting Version 4 Post-Deployment Script'
	INSERT [dbo].[schema_version]([version])VALUES(4)
	PRINT 'Done Version 4 Post-Deployment Script'
END
GO