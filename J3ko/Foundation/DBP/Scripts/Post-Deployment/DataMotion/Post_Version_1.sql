﻿IF NOT EXISTS
(
	SELECT 1 FROM [dbo].[schema_version]
		  WHERE [modified] = (SELECT MAX([modified]) FROM [dbo].[schema_version])
		  AND [version] >= 1
)
BEGIN
	PRINT 'Starting Version 1 Post-Deployment Script'
	INSERT [dbo].[schema_version]([version])VALUES(1)
	PRINT 'Done Version 1 Post-Deployment Script'
END
GO