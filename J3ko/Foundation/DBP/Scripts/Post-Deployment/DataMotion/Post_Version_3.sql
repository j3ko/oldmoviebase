IF NOT EXISTS
(
	SELECT 1 FROM [dbo].[schema_version]
		  WHERE [modified] = (SELECT MAX([modified]) FROM [dbo].[schema_version])
		  AND [version] >= 3
)
BEGIN
	PRINT 'Starting Version 3 Post-Deployment Script'
	INSERT [dbo].[schema_version]([version])VALUES(3)
	PRINT 'Done Version 3 Post-Deployment Script'
END
GO