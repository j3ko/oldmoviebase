﻿IF NOT EXISTS
(
	SELECT 1 FROM [dbo].[schema_version]
		  WHERE [modified] = (SELECT MAX([modified]) FROM [dbo].[schema_version])
		  AND [version] >= 2
)
BEGIN
	PRINT 'Starting Version 2 Post-Deployment Script'
	SET NOCOUNT ON

	IF EXISTS(
		SELECT * FROM [sys].[tables]
				WHERE object_id  = OBJECT_ID('[dbo].[v2xDATAMOTOxxxGENRE_TITLE]'))
	BEGIN
	BEGIN TRAN POSTDATAMOTO_TITLE_TABLE;       
	BEGIN TRY
		PRINT 'POPULATING [moviebase].[genre_title]'
		IF (SELECT COUNT(*) FROM [dbo].[v2xDATAMOTOxxxGENRE_TITLE]) > 0
		BEGIN
				INSERT [moviebase].[genre_title]
						([title_id],[genre_id])
					SELECT
						[title_id],[genre_id]
					FROM [dbo].[v2xDATAMOTOxxxGENRE_TITLE]
							ORDER BY [dbo].[v2xDATAMOTOxxxGENRE_TITLE].[title_id]
		END
		ELSE
		BEGIN
					PRINT 'WARNING!!!!!: SCHEMA IS AN UNEXPECTED STATE FOR THIS UPGRADE SCRIPT'
		END
		PRINT 'DROPPING [dbo].[v2xDATAMOTOxxxGENRE_TITLE]'           
		DROP TABLE [dbo].[v2xDATAMOTOxxxGENRE_TITLE]                 
		COMMIT TRAN POSTDATAMOTO_TITLE_TABLE;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN POSTDATAMOTO_TITLE_TABLE;
		PRINT 'POSTDATAMOTO_TITLE_TABLE TRANSACTION ROLLED BACK'     
		PRINT CAST(ERROR_NUMBER() AS CHAR(5)) + ERROR_MESSAGE()
	END CATCH
	END

	INSERT [dbo].[schema_version]([version])VALUES(2)
	SET NOCOUNT OFF
	PRINT 'Done Version 2 Post-Deployment Script'
END
GO