﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

PRINT 'STARTING POST-DEPLOYMENT SCRIPT'
-- REFERNCE DATA SYNC
:r .\RefDataSync\Sync.dbo.aspnet_SchemaVersions.sql
:r .\RefDataSync\Sync.dbo.aspnet_Applications.sql
:r .\RefDataSync\Sync.dbo.aspnet_Users.sql
:r .\RefDataSync\Sync.dbo.aspnet_Membership.sql
:r .\RefDataSync\Sync.moviebase.category.sql
:r .\RefDataSync\Sync.moviebase.container.sql
:r .\RefDataSync\Sync.moviebase.slot.sql
:r .\RefDataSync\Sync.moviebase.genre.sql
:r .\RefDataSync\Sync.moviebase.genre_tmdb.sql
:r .\RefDataSync\Sync.moviebase.instance.sql
:r .\RefDataSync\Sync.moviebase.media.sql
:r .\RefDataSync\Sync.moviebase.media_format.sql
:r .\RefDataSync\Sync.moviebase.title_role.sql

-- DATAMOTION
:r .\DataMotion\Post_Version_1.sql
:r .\DataMotion\Post_Version_2.sql
:r .\DataMotion\Post_Version_3.sql
:r .\DataMotion\Post_Version_4.sql

PRINT 'FINISHED POST-DEPLOYMENT SCRIPT'
