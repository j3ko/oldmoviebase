﻿------------------------------------------------------------------------
-- Description:	Reference data sync for: moviebase.media
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables without identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
--
-- Modification History:
-- 2011.09.20	devuser			Auto generated from template
------------------------------------------------------------------------

PRINT 'Starting [moviebase].[media] Syncronization'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
                  WHERE TABLE_NAME = N'xxxSYNCxxMEDIA')
      DROP TABLE [moviebase].[xxxSYNCxxMEDIA]
GO
CREATE TABLE [moviebase].[xxxSYNCxxMEDIA]
(media_id int, media_name nvarchar(255), media_format_id int)
GO
SET NOCOUNT ON
INSERT [moviebase].[xxxSYNCxxMEDIA](media_id, media_name, media_format_id)
            VALUES
				(N'1', N'Unknown (DVD-R)', N'1')
				,(N'2', N'Unknown (DVD+R)', N'2')
				,(N'3', N'Unknown (DVD-RW)', N'3')
				,(N'4', N'Unknown (DVD+RW)', N'4')
				,(N'5', N'Unknown (BD)', N'5')
SET NOCOUNT OFF
GO
ALTER TABLE moviebase.media NOCHECK CONSTRAINT ALL
SET IDENTITY_INSERT [moviebase].[media] ON
MERGE [moviebase].[media] AS Target
USING [moviebase].[xxxSYNCxxMEDIA] AS Source ON (Target.media_id = Source.media_id)
			WHEN MATCHED 
					AND (Target.media_name <> Source.media_name
					OR Target.media_format_id <> Source.media_format_id)
				THEN
					UPDATE
					SET 
					Target.media_name = Source.media_name
					,Target.media_format_id = Source.media_format_id

            WHEN NOT MATCHED BY TARGET 
                THEN
					INSERT      (media_id, media_name, media_format_id)
					VALUES      (media_id, media_name, media_format_id);
SET IDENTITY_INSERT [moviebase].[media] OFF
ALTER TABLE moviebase.media CHECK CONSTRAINT ALL
GO
DROP TABLE [moviebase].[xxxSYNCxxMEDIA];
GO
PRINT 'Done Syncronizing [moviebase].[media]'
GO
