﻿------------------------------------------------------------------------------
-- Description:	Reference data sync for: moviebase.title_role
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables w/o identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
-- 2011.09.28	Jeffrey Ko		SQL 2005 compatibility
-- 2011.09.29	Jeffrey Ko		Merged 2005/2008 scripts
-- 2012.05.31	Jeffrey Ko		Issue fixed for columns with SQL keywords
--
-- Modification History:
-- 2012.09.26	devuser			Generated from BABYLON\SQLEXPRESS [dev]
------------------------------------------------------------------------------

SET XACT_ABORT ON
PRINT 'Starting [moviebase].[title_role] Synchronization'
GO
BEGIN TRY
	BEGIN TRAN REFDATASYNC_TRANSACTION
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
					  WHERE TABLE_NAME = N'xxxSYNCxxTITLE_ROLE')
			BEGIN
				DROP TABLE [moviebase].[xxxSYNCxxTITLE_ROLE]
			END
	CREATE TABLE [moviebase].[xxxSYNCxxTITLE_ROLE]
	([title_role_id] int, [description] nvarchar(255))
	SET NOCOUNT ON

	INSERT [moviebase].[xxxSYNCxxTITLE_ROLE]
	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/

				([title_role_id], [description]) VALUES

				(N'1', N'Other')
				,(N'2', N'Actor')
				,(N'3', N'Director')
				,(N'4', N'Producer')

	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/
	SET NOCOUNT OFF
	ALTER TABLE moviebase.title_role NOCHECK CONSTRAINT ALL
	SET IDENTITY_INSERT [moviebase].[title_role] ON

	MERGE [moviebase].[title_role] AS Target
	USING [moviebase].[xxxSYNCxxTITLE_ROLE] AS Source ON (Target.[title_role_id] = Source.[title_role_id])
				WHEN MATCHED 
	AND ((Target.[description] <> Source.[description]
			OR (COALESCE(Target.description, Source.description) IS NOT NULL
			AND Target.description + Source.description IS NULL)))					THEN
						UPDATE
						SET 
						Target.[description] = Source.[description]

				WHEN NOT MATCHED BY TARGET 
					THEN
						INSERT      ([title_role_id], [description])
						VALUES      ([title_role_id], [description])

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/

				WHEN NOT MATCHED BY SOURCE 
					THEN
						DELETE

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/
	;
	
	SET IDENTITY_INSERT [moviebase].[title_role] OFF
	ALTER TABLE [moviebase].[title_role] CHECK CONSTRAINT ALL

	DROP TABLE [moviebase].[xxxSYNCxxTITLE_ROLE];

	COMMIT TRAN REFDATASYNC_TRANSACTION
	PRINT 'Done [moviebase].[title_role] Synchronization'
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN REFDATASYNC_TRANSACTION
			DECLARE @ERRORMSG VARCHAR(MAX)
			SET @ERRORMSG = ERROR_MESSAGE()
			RAISERROR(@ERRORMSG, 16, 1)
			PRINT 'Failed Synchronizing [moviebase].[title_role]'
		END
END CATCH
