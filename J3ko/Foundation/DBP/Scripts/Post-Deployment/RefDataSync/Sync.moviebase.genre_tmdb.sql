﻿------------------------------------------------------------------------------
-- Description:	Reference data sync for: moviebase.genre_tmdb
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables w/o identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
-- 2011.09.28	Jeffrey Ko		SQL 2005 compatibility
-- 2011.09.29	Jeffrey Ko		Merged 2005/2008 scripts
--
-- Modification History:
-- 2011.10.08	devuser			Generated from BABYLON\SQLEXPRESS [dev]
------------------------------------------------------------------------------

PRINT 'Starting [moviebase].[genre_tmdb] Synchronization'
GO
BEGIN TRY
	BEGIN TRAN REFDATASYNC_TRANSACTION
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
					  WHERE TABLE_NAME = N'xxxSYNCxxGENRE_TMDB')
			BEGIN
				DROP TABLE [moviebase].[xxxSYNCxxGENRE_TMDB]
			END
	CREATE TABLE [moviebase].[xxxSYNCxxGENRE_TMDB]
	(genre_id int, tmdb_id int)
	SET NOCOUNT ON

	INSERT [moviebase].[xxxSYNCxxGENRE_TMDB]
	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/

				(genre_id, tmdb_id) VALUES

				(N'2', N'28')
				,(N'2', N'10759')
				,(N'3', N'12')
				,(N'3', N'10759')
				,(N'4', N'16')
				,(N'6', N'35')
				,(N'7', N'50')
				,(N'8', N'99')
				,(N'9', N'18')
				,(N'10', N'10751')
				,(N'11', N'14')
				,(N'11', N'10765')
				,(N'12', N'10753')
				,(N'12', N'10754')
				,(N'14', N'36')
				,(N'15', N'27')
				,(N'16', N'10402')
				,(N'17', N'22')
				,(N'17', N'10402')
				,(N'18', N'9648')
				,(N'19', N'10763')
				,(N'20', N'10764')
				,(N'21', N'10749')
				,(N'22', N'878')
				,(N'22', N'10765')
				,(N'23', N'10755')
				,(N'24', N'9805')
				,(N'24', N'10757')
				,(N'24', N'10758')
				,(N'25', N'10767')
				,(N'26', N'53')
				,(N'26', N'10748')
				,(N'27', N'10752')
				,(N'27', N'10768')
				,(N'28', N'37')

	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/
	SET NOCOUNT OFF
	ALTER TABLE moviebase.genre_tmdb NOCHECK CONSTRAINT ALL

	MERGE [moviebase].[genre_tmdb] AS Target
	USING [moviebase].[xxxSYNCxxGENRE_TMDB] AS Source ON (Target.genre_id = Source.genre_id AND Target.tmdb_id = Source.tmdb_id)
				WHEN NOT MATCHED BY TARGET 
					THEN
						INSERT      (genre_id, tmdb_id)
						VALUES      (genre_id, tmdb_id)

				WHEN NOT MATCHED BY SOURCE 
					THEN
						DELETE;
	
	ALTER TABLE moviebase.genre_tmdb CHECK CONSTRAINT ALL

	DROP TABLE [moviebase].[xxxSYNCxxGENRE_TMDB];

	COMMIT TRAN REFDATASYNC_TRANSACTION
	PRINT 'Done [moviebase].[genre_tmdb] Synchronization'
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN REFDATASYNC_TRANSACTION
			DECLARE @ERRORMSG VARCHAR(MAX)
			SET @ERRORMSG = ERROR_MESSAGE()
			RAISERROR(@ERRORMSG, 16, 1)
			PRINT 'Failed Synchronizing [moviebase].[genre_tmdb]'
		END
END CATCH
