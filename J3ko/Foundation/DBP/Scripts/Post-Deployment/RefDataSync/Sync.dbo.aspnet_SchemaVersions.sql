﻿------------------------------------------------------------------------
-- Description:		Reference data sync script for: dbo.aspnet_SchemaVersions
-- Parameters:		
-- Template Modification History:
-- 2011-08-25	Jeffrey Ko		Initial version
-- 2011-09-15   Jeffrey Ko      Issue fixed for tables without identity columns
-- Modification History:
-- 15 Sep 2011	devuser		Auto generated from template
------------------------------------------------------------------------

PRINT 'Starting [dbo].[aspnet_SchemaVersions] Syncronization'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
                  WHERE TABLE_NAME = N'xxxSYNCxxASPNET_SCHEMAVERSIONS')
      DROP TABLE [dbo].[xxxSYNCxxASPNET_SCHEMAVERSIONS]
GO
CREATE TABLE [dbo].[xxxSYNCxxASPNET_SCHEMAVERSIONS]
(Feature nvarchar(128) not null, CompatibleSchemaVersion nvarchar(128) not null, IsCurrentVersion bit not null)
GO
SET NOCOUNT ON
INSERT [dbo].[xxxSYNCxxASPNET_SCHEMAVERSIONS](Feature, CompatibleSchemaVersion, IsCurrentVersion)
            VALUES
				(N'common', N'1', N'1')
				,(N'health monitoring', N'1', N'1')
				,(N'membership', N'1', N'1')
				,(N'personalization', N'1', N'1')
				,(N'profile', N'1', N'1')
				,(N'role manager', N'1', N'1')
SET NOCOUNT OFF
GO
MERGE [dbo].[aspnet_SchemaVersions] AS Target
USING [dbo].[xxxSYNCxxASPNET_SCHEMAVERSIONS] AS Source ON (Target.Feature = Source.Feature AND Target.CompatibleSchemaVersion = Source.CompatibleSchemaVersion)
			WHEN MATCHED 
					AND (Target.IsCurrentVersion <> Source.IsCurrentVersion)
				THEN
					UPDATE
					SET 
					Target.IsCurrentVersion = Source.IsCurrentVersion

            WHEN NOT MATCHED BY TARGET 
                THEN
					INSERT      (Feature, CompatibleSchemaVersion, IsCurrentVersion)
					VALUES      (Feature, CompatibleSchemaVersion, IsCurrentVersion)

            WHEN NOT MATCHED BY SOURCE 
				THEN
					DELETE;
GO
DROP TABLE [dbo].[xxxSYNCxxASPNET_SCHEMAVERSIONS];
GO
PRINT 'Done Syncronizing [dbo].[aspnet_SchemaVersions]'
GO
