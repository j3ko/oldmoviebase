﻿------------------------------------------------------------------------
-- Description:	Reference data sync for: dbo.aspnet_Users
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables without identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
--
-- Modification History:
-- 2011.09.15	devuser			Auto generated from template
-- 2011.09.15	Jeffrey Ko		Removed merge delete
------------------------------------------------------------------------

PRINT 'Starting [dbo].[aspnet_Users] Syncronization'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
                  WHERE TABLE_NAME = N'xxxSYNCxxASPNET_USERS')
      DROP TABLE [dbo].[xxxSYNCxxASPNET_USERS]
GO
CREATE TABLE [dbo].[xxxSYNCxxASPNET_USERS]
(ApplicationId uniqueidentifier, UserId uniqueidentifier, UserName nvarchar(256), LoweredUserName nvarchar(256), MobileAlias nvarchar(16), IsAnonymous bit, LastActivityDate datetime)
GO
SET NOCOUNT ON
INSERT [dbo].[xxxSYNCxxASPNET_USERS](ApplicationId, UserId, UserName, LoweredUserName, MobileAlias, IsAnonymous, LastActivityDate)
            VALUES
				(N'271A4678-5CB9-4094-A9DA-5991F0B66EED', N'33DEE93B-C58E-4D0E-9BE7-E2AA92FC1FCF', N'movieadmin', N'movieadmin', null, N'0', N'Sep 16 2011 12:54AM')
SET NOCOUNT OFF
GO
MERGE [dbo].[aspnet_Users] AS Target
USING [dbo].[xxxSYNCxxASPNET_USERS] AS Source ON (Target.UserId = Source.UserId)
			WHEN MATCHED 
					AND (Target.ApplicationId <> Source.ApplicationId
					OR Target.UserName <> Source.UserName
					OR Target.LoweredUserName <> Source.LoweredUserName
					OR Target.MobileAlias <> Source.MobileAlias
					OR Target.IsAnonymous <> Source.IsAnonymous
					OR Target.LastActivityDate <> Source.LastActivityDate)
				THEN
					UPDATE
					SET 
					Target.ApplicationId = Source.ApplicationId
					,Target.UserName = Source.UserName
					,Target.LoweredUserName = Source.LoweredUserName
					,Target.MobileAlias = Source.MobileAlias
					,Target.IsAnonymous = Source.IsAnonymous
					,Target.LastActivityDate = Source.LastActivityDate

            WHEN NOT MATCHED BY TARGET 
                THEN
					INSERT      (ApplicationId, UserId, UserName, LoweredUserName, MobileAlias, IsAnonymous, LastActivityDate)
					VALUES      (ApplicationId, UserId, UserName, LoweredUserName, MobileAlias, IsAnonymous, LastActivityDate);
GO
DROP TABLE [dbo].[xxxSYNCxxASPNET_USERS];
GO
PRINT 'Done Syncronizing [dbo].[aspnet_Users]'
GO
