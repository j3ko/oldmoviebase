﻿PRINT 'Starting [moviebase].[category] Syncronization'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
                  WHERE TABLE_NAME = N'xxxSyncxxCategory')
      DROP TABLE [moviebase].[xxxSyncxxCategory]
GO
CREATE TABLE [moviebase].[xxxSyncxxCategory]
      (Id int NOT NULL,
       Description NVARCHAR(255) NULL)
GO
SET NOCOUNT ON
INSERT [moviebase].[xxxSyncxxCategory](Id, Description)
            VALUES            
                (N'1', N'Movie')
               ,(N'2', N'TV Show')
               ,(N'3', N'Mini Series')
                             
SET NOCOUNT OFF
GO
SET IDENTITY_INSERT [moviebase].[category] ON
MERGE [moviebase].[category] AS Target
USING [moviebase].[xxxSyncxxCategory] AS Source ON (Target.category_id = Source.Id)
			WHEN MATCHED 
					AND Target.category_name <> Source.Description    
				THEN
					UPDATE
					SET Target.category_name = Source.Description

            WHEN NOT MATCHED BY TARGET 
                THEN
					INSERT      (category_id, category_name)
					VALUES      (Id, Description)

            WHEN NOT MATCHED BY SOURCE 
				THEN
					DELETE;
SET IDENTITY_INSERT [moviebase].[category] OFF
GO
DROP TABLE [moviebase].[xxxSyncxxCategory];
GO
PRINT 'Done Syncronizing [moviebase].[category]'
GO
