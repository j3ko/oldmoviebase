﻿------------------------------------------------------------------------
-- Description:	Reference data sync for: moviebase.media_format
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables without identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
--
-- Modification History:
-- 2011.09.20	devuser			Auto generated from template
------------------------------------------------------------------------

PRINT 'Starting [moviebase].[media_format] Syncronization'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
                  WHERE TABLE_NAME = N'xxxSYNCxxMEDIA_FORMAT')
      DROP TABLE [moviebase].[xxxSYNCxxMEDIA_FORMAT]
GO
CREATE TABLE [moviebase].[xxxSYNCxxMEDIA_FORMAT]
(media_format_id int, description nvarchar(50))
GO
SET NOCOUNT ON
INSERT [moviebase].[xxxSYNCxxMEDIA_FORMAT](media_format_id, description)
            VALUES
				(N'1', N'DVD-R')
				,(N'2', N'DVD+R')
				,(N'3', N'DVD-RW')
				,(N'4', N'DVD+RW')
				,(N'5', N'BD')
SET NOCOUNT OFF
GO
ALTER TABLE moviebase.media_format NOCHECK CONSTRAINT ALL
SET IDENTITY_INSERT [moviebase].[media_format] ON
MERGE [moviebase].[media_format] AS Target
USING [moviebase].[xxxSYNCxxMEDIA_FORMAT] AS Source ON (Target.media_format_id = Source.media_format_id)
			WHEN MATCHED 
					AND (Target.description <> Source.description)
				THEN
					UPDATE
					SET 
					Target.description = Source.description

            WHEN NOT MATCHED BY TARGET 
                THEN
					INSERT      (media_format_id, description)
					VALUES      (media_format_id, description)

            WHEN NOT MATCHED BY SOURCE 
				THEN
					DELETE;
SET IDENTITY_INSERT [moviebase].[media_format] OFF
ALTER TABLE moviebase.media_format CHECK CONSTRAINT ALL
GO
DROP TABLE [moviebase].[xxxSYNCxxMEDIA_FORMAT];
GO
PRINT 'Done Syncronizing [moviebase].[media_format]'
GO
