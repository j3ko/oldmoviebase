﻿PRINT 'Starting [moviebase].[instance] Syncronization'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
                  WHERE TABLE_NAME = N'xxxSYNCxxINSTANCE')
      DROP TABLE [moviebase].[xxxSYNCxxINSTANCE]
GO
CREATE TABLE [moviebase].[xxxSYNCxxINSTANCE]
(instance_id int not null, instance_desc nvarchar(255) not null)
GO
SET NOCOUNT ON
INSERT [moviebase].[xxxSYNCxxINSTANCE](instance_id, instance_desc)
            VALUES
				(N'1', N'Part 1')
				,(N'2', N'Part 2')
				,(N'3', N'Part 3')
				,(N'4', N'Part 4')
				,(N'5', N'Part 5')
				,(N'6', N'Part 6')
				,(N'7', N'Part 7')
				,(N'8', N'Part 8')
				,(N'9', N'Part 9')
				,(N'10', N'Part 10')
				,(N'11', N'Part 11')
				,(N'12', N'Part 12')
				,(N'13', N'Part 13')
				,(N'14', N'Part 14')
				,(N'15', N'Part 15')
				,(N'16', N'Part 16')
				,(N'17', N'Part 17')
				,(N'18', N'Part 18')
				,(N'19', N'Part 19')
				,(N'20', N'Part 20')
				,(N'21', N'Part 21')
				,(N'22', N'Part 22')
				,(N'23', N'Part 23')
				,(N'24', N'Part 24')
				,(N'25', N'Part 25')
				,(N'26', N'Part 26')
				,(N'27', N'Part 27')
				,(N'28', N'Part 28')
				,(N'29', N'Part 29')
				,(N'30', N'Part 30')
				,(N'31', N'Part 31')
				,(N'32', N'Part 32')
				,(N'33', N'Part 33')
				,(N'34', N'Part 34')
				,(N'35', N'Part 35')
				,(N'36', N'Part 36')
				,(N'37', N'Part 37')
				,(N'38', N'Part 38')
				,(N'39', N'Part 39')
				,(N'40', N'Part 40')
				,(N'41', N'Part 41')
				,(N'42', N'Part 42')
				,(N'43', N'Part 43')
				,(N'44', N'Part 44')
				,(N'45', N'Part 45')
				,(N'46', N'Part 46')
				,(N'47', N'Part 47')
				,(N'48', N'Part 48')
				,(N'49', N'Part 49')
				,(N'50', N'Part 50')
SET NOCOUNT OFF
GO
SET IDENTITY_INSERT [moviebase].[instance] ON
MERGE [moviebase].[instance] AS Target
USING [moviebase].[xxxSYNCxxINSTANCE] AS Source ON (Target.instance_id = Source.instance_id)
			WHEN MATCHED 
					AND (Target.instance_desc <> Source.instance_desc)
				THEN
					UPDATE
					SET 
					Target.instance_desc = Source.instance_desc

            WHEN NOT MATCHED BY TARGET 
                THEN
					INSERT      (instance_id, instance_desc)
					VALUES      (instance_id, instance_desc)

            WHEN NOT MATCHED BY SOURCE 
				THEN
					DELETE;
SET IDENTITY_INSERT [moviebase].[instance] OFF
GO
DROP TABLE [moviebase].[xxxSYNCxxINSTANCE];
GO
PRINT 'Done Syncronizing [moviebase].[instance]'
GO
