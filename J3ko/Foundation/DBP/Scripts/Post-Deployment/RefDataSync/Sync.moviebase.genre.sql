﻿------------------------------------------------------------------------------
-- Description:	Reference data sync for: moviebase.genre
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables w/o identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
-- 2011.09.28	Jeffrey Ko		SQL 2005 compatibility
-- 2011.09.29	Jeffrey Ko		Merged 2005/2008 scripts
--
-- Modification History:
-- 2011.10.07	devuser			Generated from BABYLON\SQLEXPRESS [dev]
------------------------------------------------------------------------------

PRINT 'Starting [moviebase].[genre] Synchronization'
GO
BEGIN TRY
	BEGIN TRAN REFDATASYNC_TRANSACTION
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
					  WHERE TABLE_NAME = N'xxxSYNCxxGENRE')
			BEGIN
				DROP TABLE [moviebase].[xxxSYNCxxGENRE]
			END
	CREATE TABLE [moviebase].[xxxSYNCxxGENRE]
	(genre_id int, genre_name nvarchar(255))
	SET NOCOUNT ON

	INSERT [moviebase].[xxxSYNCxxGENRE]
	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/

				(genre_id, genre_name) VALUES

				(N'1', N'None')
				,(N'2', N'Action')
				,(N'3', N'Adventure')
				,(N'4', N'Animation')
				,(N'5', N'Biography')
				,(N'6', N'Comedy')
				,(N'7', N'Crime')
				,(N'8', N'Documentary')
				,(N'9', N'Drama')
				,(N'10', N'Family')
				,(N'11', N'Fantasy')
				,(N'12', N'Film-Noir')
				,(N'13', N'Game Show')
				,(N'14', N'History')
				,(N'15', N'Horror')
				,(N'16', N'Music')
				,(N'17', N'Musical')
				,(N'18', N'Mystery')
				,(N'19', N'News')
				,(N'20', N'Reality')
				,(N'21', N'Romance')
				,(N'22', N'Sci-Fi')
				,(N'23', N'Shorts')
				,(N'24', N'Sports')
				,(N'25', N'Talk-Show')
				,(N'26', N'Thriller')
				,(N'27', N'War')
				,(N'28', N'Western')

	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/
	SET NOCOUNT OFF
	ALTER TABLE moviebase.genre NOCHECK CONSTRAINT ALL
	SET IDENTITY_INSERT [moviebase].[genre] ON

	MERGE [moviebase].[genre] AS Target
	USING [moviebase].[xxxSYNCxxGENRE] AS Source ON (Target.genre_id = Source.genre_id)
				WHEN MATCHED 
	AND ((Target.genre_name <> Source.genre_name
			OR (COALESCE(Target.genre_name, Source.genre_name) IS NOT NULL
			AND Target.genre_name + Source.genre_name IS NULL)))
					THEN
						UPDATE
						SET 
						Target.genre_name = Source.genre_name

				WHEN NOT MATCHED BY TARGET 
					THEN
						INSERT      (genre_id, genre_name)
						VALUES      (genre_id, genre_name)

				WHEN NOT MATCHED BY SOURCE 
					THEN
						DELETE;
	
	SET IDENTITY_INSERT [moviebase].[genre] OFF
	ALTER TABLE moviebase.genre CHECK CONSTRAINT ALL

	DROP TABLE [moviebase].[xxxSYNCxxGENRE];

	COMMIT TRAN REFDATASYNC_TRANSACTION
	PRINT 'Done [moviebase].[genre] Synchronization'
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN REFDATASYNC_TRANSACTION
			DECLARE @ERRORMSG VARCHAR(MAX)
			SET @ERRORMSG = ERROR_MESSAGE()
			RAISERROR(@ERRORMSG, 16, 1)
			PRINT 'Failed Synchronizing [moviebase].[genre]'
		END
END CATCH
