﻿------------------------------------------------------------------------
-- Description:	Reference data sync for: moviebase.container
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables without identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
--
-- Modification History:
-- 2011.09.20	devuser			Auto generated from template
------------------------------------------------------------------------

PRINT 'Starting [moviebase].[container] Syncronization'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
                  WHERE TABLE_NAME = N'xxxSYNCxxCONTAINER')
      DROP TABLE [moviebase].[xxxSYNCxxCONTAINER]
GO
CREATE TABLE [moviebase].[xxxSYNCxxCONTAINER]
(container_id int, container_name nvarchar(255))
GO
SET NOCOUNT ON
INSERT [moviebase].[xxxSYNCxxCONTAINER](container_id, container_name)
            VALUES
				(N'1', N'Big Box')
SET NOCOUNT OFF
GO
ALTER TABLE moviebase.container NOCHECK CONSTRAINT ALL
SET IDENTITY_INSERT [moviebase].[container] ON
MERGE [moviebase].[container] AS Target
USING [moviebase].[xxxSYNCxxCONTAINER] AS Source ON (Target.container_id = Source.container_id)
			WHEN MATCHED 
					AND (Target.container_name <> Source.container_name)
				THEN
					UPDATE
					SET 
					Target.container_name = Source.container_name

            WHEN NOT MATCHED BY TARGET 
                THEN
					INSERT      (container_id, container_name)
					VALUES      (container_id, container_name)

            WHEN NOT MATCHED BY SOURCE 
				THEN
					DELETE;
SET IDENTITY_INSERT [moviebase].[container] OFF
ALTER TABLE moviebase.container CHECK CONSTRAINT ALL
GO
DROP TABLE [moviebase].[xxxSYNCxxCONTAINER];
GO
PRINT 'Done Syncronizing [moviebase].[container]'
GO
