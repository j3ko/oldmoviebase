﻿------------------------------------------------------------------------
-- Description:	Reference data sync for: dbo.aspnet_Membership
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables without identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
--
-- Modification History:
-- 2011.09.15	devuser			Auto generated from template
-- 2011.09.15	Jeffrey Ko		Removed delete
------------------------------------------------------------------------

PRINT 'Starting [dbo].[aspnet_Membership] Syncronization'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
                  WHERE TABLE_NAME = N'xxxSYNCxxASPNET_MEMBERSHIP')
      DROP TABLE [dbo].[xxxSYNCxxASPNET_MEMBERSHIP]
GO
CREATE TABLE [dbo].[xxxSYNCxxASPNET_MEMBERSHIP]
(ApplicationId uniqueidentifier, UserId uniqueidentifier, Password nvarchar(128), PasswordFormat int, PasswordSalt nvarchar(128), MobilePIN nvarchar(16), Email nvarchar(256), LoweredEmail nvarchar(256), PasswordQuestion nvarchar(256), PasswordAnswer nvarchar(128), IsApproved bit, IsLockedOut bit, CreateDate datetime, LastLoginDate datetime, LastPasswordChangedDate datetime, LastLockoutDate datetime, FailedPasswordAttemptCount int, FailedPasswordAttemptWindowStart datetime, FailedPasswordAnswerAttemptCount int, FailedPasswordAnswerAttemptWindowStart datetime, Comment ntext)
GO
SET NOCOUNT ON
INSERT [dbo].[xxxSYNCxxASPNET_MEMBERSHIP](ApplicationId, UserId, Password, PasswordFormat, PasswordSalt, MobilePIN, Email, LoweredEmail, PasswordQuestion, PasswordAnswer, IsApproved, IsLockedOut, CreateDate, LastLoginDate, LastPasswordChangedDate, LastLockoutDate, FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart, FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart, Comment)
            VALUES
				(N'271A4678-5CB9-4094-A9DA-5991F0B66EED', N'33DEE93B-C58E-4D0E-9BE7-E2AA92FC1FCF', N'I1K3moUy7Z9kAc0hlN3Aa3D+Spw=', N'1', N'zOdZa7uUmtDpqh1ZUvFBQw==', null, N'admin@j3ko.com', N'admin@j3ko.com', null, null, N'1', N'0', N'Sep 15 2011  5:27AM', N'Sep 16 2011 12:54AM', N'Sep 15 2011  5:27AM', N'Jan  1 1754 12:00AM', N'0', N'Jan  1 1754 12:00AM', N'0', N'Jan  1 1754 12:00AM', null)
SET NOCOUNT OFF
GO
MERGE [dbo].[aspnet_Membership] AS Target
USING [dbo].[xxxSYNCxxASPNET_MEMBERSHIP] AS Source ON (Target.UserId = Source.UserId)
			WHEN MATCHED 
					AND (Target.ApplicationId <> Source.ApplicationId
					OR Target.UserId <> Source.UserId
					OR Target.Password <> Source.Password
					OR Target.PasswordFormat <> Source.PasswordFormat
					OR Target.PasswordSalt <> Source.PasswordSalt
					OR Target.MobilePIN <> Source.MobilePIN
					OR Target.Email <> Source.Email
					OR Target.LoweredEmail <> Source.LoweredEmail
					OR Target.PasswordQuestion <> Source.PasswordQuestion
					OR Target.PasswordAnswer <> Source.PasswordAnswer
					OR Target.IsApproved <> Source.IsApproved
					OR Target.IsLockedOut <> Source.IsLockedOut
					OR Target.CreateDate <> Source.CreateDate
					OR Target.LastLoginDate <> Source.LastLoginDate
					OR Target.LastPasswordChangedDate <> Source.LastPasswordChangedDate
					OR Target.LastLockoutDate <> Source.LastLockoutDate
					OR Target.FailedPasswordAttemptCount <> Source.FailedPasswordAttemptCount
					OR Target.FailedPasswordAttemptWindowStart <> Source.FailedPasswordAttemptWindowStart
					OR Target.FailedPasswordAnswerAttemptCount <> Source.FailedPasswordAnswerAttemptCount
					OR Target.FailedPasswordAnswerAttemptWindowStart <> Source.FailedPasswordAnswerAttemptWindowStart)
				THEN
					UPDATE
					SET 
					Target.ApplicationId = Source.ApplicationId
					,Target.UserId = Source.UserId
					,Target.Password = Source.Password
					,Target.PasswordFormat = Source.PasswordFormat
					,Target.PasswordSalt = Source.PasswordSalt
					,Target.MobilePIN = Source.MobilePIN
					,Target.Email = Source.Email
					,Target.LoweredEmail = Source.LoweredEmail
					,Target.PasswordQuestion = Source.PasswordQuestion
					,Target.PasswordAnswer = Source.PasswordAnswer
					,Target.IsApproved = Source.IsApproved
					,Target.IsLockedOut = Source.IsLockedOut
					,Target.CreateDate = Source.CreateDate
					,Target.LastLoginDate = Source.LastLoginDate
					,Target.LastPasswordChangedDate = Source.LastPasswordChangedDate
					,Target.LastLockoutDate = Source.LastLockoutDate
					,Target.FailedPasswordAttemptCount = Source.FailedPasswordAttemptCount
					,Target.FailedPasswordAttemptWindowStart = Source.FailedPasswordAttemptWindowStart
					,Target.FailedPasswordAnswerAttemptCount = Source.FailedPasswordAnswerAttemptCount
					,Target.FailedPasswordAnswerAttemptWindowStart = Source.FailedPasswordAnswerAttemptWindowStart
					,Target.Comment = Source.Comment

            WHEN NOT MATCHED BY TARGET 
                THEN
					INSERT      (ApplicationId, UserId, Password, PasswordFormat, PasswordSalt, MobilePIN, Email, LoweredEmail, PasswordQuestion, PasswordAnswer, IsApproved, IsLockedOut, CreateDate, LastLoginDate, LastPasswordChangedDate, LastLockoutDate, FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart, FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart, Comment)
					VALUES      (ApplicationId, UserId, Password, PasswordFormat, PasswordSalt, MobilePIN, Email, LoweredEmail, PasswordQuestion, PasswordAnswer, IsApproved, IsLockedOut, CreateDate, LastLoginDate, LastPasswordChangedDate, LastLockoutDate, FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart, FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart, Comment);
GO
DROP TABLE [dbo].[xxxSYNCxxASPNET_MEMBERSHIP];
GO
PRINT 'Done Syncronizing [dbo].[aspnet_Membership]'
GO
