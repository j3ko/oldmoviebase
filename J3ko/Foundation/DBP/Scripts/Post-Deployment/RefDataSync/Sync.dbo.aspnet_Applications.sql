﻿------------------------------------------------------------------------
-- Description:	Reference data sync for: dbo.aspnet_Applications
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables without identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
--
-- Modification History:
-- 2011.09.15	devuser			Auto generated from template
-- 2011.09.15   Jeffrey Ko		Removed merge delete
------------------------------------------------------------------------

PRINT 'Starting [dbo].[aspnet_Applications] Syncronization'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
                  WHERE TABLE_NAME = N'xxxSYNCxxASPNET_APPLICATIONS')
      DROP TABLE [dbo].[xxxSYNCxxASPNET_APPLICATIONS]
GO
CREATE TABLE [dbo].[xxxSYNCxxASPNET_APPLICATIONS]
(ApplicationName nvarchar(256), LoweredApplicationName nvarchar(256), ApplicationId uniqueidentifier, Description nvarchar(256))
GO
SET NOCOUNT ON
INSERT [dbo].[xxxSYNCxxASPNET_APPLICATIONS](ApplicationName, LoweredApplicationName, ApplicationId, Description)
            VALUES
				(N'Moviebase', N'moviebase', N'271A4678-5CB9-4094-A9DA-5991F0B66EED', null)
SET NOCOUNT OFF
GO
MERGE [dbo].[aspnet_Applications] AS Target
USING [dbo].[xxxSYNCxxASPNET_APPLICATIONS] AS Source ON (Target.ApplicationId = Source.ApplicationId)
			WHEN MATCHED 
					AND (Target.ApplicationName <> Source.ApplicationName
					OR Target.LoweredApplicationName <> Source.LoweredApplicationName
					OR Target.Description <> Source.Description)
				THEN
					UPDATE
					SET 
					Target.ApplicationName = Source.ApplicationName
					,Target.LoweredApplicationName = Source.LoweredApplicationName
					,Target.Description = Source.Description

            WHEN NOT MATCHED BY TARGET 
                THEN
					INSERT      (ApplicationName, LoweredApplicationName, ApplicationId, Description)
					VALUES      (ApplicationName, LoweredApplicationName, ApplicationId, Description);
GO
DROP TABLE [dbo].[xxxSYNCxxASPNET_APPLICATIONS];
GO
PRINT 'Done Syncronizing [dbo].[aspnet_Applications]'
GO
