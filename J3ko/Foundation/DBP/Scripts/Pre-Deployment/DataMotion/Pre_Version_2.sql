﻿IF NOT EXISTS
(
	SELECT 1 FROM [dbo].[schema_version]
		  WHERE [modified] = (SELECT MAX([modified]) FROM [dbo].[schema_version])
		  AND [version] >= 2
)
BEGIN
	PRINT 'Starting Version 2 Pre-Deployment Script'
      SET NOCOUNT ON
      IF NOT EXISTS(
            SELECT * FROM [sys].[tables]
                  WHERE object_id  = OBJECT_ID('[moviebase].[genre_title]')
				  )
      BEGIN
            IF (SELECT COUNT(*) FROM [moviebase].[title]) > 0
            BEGIN
                  BEGIN TRY
                        BEGIN TRAN PREDATAMOTO_GENRE_TITLE_TABLE
                              PRINT 'BACKING UP [moviebase].[title]'
                              EXEC('SELECT [title_id],[genre_id]
                                    INTO [dbo].[v2xDATAMOTOxxxGENRE_TITLE]
									FROM [moviebase].[title]')
                        COMMIT TRAN PREDATAMOTO_TITLE_TABLE;
                  END TRY
                  BEGIN CATCH
                        ROLLBACK TRAN PREDATAMOTO_TITLE_TABLE;
                        PRINT 'PREDATAMOTO_TITLE_TABLE TRANSACTION ROLLED BACK'
                        PRINT CAST(ERROR_NUMBER() AS CHAR(5)) + ERROR_MESSAGE()
                  END CATCH
            END
            ELSE
                  PRINT 'NO RECORDS TO WORRY ABOUT!!'
      END
      ELSE
            PRINT 'WARNING!!!!!: SCHEMA IS AN UNEXPECTED STATE FOR THIS UPGRADE SCRIPT AND DATABASE VERSION'
	SET NOCOUNT OFF
	PRINT 'Done Version 2 Pre-Deployment Script'
END
GO