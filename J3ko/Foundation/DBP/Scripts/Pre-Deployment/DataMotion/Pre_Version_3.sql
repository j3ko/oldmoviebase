IF NOT EXISTS
(
	SELECT 1 FROM [dbo].[schema_version]
		  WHERE [modified] = (SELECT MAX([modified]) FROM [dbo].[schema_version])
		  AND [version] >= 3
)
BEGIN
	PRINT 'Starting Version 3 Pre-Deployment Script'
      SET NOCOUNT ON
      IF NOT EXISTS(
            SELECT * FROM [sys].[indexes]
                  WHERE name = 'IX_title__imdb_id'
                  AND object_id  = OBJECT_ID('[moviebase].[title]')
				  )
      BEGIN
			UPDATE [moviebase].[title]
				SET
					[imdb_id] = NULL
				WHERE 
					[title_id] IN
					(
						SELECT DISTINCT	[t1].[title_id] 
						FROM
							[moviebase].[title] [t1]
						INNER JOIN
							[moviebase].[title] [t2] ON [t1].[imdb_id] = [t2].[imdb_id]
						WHERE ( [t1].[modified] = [t2].[modified] AND [t1].[title_id] < [t2].[title_id] )
						OR ([t1].[modified] < [t2].[modified])
					)

			CREATE UNIQUE NONCLUSTERED INDEX IX_title__imdb_id
				ON 
					[moviebase].[title](imdb_id)
				WHERE [imdb_id] IS NOT NULL
      END
      ELSE
            PRINT 'WARNING!!!!!: SCHEMA IS AN UNEXPECTED STATE FOR THIS UPGRADE SCRIPT AND DATABASE VERSION'
	SET NOCOUNT OFF
	PRINT 'Done Version 3 Pre-Deployment Script'
END
GO