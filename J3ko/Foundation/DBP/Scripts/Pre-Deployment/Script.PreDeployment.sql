﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

PRINT 'STARTING PRE-DEPLOYMENT SCRIPT'

-- DATAMOTION
:r .\DataMotion\Pre_Version_2.sql
:r .\DataMotion\Pre_Version_3.sql

PRINT 'FINISHED PRE-DEPLOYMENT SCRIPT'