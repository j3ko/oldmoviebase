﻿CREATE TABLE [moviebase].[title_person] (
    [title_id]       INT            NOT NULL,
    [person_id]      INT            NOT NULL,
    [ordinal]        INT            NOT NULL,
    [character_name] NVARCHAR (255) NULL,
    [title_role_id]  INT            NOT NULL,
    [row_version]    TIMESTAMP      NOT NULL
);

