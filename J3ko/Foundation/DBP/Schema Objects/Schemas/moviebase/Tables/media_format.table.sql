﻿CREATE TABLE [moviebase].[media_format] (
    [media_format_id] INT           IDENTITY (1, 1) NOT NULL,
    [description]     NVARCHAR (50) NOT NULL,
    [row_version]     TIMESTAMP     NOT NULL
);

