﻿CREATE TABLE [moviebase].[title] (
    [title_id]       INT              IDENTITY (1, 1) NOT NULL,
    [title_name]     NVARCHAR (255)   NOT NULL,
    [category_id]    INT              NOT NULL,
    [plot]           NVARCHAR (MAX)   NULL,
    [cover_id]       UNIQUEIDENTIFIER NULL,
    [small_cover_id] UNIQUEIDENTIFIER NULL,
    [imdb_id]        VARCHAR (50)     NULL,
    [synced]         DATE             NULL,
    [released]       DATE             NOT NULL,
    [modified]       DATETIME         NOT NULL,
    [modified_by]    NVARCHAR (255)   NOT NULL,
    [row_version]    TIMESTAMP        NOT NULL
);









