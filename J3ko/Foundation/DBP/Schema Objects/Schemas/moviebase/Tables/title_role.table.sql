﻿CREATE TABLE [moviebase].[title_role] (
    [title_role_id] INT            IDENTITY (1, 1) NOT NULL,
    [description]   NVARCHAR (255) NOT NULL,
    [row_version]   TIMESTAMP      NOT NULL
);

