﻿ALTER TABLE [moviebase].[person]
    ADD CONSTRAINT [FK_person_document] FOREIGN KEY ([thumbnail_id]) REFERENCES [common].[document] ([document_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

