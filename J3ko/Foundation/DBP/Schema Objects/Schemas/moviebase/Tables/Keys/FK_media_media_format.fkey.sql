﻿ALTER TABLE [moviebase].[media]
    ADD CONSTRAINT [FK_media_media_format] FOREIGN KEY ([media_format_id]) REFERENCES [moviebase].[media_format] ([media_format_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

