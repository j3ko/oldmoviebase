﻿ALTER TABLE [moviebase].[slot]
    ADD CONSTRAINT [PK_slot] PRIMARY KEY CLUSTERED ([slot_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

