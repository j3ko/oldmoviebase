﻿ALTER TABLE [moviebase].[genre_title]
    ADD CONSTRAINT [FK_genre_title_genre] FOREIGN KEY ([genre_id]) REFERENCES [moviebase].[genre] ([genre_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

