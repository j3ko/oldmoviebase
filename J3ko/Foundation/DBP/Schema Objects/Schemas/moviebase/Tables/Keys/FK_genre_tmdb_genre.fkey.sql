﻿ALTER TABLE [moviebase].[genre_tmdb]
    ADD CONSTRAINT [FK_genre_tmdb_genre] FOREIGN KEY ([genre_id]) REFERENCES [moviebase].[genre] ([genre_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

