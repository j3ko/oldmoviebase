﻿ALTER TABLE [moviebase].[alt_title_name]
    ADD CONSTRAINT [FK_alt_title_name_title] FOREIGN KEY ([title_id]) REFERENCES [moviebase].[title] ([title_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

