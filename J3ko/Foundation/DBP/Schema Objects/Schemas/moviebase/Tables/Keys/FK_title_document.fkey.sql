﻿ALTER TABLE [moviebase].[title]
    ADD CONSTRAINT [FK_title_document] FOREIGN KEY ([cover_id]) REFERENCES [common].[document] ([document_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

