﻿ALTER TABLE [moviebase].[title]
    ADD CONSTRAINT [FK_title_category] FOREIGN KEY ([category_id]) REFERENCES [moviebase].[category] ([category_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

