﻿ALTER TABLE [moviebase].[episode]
    ADD CONSTRAINT [FK_episode_instance] FOREIGN KEY ([instance_id]) REFERENCES [moviebase].[instance] ([instance_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

