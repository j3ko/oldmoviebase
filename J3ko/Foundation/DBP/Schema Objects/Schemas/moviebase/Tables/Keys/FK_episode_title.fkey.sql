﻿ALTER TABLE [moviebase].[episode]
    ADD CONSTRAINT [FK_episode_title] FOREIGN KEY ([title_id]) REFERENCES [moviebase].[title] ([title_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

