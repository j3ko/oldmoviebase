﻿ALTER TABLE [moviebase].[title_person]
    ADD CONSTRAINT [FK_title_person_person] FOREIGN KEY ([person_id]) REFERENCES [moviebase].[person] ([person_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

