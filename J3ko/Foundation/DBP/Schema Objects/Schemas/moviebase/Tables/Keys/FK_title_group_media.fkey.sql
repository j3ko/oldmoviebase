﻿ALTER TABLE [moviebase].[title_group]
    ADD CONSTRAINT [FK_title_group_media] FOREIGN KEY ([media_id]) REFERENCES [moviebase].[media] ([media_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

