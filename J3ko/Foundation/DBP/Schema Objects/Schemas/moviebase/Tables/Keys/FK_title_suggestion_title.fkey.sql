﻿ALTER TABLE [moviebase].[title_suggestion]
    ADD CONSTRAINT [FK_title_suggestion_title] FOREIGN KEY ([title_id]) REFERENCES [moviebase].[title] ([title_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

