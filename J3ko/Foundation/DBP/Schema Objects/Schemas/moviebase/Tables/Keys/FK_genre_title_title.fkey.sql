﻿ALTER TABLE [moviebase].[genre_title]
    ADD CONSTRAINT [FK_genre_title_title] FOREIGN KEY ([title_id]) REFERENCES [moviebase].[title] ([title_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

