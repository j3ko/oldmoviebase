﻿ALTER TABLE [moviebase].[title_group]
    ADD CONSTRAINT [FK_group_title] FOREIGN KEY ([title_id]) REFERENCES [moviebase].[title] ([title_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

