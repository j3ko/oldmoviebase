﻿CREATE TABLE [moviebase].[episode] (
    [title_id]       INT            NOT NULL,
    [instance_id]    INT            NOT NULL,
    [episode_name]   NVARCHAR (255) NOT NULL,
    [is_wishlist]    BIT            NOT NULL,
    [title_group_id] INT            NULL,
    [modified]       DATETIME       NOT NULL,
    [modified_by]    NVARCHAR (255) NOT NULL,
    [row_version]    TIMESTAMP      NOT NULL
);

