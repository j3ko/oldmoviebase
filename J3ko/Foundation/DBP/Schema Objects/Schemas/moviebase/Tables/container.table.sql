﻿CREATE TABLE [moviebase].[container] (
    [container_id]   INT            IDENTITY (1, 1) NOT NULL,
    [container_name] NVARCHAR (255) NOT NULL,
    [row_version]    TIMESTAMP      NOT NULL
);

