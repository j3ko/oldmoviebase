﻿CREATE TABLE [moviebase].[media] (
    [media_id]        INT            IDENTITY (1, 1) NOT NULL,
    [media_name]      NVARCHAR (255) NOT NULL,
    [media_format_id] INT            NOT NULL,
    [row_version]     TIMESTAMP      NOT NULL
);



