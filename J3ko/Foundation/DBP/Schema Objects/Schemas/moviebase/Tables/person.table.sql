﻿CREATE TABLE [moviebase].[person] (
    [person_id]    INT              IDENTITY (1, 1) NOT NULL,
    [full_name]    NVARCHAR (255)   NOT NULL,
    [thumbnail_id] UNIQUEIDENTIFIER NULL,
    [tmdb_id]      INT              NULL,
    [modified]     DATETIME         NOT NULL,
    [modified_by]  NVARCHAR (255)   NOT NULL,
    [row_version]  TIMESTAMP        NOT NULL
);





