﻿CREATE TABLE [moviebase].[title_suggestion] (
    [title_id]      INT   NOT NULL,
    [suggestion_id] INT   NOT NULL,
    [distance]      FLOAT NOT NULL
);

