﻿CREATE TABLE [moviebase].[instance] (
    [instance_id]   INT            IDENTITY (1, 1) NOT NULL,
    [instance_desc] NVARCHAR (255) NOT NULL,
    [row_version]   TIMESTAMP      NOT NULL
);

