﻿CREATE TABLE [moviebase].[title_group] (
    [title_group_id] INT            IDENTITY (1, 1) NOT NULL,
    [description]    NVARCHAR (255) NOT NULL,
    [date_added]     DATE           NOT NULL,
    [date_burned]    DATE           NOT NULL,
    [media_id]       INT            NOT NULL,
    [slot_id]        INT            NOT NULL,
    [title_id]       INT            NOT NULL,
    [modified]       DATETIME       NOT NULL,
    [modified_by]    NVARCHAR (255) NOT NULL,
    [row_version]    TIMESTAMP      NOT NULL
);



