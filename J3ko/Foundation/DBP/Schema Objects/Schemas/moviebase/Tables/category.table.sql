﻿CREATE TABLE [moviebase].[category] (
    [category_id]   INT            IDENTITY (1, 1) NOT NULL,
    [category_name] NVARCHAR (255) NOT NULL,
    [row_version]   TIMESTAMP      NOT NULL
);

