﻿
CREATE VIEW [moviebase].[vw_genres]
AS
SELECT
	a.[genre_id]
	, a.[genre_name]
	, CONVERT(BIT, COALESCE(NULLIF(b.genre_id, 0), 0)) [is_used]
	, a.[row_version]
FROM
	moviebase.genre a
LEFT OUTER JOIN
	( 
		SELECT DISTINCT genre_id
			FROM moviebase.genre_title 
	) b	ON a.genre_id = b.genre_id