﻿

CREATE VIEW [moviebase].[vw_title_genre_weights]
AS
	SELECT [t1].[title_id],
	stuff( (
		SELECT ',' + COALESCE([v].[value], '0') [value] 
		FROM   [moviebase].[genre] [g]
			   CROSS JOIN [moviebase].[title] [t]
			   CROSS APPLY ( SELECT 
								CASE WHEN EXISTS(	
										SELECT [gt].[genre_id] 
										FROM [moviebase].[genre_title] [gt] 
										WHERE [gt].[title_id] = [t].[title_id]
										AND [gt].[genre_id] = [g].[genre_id] ) THEN '2'
								ELSE '0'
								END [value] ) v
		WHERE [t1].[title_id] = [t].[title_id]
		ORDER BY [g].[genre_id]
		FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,1,'') AS weights
	FROM [moviebase].[title] [t1]
	GROUP BY [t1].[title_id]