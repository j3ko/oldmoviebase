﻿CREATE VIEW [moviebase].[vw_available_released_years]
AS
SELECT DISTINCT TOP (100) PERCENT DATEPART(year, released) AS released_year
FROM         moviebase.title
ORDER BY released_year