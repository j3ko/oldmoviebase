﻿

CREATE VIEW [moviebase].[vw_title_person_weights]
AS

	SELECT [t1].[title_id],
	stuff( (
		SELECT ',' + COALESCE([v].[value], '0') [value]
		FROM   [moviebase].[person] [p]
			   CROSS JOIN [moviebase].[title] [t]
			   CROSS APPLY ( SELECT CASE WHEN EXISTS (
   									SELECT [tp].[ordinal]
									FROM [moviebase].[title_person] [tp] 
									WHERE [tp].[title_id] = [t].[title_id]
									AND [tp].[person_id] = [p].[person_id]
									AND ([tp].[title_role_id] = 2 AND [tp].[ordinal] < 6)
							) THEN ( SELECT TOP 1 CONVERT(VARCHAR, 1 + (CONVERT(FLOAT, 1) / (COALESCE([tp].[ordinal], 0) + 1)))
									 FROM [moviebase].[title_person] [tp] 
									 WHERE [tp].[title_id] = [t].[title_id]
									 AND [tp].[person_id] = [p].[person_id]
									 ORDER BY [tp].[ordinal] ASC )
							ELSE '0'
							END [value] ) v
		WHERE [t1].[title_id] = [t].[title_id]
		ORDER BY [p].[person_id]
		FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,1,'') AS weights
	FROM [moviebase].[title] [t1]
	GROUP BY [t1].[title_id]