﻿
CREATE VIEW [moviebase].[vw_available_slots]
AS
SELECT
	[moviebase].[slot].[slot_id]
	,[moviebase].[slot].[slot_label]
	,[moviebase].[slot].[container_id]
	,CASE
		WHEN [moviebase].[title_group].[title_group_id] IS NULL THEN CONVERT(BIT, 'TRUE')
		ELSE CONVERT(BIT, 'FALSE')
	END [is_available]
	,[moviebase].[slot].[row_version]
FROM
	[moviebase].[slot]
LEFT OUTER JOIN
	[moviebase].[title_group] ON [moviebase].[slot].[slot_id] = [moviebase].[title_group].[slot_id]
