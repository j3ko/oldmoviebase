﻿
CREATE VIEW [moviebase].[vw_comparison_weights]
AS

SELECT [tgw].[title_id], [tgw].[weights] + ',' + [tpw].[weights] [weights]
FROM [moviebase].[vw_title_genre_weights] [tgw]
INNER JOIN [moviebase].[vw_title_person_weights] [tpw] ON [tgw].[title_id] = [tpw].[title_id]