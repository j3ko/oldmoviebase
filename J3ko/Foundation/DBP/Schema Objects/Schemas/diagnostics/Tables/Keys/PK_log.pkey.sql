﻿ALTER TABLE [diagnostics].[log]
    ADD CONSTRAINT [PK_log] PRIMARY KEY CLUSTERED ([log_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

