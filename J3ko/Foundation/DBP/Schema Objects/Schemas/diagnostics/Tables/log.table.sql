﻿CREATE TABLE [diagnostics].[log] (
    [log_id]      INT            IDENTITY (1, 1) NOT NULL,
    [app_name]    NVARCHAR (256) NOT NULL,
    [app_version] VARCHAR (50)   NOT NULL,
    [source]      NVARCHAR (MAX) NULL,
    [message]     NVARCHAR (MAX) NOT NULL,
    [stacktrace]  NVARCHAR (MAX) NULL,
    [severity]    INT            NOT NULL,
    [modified]    DATETIME       NOT NULL,
    [modified_by] NVARCHAR (256) NOT NULL,
    [row_version] TIMESTAMP      NOT NULL
);

