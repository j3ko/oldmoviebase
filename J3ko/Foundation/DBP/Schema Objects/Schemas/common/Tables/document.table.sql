﻿CREATE TABLE [common].[document] (
    [document_id]         UNIQUEIDENTIFIER           DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [file_name]           NVARCHAR (255)             NULL,
    [file_ext]            NVARCHAR (255)             NULL,
    [mime_type]           NVARCHAR (255)             NULL,
    [file_content]        VARBINARY (MAX) FILESTREAM NOT NULL,
    [file_size]           INT                        NOT NULL,
    [source]              NVARCHAR (MAX)             NULL,
    [local_path]          AS                         ([file_content].[PathName]()),
    [transaction_context] AS                         (get_filestream_transaction_context()),
    [modified]            DATETIME                   NOT NULL,
    [modified_by]         NVARCHAR (255)             NOT NULL,
    [row_version]         TIMESTAMP                  NOT NULL,
    PRIMARY KEY CLUSTERED ([document_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);

