﻿ALTER TABLE [dbo].[schema_version]
    ADD CONSTRAINT [PK_schema_version] PRIMARY KEY CLUSTERED ([version_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

