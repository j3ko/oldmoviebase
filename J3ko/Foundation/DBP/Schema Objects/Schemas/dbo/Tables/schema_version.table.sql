﻿CREATE TABLE [dbo].[schema_version] (
    [version_id] INT      IDENTITY (1, 1) NOT NULL,
    [version]    INT      NOT NULL,
    [modified]   DATETIME NOT NULL
);

