﻿GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Users_DeleteUser] TO [aspnet_Membership_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_CreateUser] TO [aspnet_Membership_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_SetPassword] TO [aspnet_Membership_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_ResetPassword] TO [aspnet_Membership_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_UnlockUser] TO [aspnet_Membership_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_UpdateUser] TO [aspnet_Membership_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer] TO [aspnet_Membership_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Membership_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Membership_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Membership_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_GetUserByName] TO [aspnet_Membership_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_GetUserByUserId] TO [aspnet_Membership_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_GetUserByEmail] TO [aspnet_Membership_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_GetPasswordWithFormat] TO [aspnet_Membership_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_UpdateUserInfo] TO [aspnet_Membership_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_GetPassword] TO [aspnet_Membership_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_GetNumberOfUsersOnline] TO [aspnet_Membership_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Membership_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Membership_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Membership_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_Applications] TO [aspnet_Membership_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_Users] TO [aspnet_Membership_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_GetUserByName] TO [aspnet_Membership_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_GetUserByUserId] TO [aspnet_Membership_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_GetUserByEmail] TO [aspnet_Membership_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_GetAllUsers] TO [aspnet_Membership_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_GetNumberOfUsersOnline] TO [aspnet_Membership_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_FindUsersByName] TO [aspnet_Membership_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_FindUsersByEmail] TO [aspnet_Membership_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_MembershipUsers] TO [aspnet_Membership_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Profile_DeleteProfiles] TO [aspnet_Profile_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Profile_DeleteInactiveProfiles] TO [aspnet_Profile_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Profile_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Profile_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Profile_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Profile_GetProperties] TO [aspnet_Profile_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Profile_SetProperties] TO [aspnet_Profile_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Profile_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Profile_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Profile_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_Applications] TO [aspnet_Profile_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_Users] TO [aspnet_Profile_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Profile_GetNumberOfInactiveProfiles] TO [aspnet_Profile_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Profile_GetProfiles] TO [aspnet_Profile_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_Profiles] TO [aspnet_Profile_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Roles_CreateRole] TO [aspnet_Roles_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Roles_DeleteRole] TO [aspnet_Roles_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UsersInRoles_AddUsersToRoles] TO [aspnet_Roles_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles] TO [aspnet_Roles_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Roles_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Roles_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Roles_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UsersInRoles_IsUserInRole] TO [aspnet_Roles_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UsersInRoles_GetRolesForUser] TO [aspnet_Roles_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Roles_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Roles_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Roles_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_Applications] TO [aspnet_Roles_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_Users] TO [aspnet_Roles_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UsersInRoles_IsUserInRole] TO [aspnet_Roles_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UsersInRoles_GetRolesForUser] TO [aspnet_Roles_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Roles_RoleExists] TO [aspnet_Roles_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UsersInRoles_GetUsersInRoles] TO [aspnet_Roles_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UsersInRoles_FindUsersInRole] TO [aspnet_Roles_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Roles_GetAllRoles] TO [aspnet_Roles_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_Roles] TO [aspnet_Roles_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_UsersInRoles] TO [aspnet_Roles_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_PersonalizationAdministration_DeleteAllState] TO [aspnet_Personalization_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_PersonalizationAdministration_ResetSharedState] TO [aspnet_Personalization_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_PersonalizationAdministration_ResetUserState] TO [aspnet_Personalization_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Personalization_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Personalization_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Personalization_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Personalization_GetApplicationId] TO [aspnet_Personalization_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Paths_CreatePath] TO [aspnet_Personalization_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_PersonalizationAllUsers_GetPageSettings] TO [aspnet_Personalization_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings] TO [aspnet_Personalization_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_PersonalizationAllUsers_SetPageSettings] TO [aspnet_Personalization_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_PersonalizationPerUser_GetPageSettings] TO [aspnet_Personalization_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_PersonalizationPerUser_ResetPageSettings] TO [aspnet_Personalization_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_PersonalizationPerUser_SetPageSettings] TO [aspnet_Personalization_BasicAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Personalization_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Personalization_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Personalization_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_Applications] TO [aspnet_Personalization_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_Users] TO [aspnet_Personalization_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_PersonalizationAdministration_FindState] TO [aspnet_Personalization_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_PersonalizationAdministration_GetCountOfState] TO [aspnet_Personalization_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_WebPartState_Paths] TO [aspnet_Personalization_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_WebPartState_Shared] TO [aspnet_Personalization_ReportingAccess];


GO
GRANT SELECT
    ON OBJECT::[dbo].[vw_aspnet_WebPartState_User] TO [aspnet_Personalization_ReportingAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_WebEvent_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_CheckSchemaVersion] TO [aspnet_WebEvent_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_WebEvent_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_WebEvent_LogEvent] TO [aspnet_WebEvent_FullAccess];

