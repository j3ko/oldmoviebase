﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Security.Principal;
using System.Web;

namespace J3ko.Foundation
{
    public static class AuditUtility
    {

        public static void ProcessAuditFields(IEnumerable<object> list)
        {
            foreach (var i in list)
            {
                SetProperty(i, "modified_by", Username);
                SetProperty(i, "modified", Timestamp);
            }
        }

        public static void SetCurrentUser(string name)
        {
            System.Threading.Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(name, "Passport"), null);
        }

        private static void SetProperty(object obj, string property, object value)
        {
            Type t = obj.GetType();
            PropertyInfo p = null;
            
            if (t != null)
                p = t.GetProperty(property, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);            
            if (p != null)
                p.SetValue(obj, value, null);
        }

        private static string Username
        {
            get
            {

                string result = null;

                if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
                    result = HttpContext.Current.User.Identity.Name;

                if (string.IsNullOrWhiteSpace(result))
                {
                    result = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                    if (string.IsNullOrWhiteSpace(result))
                        result = WindowsIdentity.GetCurrent().Name;
                }
                return result ?? "Unknown";
            }
        }

        private static DateTime Timestamp
        {
            get
            {
                return DateTime.Now;
            }
        }
    }
}
