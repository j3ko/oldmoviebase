﻿using System;
using System.Data.Objects;
using System.Linq;

namespace J3ko.Foundation.Repository.EF
{
    public abstract class EFRepositoryBase<TEntity, TContext> 
        : DbRepositoryBase<TEntity, TContext, EFWorkUnit<TContext>>
        where TContext : ObjectContext
        where TEntity : class
    {
        private IObjectContextFactory<TContext> ContextFactory { get; set; }

        public EFRepositoryBase(IObjectContextFactory<TContext> contextFactory)
        {
            ContextFactory = contextFactory;
        }

        sealed public override IWorkUnit CreateWorkUnit()
        {
            TContext context = ContextFactory.Create();
            return new EFWorkUnit<TContext>(context);
        }

        sealed protected override TContext RepositoryContext
        {
            get
            {
                return ContextFactory.Create();
            }
        }

        protected abstract override void OnSave(TEntity entity, EFWorkUnit<TContext> work);

        protected abstract override void OnDelete(TEntity entity, EFWorkUnit<TContext> work);

        protected abstract override IQueryable<TEntity> OnGetAll(EFWorkUnit<TContext> work);
    }
}
