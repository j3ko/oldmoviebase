﻿using System;
using System.Data;
using System.Data.Objects;

namespace J3ko.Foundation.Repository.EF
{
    public class EFWorkUnit<TContext> : IWorkUnit<TContext>
        where TContext : ObjectContext
    {
        private bool disposed = false;

        public TContext Context { get; set; }
        private IDbTransaction DbTransaction { get; set; }

        public EFWorkUnit(TContext dbContext)
        {
            this.Context = dbContext;

            if (this.Context.Connection.State == System.Data.ConnectionState.Closed)
            {
                this.Context.Connection.Open();
            }

            this.DbTransaction = this.Context.Connection.BeginTransaction();            
        }

        public void Commit()
        {
            try
            {
                this.DbTransaction.Commit();
            }
            finally
            {
                this.Dispose(true);
            }
        }

        public void Rollback()
        {
            try
            {
                this.DbTransaction.Rollback();
            }
            finally
            {
                this.Dispose(true);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (this.DbTransaction != null)
                        this.DbTransaction.Dispose();

                    if (this.Context != null)
                        this.Context.Dispose();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }
    }
}
