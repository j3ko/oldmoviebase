﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;

namespace J3ko.Foundation.Repository.EF
{
    public interface IObjectContextFactory<TContext>
        where TContext : ObjectContext
    {
        TContext Create();
    }
}
