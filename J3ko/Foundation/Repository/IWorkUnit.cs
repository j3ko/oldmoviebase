﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Foundation.Repository
{
    public interface IWorkUnit : IDisposable
    {
        void Commit();
        void Rollback();
    }
    public interface IWorkUnit<TContext> : IWorkUnit
        where TContext : class
    {
        TContext Context { get; }
    }
}
