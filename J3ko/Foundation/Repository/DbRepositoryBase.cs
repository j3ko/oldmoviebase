﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Foundation.Repository
{
    public abstract class DbRepositoryBase<TEntity, TContext, TWorkUnit> : IDisposable, IRepository<TEntity>
        where TEntity : class
        where TContext : class
        where TWorkUnit : class, IWorkUnit<TContext>
    {
        private bool _isDisposed = false;
        private TWorkUnit _getWork = null;

        #region IRepository<TEntity> Members

        public void Save(TEntity entity)
        {
            IWorkUnit work = CreateWorkUnit();
            Save(entity, work);
            work.Commit();
        }

        public void Save(TEntity entity, IWorkUnit workUnit)
        {
            TWorkUnit work = workUnit as TWorkUnit;

            try
            {
                if (workUnit == null)
                    throw new ArgumentNullException("workUnit");
                if (work == null)
                    throw new RepositoryException(string.Format("Expecting IWorkUnit of type {0} but {1} was found.", typeof(TWorkUnit).FullName, workUnit.GetType().FullName));

                OnSave(entity, work);
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                work.Rollback();
                throw ex;
            }
        }

        public void Delete(TEntity entity)
        {
            IWorkUnit work = CreateWorkUnit();
            Delete(entity, work);
            work.Commit();
        }

        public void Delete(TEntity entity, IWorkUnit workUnit)
        {
            TWorkUnit work = workUnit as TWorkUnit;

            try
            {
                if (workUnit == null)
                    throw new ArgumentNullException("workUnit");
                if (work == null)
                    throw new RepositoryException(string.Format("Expecting IWorkUnit of type {0} but {1} was found.", typeof(TWorkUnit).FullName, workUnit.GetType().FullName));

                OnDelete(entity, work);
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                work.Rollback();
                throw ex;
            }
        }

        public IQueryable<TEntity> GetAll()
        {
            if (_getWork == null) 
                _getWork = Activator.CreateInstance(typeof(TWorkUnit), new object[] { RepositoryContext, false }) as TWorkUnit;

            return OnGetAll(_getWork);
        }

        public IQueryable<TEntity> GetAll(IWorkUnit workUnit)
        {
            TWorkUnit work = workUnit as TWorkUnit;

            try
            {
                if (workUnit == null)
                    throw new ArgumentNullException("workUnit");
                if (work == null)
                    throw new RepositoryException(string.Format("Expecting IWorkUnit of type {0} but {1} was found.", typeof(TWorkUnit).FullName, workUnit.GetType().FullName));

                return OnGetAll(work);
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                work.Rollback();
                throw ex;
            }
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause)
        {
            return GetAll()
                .Where(whereClause)
                .ToList();
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, IWorkUnit workUnit)
        {
            return GetAll(workUnit)
                .Where(whereClause)
                .ToList();
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, System.Linq.Expressions.Expression<Func<TEntity, bool>> orderClause)
        {
            return GetAll()
                .Where(whereClause)
                .OrderBy(orderClause)
                .ToList();
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, System.Linq.Expressions.Expression<Func<TEntity, bool>> orderClause, IWorkUnit workUnit)
        {
            return GetAll(workUnit)
                .Where(whereClause)
                .OrderBy(orderClause)
                .ToList();
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, System.Linq.Expressions.Expression<Func<TEntity, bool>> orderClause, int page, int pageSize)
        {
            return GetAll()
                .Where(whereClause)
                .OrderBy(orderClause)
                .Skip(pageSize)
                .Take(page)
                .ToList();
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, System.Linq.Expressions.Expression<Func<TEntity, bool>> orderClause, int page, int pageSize, IWorkUnit workUnit)
        {
            return GetAll(workUnit)
                .Where(whereClause)
                .OrderBy(orderClause)
                .Skip(pageSize)
                .Take(page)
                .ToList();
        }

        public TEntity FindSingle(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause)
        {
            return GetAll()
                .SingleOrDefault(whereClause);
        }

        public TEntity FindSingle(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, IWorkUnit workUnit)
        {
            return GetAll(workUnit)
                .SingleOrDefault(whereClause);
        }
        #endregion

        public abstract IWorkUnit CreateWorkUnit();

        protected abstract TContext RepositoryContext { get; }

        protected abstract void OnSave(TEntity entity, TWorkUnit work);

        protected abstract void OnDelete(TEntity entity, TWorkUnit work);

        protected abstract IQueryable<TEntity> OnGetAll(TWorkUnit work);

        ~DbRepositoryBase()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // Code to dispose the managed resources of the class
                    if (_getWork != null)
                    {
                        _getWork.Dispose();
                        _getWork = null;
                    }
                }
            }
            // Code to dispose the un-managed resources of the class

            _isDisposed = true;
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
