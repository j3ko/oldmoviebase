﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace J3ko.Foundation.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Creates a Transaction
        /// </summary>
        /// <returns></returns>
        IWorkUnit CreateWorkUnit();
        void Save(TEntity entity);
        void Save(TEntity entity, IWorkUnit workUnit);
        void Delete(TEntity entity);
        void Delete(TEntity entity, IWorkUnit workUnit);
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetAll(IWorkUnit workUnit);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> whereClause);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> whereClause, IWorkUnit workUnit);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> whereClause, Expression<Func<TEntity, bool>> orderClause);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> whereClause, Expression<Func<TEntity, bool>> orderClause, IWorkUnit workUnit);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> whereClause, Expression<Func<TEntity, bool>> orderClause, int page, int pageSize);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> whereClause, Expression<Func<TEntity, bool>> orderClause, int page, int pageSize, IWorkUnit workUnit);
        TEntity FindSingle(Expression<Func<TEntity, bool>> whereClause);
        TEntity FindSingle(Expression<Func<TEntity, bool>> whereClause, IWorkUnit workUnit);
    }
}
