﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;

namespace J3ko.Foundation.Repository.Linq
{
    public interface IDataContextFactory<TContext>
        where TContext : DataContext
    {
        TContext Create();
    }
}
