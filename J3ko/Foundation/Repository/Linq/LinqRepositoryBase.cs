﻿using System;
using System.Data.Linq;
using System.Linq;

namespace J3ko.Foundation.Repository.Linq
{
    public abstract class LinqRepositoryBase<TEntity, TContext>
        : DbRepositoryBase<TEntity, TContext, LinqWorkUnit<TContext>>
        where TContext : DataContext
        where TEntity : class
    {
        private IDataContextFactory<TContext> ContextFactory { get; set; }

        public LinqRepositoryBase(IDataContextFactory<TContext> contextFactory)
        {
            ContextFactory = contextFactory;
        }

        sealed public override IWorkUnit CreateWorkUnit()
        {
            TContext context = ContextFactory.Create();
            return new LinqWorkUnit<TContext>(context);
        }

        sealed protected override TContext RepositoryContext
        {
            get
            {
                return ContextFactory.Create();
            }
        }

        protected abstract override void OnSave(TEntity entity, LinqWorkUnit<TContext> work);

        protected abstract override void OnDelete(TEntity entity, LinqWorkUnit<TContext> work);

        protected abstract override IQueryable<TEntity> OnGetAll(LinqWorkUnit<TContext> work);
    }
}
