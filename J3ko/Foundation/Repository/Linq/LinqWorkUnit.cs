﻿using System;
using System.Data;
using System.Data.Linq;

namespace J3ko.Foundation.Repository.Linq
{
    public class LinqWorkUnit<TContext> : IWorkUnit<TContext>
        where TContext : DataContext
    {
        private bool _isDisposed = false;

        public TContext Context { get; private set; }
        private IDbTransaction DbTransaction { get; set; }

        public LinqWorkUnit(TContext dbContext)
            : this(dbContext, true)
        { }

        public LinqWorkUnit(TContext dbContext, bool beginTransaction)
        {
            if (dbContext == null)
                throw new ArgumentNullException("dbContext");

            this.Context = dbContext;

            if (this.Context.Connection.State == System.Data.ConnectionState.Closed)
            {
                this.Context.Connection.Open();
            }

            if (beginTransaction)
            {
                this.Context.Transaction = this.Context.Connection.BeginTransaction();
                this.DbTransaction = this.Context.Transaction;
            }
        }

        public void Commit()
        {
            try
            {
                if (this.Context != null)
                    this.Context.SubmitChanges();
                if (this.DbTransaction != null)
                    this.DbTransaction.Commit();
            }
            finally
            {
                this.Dispose(true);
            }
        }

        public void Rollback()
        {
            try
            {
                if (this.Context != null)
                    this.Context.SubmitChanges();
                if (this.Context != null)
                    this.DbTransaction.Rollback();
            }
            finally
            {
                this.Dispose(true);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        ~LinqWorkUnit()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._isDisposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (this.DbTransaction != null)
                    {
                        this.DbTransaction.Dispose();
                        this.DbTransaction = null;
                    }

                    if (this.Context != null)
                    {
                        if (this.Context.Connection.State != ConnectionState.Closed)
                        {
                            this.Context.Connection.Close();
                        }

                        this.Context.Connection.Dispose();
                        this.Context.Dispose();
                        this.Context = null;
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                _isDisposed = true;

            }
        }
    }
}
