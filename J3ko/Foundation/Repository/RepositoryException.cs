﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Foundation.Repository
{
    public class RepositoryException : Exception
    {
        public RepositoryException(string message)
            : base(message)
        { }
    }
}
