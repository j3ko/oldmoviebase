﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Algorithm.Suggestion
{
    public static class CosineDistance
    {
        public static double Calculate(double[] a, double[] b)
        {
            if (a == null || b == null)
                throw new ArgumentNullException("a and b cannot be null");
            if (a.Length != b.Length)
                throw new ArgumentException("Length of a must match length of b");

            // dot product
            double dotp = DotProduct(a, b);
            // magnitude a
            double maga = Magnitude(a);
            // magnitude b
            double magb = Magnitude(b);
            // divide dot product of a & b by the product of magnitude of a & b

            return dotp / (maga * magb);
        }

        private static double DotProduct(double[] a, double[] b)
        {
            double result = 0;
            for (int i = 0; i < a.Length; i++)
            {
                result += a[i] * b[i];
            }
            return result;
        }

        private static double Magnitude(double[] a)
        {
            double result = 0;
            for (int i = 0; i < a.Length; i++)
            {
                result += Math.Pow(a[i], 2);
            }
            return Math.Sqrt(result);
        }


    }
}
