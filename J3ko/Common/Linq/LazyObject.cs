﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Common.Linq
{
    /// <summary>
    /// Not complete
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LazyObject<T>
    {
        private IQueryable<T> _query;
        private T _inner;

        /*
        public LazyObject()
        {
            inner = new object();
        }
        */

        public LazyObject(IQueryable<T> query)
        {
            _query = query;
        }

        public T Inner
        {
            get
            {
                if (_inner == null)
                    _inner = _query.SingleOrDefault();
                return _inner;
            }
        }
    }
}
