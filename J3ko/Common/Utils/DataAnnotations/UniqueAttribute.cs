﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace J3ko.Common.MVC.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Class)]
    public class UniqueAttribute : ValidationAttribute
    {
        public string PropertyName { get; set; }

        public override bool IsValid(object value)
        {
            return base.IsValid(value);
        }
    }
}
