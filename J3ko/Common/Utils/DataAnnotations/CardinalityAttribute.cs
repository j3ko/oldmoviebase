﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Collections;
using System.Reflection;

namespace J3ko.Common.Utils.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property|AttributeTargets.Class)]
    public sealed class CardinalityAttribute : ValidationAttribute
    {
        private const string MIN_MAX_ERROR_MSG = "{0} must have between {1} and {2} elements";
        private const string MIN_ERROR_MSG = "{0} must have at least {1} element(s)";
        private const string MAX_ERROR_MSG = "{0} must have at most {2} element(s)";
        private int _min = -1;
        private int _max = -1;

        //public CardinalityAttribute()
        //    : base()
        //{ }

        public string PropertyName { get; set; }

        public int MinCount 
        {
            get
            {
                return _min;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("MinCount", "Minimum value cannot be less than 0.");
                if (MaxCount >= 0 && value > MaxCount)
                    throw new ArgumentOutOfRangeException("MinCount", "Minimum value cannot be more than maximum value.");

                _min = value;
            }
        }

        public int MaxCount 
        {
            get
            {
                return _max;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("MaxCount", "Maximum value cannot be less than 0.");
                if (MinCount >= 0 && value < MinCount)
                    throw new ArgumentOutOfRangeException("MaxCount", "Maximum value cannot be less than minimum value.");

                _max = value;
            }
        }

        public override bool IsValid(object value)
        {

            object prop = value;

            if (!string.IsNullOrWhiteSpace(PropertyName))
                prop = ReflectionUtils.GetProperty(value, PropertyName);

            IList list = prop as IList;

            if (list == null & MaxCount < 0 & MinCount < 0)
                return true;
            if (list != null & MaxCount >= 0 & MinCount < 0)
                return list.Count <= MaxCount;
            if (list != null & MaxCount < 0 & MinCount >= 0)
                return list.Count >= MinCount;

            return list.Count >= MinCount & list.Count <= MaxCount;
        }

        public override string FormatErrorMessage(string name)
        {
            string msg = MIN_MAX_ERROR_MSG;

            if (MinCount >= 0 & MaxCount < 0)
                msg = MIN_ERROR_MSG;
            if (MinCount < 0 & MaxCount >= 0)
                msg = MAX_ERROR_MSG;

            string propName = !string.IsNullOrWhiteSpace(PropertyName) ? PropertyName : name;
    
            return string.Format(msg, propName, MinCount, MaxCount);
        }
    }
}
