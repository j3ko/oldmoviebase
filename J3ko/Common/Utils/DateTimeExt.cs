﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Common.Utils
{
    public static class DateTimeExt
    {
        public static bool IsBetween(this DateTime date, DateTime firstDate, DateTime secondDate)
        {
            return (date >= firstDate && date <= secondDate) || (date <= firstDate && date >= secondDate);
        }

        public static bool IsValidSqlDateTime(this DateTime date)
        {
            return date.IsBetween((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue, (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue);
        }
    }
}
