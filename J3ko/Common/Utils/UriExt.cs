﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Web;

namespace J3ko.Common.Utils
{
    public static class UriExt
    {
        public static Uri AddHttpGetParam(this Uri uri, string key, string value)
        {
            return AddHttpGetParam(uri, key, value, true);
        }

        public static Uri AddHttpGetParam(this Uri uri, string key, string value, bool withReplacement)
        {
            if (!uri.Scheme.Equals("http", StringComparison.CurrentCultureIgnoreCase))
                throw new InvalidOperationException("Uri must be HTTP scheme");

            NameValueCollection query = HttpUtility.ParseQueryString(uri.Query);

            if (withReplacement)
                query.Remove(key);

            query.Add(key, value);

            string[] oldTokens = uri.AbsoluteUri.Split(new char[] { '?' });

            string abs = uri.AbsoluteUri;

            if (oldTokens != null && oldTokens.Length > 0)
            {
                string[] newTokens = new string[] { oldTokens[0], query.ToString() }
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .ToArray();
                abs = string.Join("?", newTokens);
            }

            return new Uri(abs, UriKind.Absolute);
        }

        public static Uri RemoveHttpGetParam(this Uri uri, string key, string value)
        {
            if (!uri.Scheme.Equals("http", StringComparison.CurrentCultureIgnoreCase))
                throw new InvalidOperationException("Uri must be HTTP scheme");

            NameValueCollection query = HttpUtility.ParseQueryString(uri.Query);

            string[] vals = query.GetValues(key);

            if (value != null)
                vals = vals.Where(x => !x.Equals(value, StringComparison.CurrentCultureIgnoreCase)).ToArray();

            query.Remove(key);

            if (value != null)
            {
                foreach (string v in vals)
                {
                    query.Add(key, v);
                }
            }

            string[] oldTokens = uri.AbsoluteUri.Split(new char[] { '?' });

            string abs = uri.AbsoluteUri;

            if (oldTokens != null && oldTokens.Length > 0)
            {
                string[] newTokens = new string[] { oldTokens[0], query.ToString() }
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .ToArray();
                abs = string.Join("?", newTokens);
            }

            return new Uri(abs, UriKind.Absolute);
        }

        public static Uri RemoveHttpGetParam(this Uri uri, string key)
        {
            return RemoveHttpGetParam(uri, key, null);
        }
    }
}
