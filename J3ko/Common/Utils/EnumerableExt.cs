﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace J3ko.Common.Utils
{
    public static class EnumerableExt
    {
        public static IEnumerable NotOfType<S>(this IEnumerable<object> source)
        {
            return source.Where(t => !(t is S));
        }
    }
}
