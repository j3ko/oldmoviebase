﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace J3ko.Common.Utils
{
    public static class ReflectionUtils
    {
        public static object GetPropertyFromPath(object srcObj, string propertyPath)
        {
            object temp = srcObj;

            foreach (string name in propertyPath.Split('.'))
            {
                temp = GetProperty(temp, name);
                if (temp == null)
                    return null;
            }
            return temp;
        }

        public static object GetProperty(object srcObj, string propertyName)
        {

            PropertyInfo propInfoObj = srcObj.GetType().GetProperty(propertyName);

            if (propInfoObj == null)
                return null;

            // Get the value from property.
            object srcValue = srcObj
                      .GetType()
                      .InvokeMember(propInfoObj.Name,
                              BindingFlags.GetProperty,
                              null,
                              srcObj,
                              null);

            return srcValue;
        }

        public static object GetProperty(object srcObj, string propertyName, bool ignoreCase)
        {
            PropertyInfo propInfoObj;

            BindingFlags bindingAttrs = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

            bindingAttrs = (ignoreCase) ? bindingAttrs | BindingFlags.IgnoreCase : bindingAttrs;

            propInfoObj = srcObj.GetType().GetProperty(propertyName, bindingAttrs);

            if (propInfoObj == null)
                return null;

            // Get the value from property.
            object srcValue = srcObj
                                .GetType()
                                .InvokeMember(propInfoObj.Name,
                                                BindingFlags.GetProperty,
                                                null,
                                                srcObj,
                                                null);

            return srcValue;
        }
    }
}
