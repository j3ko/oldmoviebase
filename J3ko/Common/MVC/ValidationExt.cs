﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Text.RegularExpressions;

namespace J3ko.Common.MVC
{
    public static class ValidationExt
    {
        public static MvcHtmlString BoundedValidationSummary(this HtmlHelper htmlHelper, bool excludePropertyErrors, string message, object htmlAttributes)
        {
            if (htmlHelper.ValidationSummary(excludePropertyErrors, message, htmlAttributes) == null)
                return null;

            string str = htmlHelper.ValidationSummary(excludePropertyErrors, message, htmlAttributes).ToString();
            MatchCollection matches = Regex.Matches(str, @"</?\w+((\s+\w+(\s*=\s*(?:"".*?""|'.*?'|[^'"">\s]+))?)+\s*|\s*)/?>");
            if (matches.Count > 2 )
            {
                string opening = matches[0].ToString();
                string closing = matches[matches.Count - 1].ToString();

                StringBuilder sb = new StringBuilder();
                sb.Append(opening);
                sb.Append(@"<div class=""validation-summary-top""></div>");
                sb.Append(@"<div class=""validation-summary-body"">");

                int i = str.IndexOf(opening) + opening.Length;
                int j = str.LastIndexOf(closing);
                string temp = str.Substring(i, j - i);
                sb.Append(temp);

                
                sb.Append(@"</div>");
                sb.Append(@"<div class=""validation-summary-bottom""></div>");
                sb.Append(closing);
                str = sb.ToString();
            }
            return new MvcHtmlString(str);
        }
    }
}
