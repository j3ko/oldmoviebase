﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using J3ko.Common.Utils;

namespace J3ko.Common.MVC.ModelBinders
{
    public class ShortGuidBinder : IModelBinder
    {
        #region IModelBinder Members

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            object result = null;
            string val = null;

            if (bindingContext.ModelType == typeof(ShortGuid)) 
            {
                ValueProviderResult temp = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
                if (temp == null)
                    val = ShortGuid.Empty;
                else
                    val = temp.RawValue.ToString();

                if (string.IsNullOrWhiteSpace(val))
                    result = ShortGuid.Empty;
                else
                    result = new ShortGuid(val);
            }
            else if (bindingContext.ModelType == typeof(ShortGuid?))
            {
                ValueProviderResult temp = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

                if (temp == null)
                {
                    result = null;
                }
                else
                {
                    val = temp.RawValue.ToString();

                    if (!string.IsNullOrWhiteSpace(val))
                        result = new ShortGuid(val);
                }
            }

            return result;
        }

        #endregion
    }
}
