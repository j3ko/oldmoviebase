﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel;

namespace J3ko.Common.MVC.ModelBinders
{
    /// <summary>
    /// Takes Model of type IDataErrorInfo and adds the Error property to the ModelState (this is not done by default)
    /// </summary>
    public class ClassValidatingBinder : DefaultModelBinder
    {
        protected override void OnModelUpdated(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            //base.OnModelUpdated(controllerContext, bindingContext);
            IDataErrorInfo errorProvider = bindingContext.Model as IDataErrorInfo;
            if (errorProvider != null)
            {
                string errorText = errorProvider.Error;
                if (!String.IsNullOrEmpty(errorText))
                {
                    bindingContext.ModelState.AddModelError(string.Empty, errorText);
                }
            }

            foreach (ModelValidator validator in bindingContext.ModelMetadata.GetValidators(controllerContext))
            {
                foreach (ModelValidationResult validationResult in validator.Validate(null))
                {
                    bindingContext.ModelState.AddModelError(CreateSubPropertyName(bindingContext.ModelName, validationResult.MemberName), validationResult.Message);
                }
            }
        }
    }
}
