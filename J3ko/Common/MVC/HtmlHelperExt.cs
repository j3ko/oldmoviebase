﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using J3ko.Common.Utils;
using System.Reflection;
using System.Web.Mvc.Html;

namespace J3ko.Common.MVC
{
    public static class HtmlHelperExt
    {
        public static string Truncate(this HtmlHelper html, string source, int length)
        {
            return source.Truncate("...", 0, length);
        }

        public static MvcForm BeginForm(this HtmlHelper html, FormMethod formMethod)
        {
            string controller = (string)html.ViewContext.RouteData.Values["controller"];
            string action = (string)html.ViewContext.RouteData.Values["action"];

            return html.BeginForm(action, controller, formMethod);
        }
    }
}
