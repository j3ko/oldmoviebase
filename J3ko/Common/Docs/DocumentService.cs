﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using J3ko.Common.Docs.Repositories;
using J3ko.Common.Utils;
using J3ko.Common.Docs.Repositories.Linq;

namespace J3ko.Common.Docs
{
    public class DocumentService : IDisposable
    {
        private DocumentRepository<DocumentBO> _drepo;

        public DocumentService()
            : this(new LinqDocumentRepository())
        { }

        public DocumentService(DocumentRepository<DocumentBO> drepo)
        {
            _drepo = drepo;
        }

        public DocumentBO GetDocument(Guid guid)
        {
            return _drepo.FindSingle(x => x.DocumentId == guid);
        }

        public IEnumerable<DocumentBO> GetDocuments()
        {
            return _drepo.GetAll();
        }

        public DocumentBO SaveDocument(DocumentBO doc)
        {
            _drepo.Save(doc);

            return doc;
        }

        ~DocumentService()
        {
            Dispose(false);
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Code to dispose the managed resources of the class
                if (_drepo != null)
                {
                    _drepo.Dispose();
                    _drepo = null;
                }
            }
            // Code to dispose the un-managed resources of the class
        }
    }
}
