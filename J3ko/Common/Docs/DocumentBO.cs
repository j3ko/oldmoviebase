﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using J3ko.Common.Utils;
using System.Drawing;

namespace J3ko.Common.Docs
{
    public class DocumentBO
    {
        private byte[] _bytes;

        public Guid DocumentId { get; set; }
        public string FileName { get; set; }
        public string FileExt { get; set; }
        public string MimeType { get; set; }
        public int FileSize { get; set; }
        public Stream FileContent 
        {
            get
            {
                return new MemoryStream(Bytes);
            }
        }
        public string Source { get; set; }

        public byte[] Bytes
        {
            get
            {
                if (_bytes == null)
                    _bytes = new byte[0];
                return _bytes;
            }
            set
            {
                _bytes = value;
            }
        }

        public bool IsEmpty
        {
            get
            {
                return (Bytes.Length == 0);
            }
        }

        public static DocumentBO FromStream(Stream stream, string filename)
        {
            byte[] b = stream.ToByteArray();
            return FromBytes(b, filename);
        }

        public static DocumentBO FromBytes(byte[] bytes, string filename)
        {
            DocumentBO doc = new DocumentBO
            {
                Bytes = bytes,
                FileSize = bytes.Count(),
                MimeType = bytes.GetMime(),
                FileExt = Path.GetExtension(filename),
                FileName = Path.GetFileNameWithoutExtension(filename),
            };

            return doc;
        }

        public static DocumentBO FromFile(string path)
        {
            byte[] bytes = WebMethods.GetImage(path).ToArray();
            string filename = Path.GetFileName(path);
            DocumentBO result = DocumentBO.FromBytes(bytes, filename);
            return result;
        }
    }
}
