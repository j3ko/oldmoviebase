﻿using J3ko.Common.Docs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace J3ko.Common.Docs.Test
{
    
    
    /// <summary>
    ///This is a test class for DocumentServiceTest and is intended
    ///to contain all DocumentServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DocumentServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetDocument
        ///</summary>
        [TestMethod()]
        public void GetDocumentTest()
        {
            DocumentService target = new DocumentService(); // TODO: Initialize to an appropriate value
            Guid guid = new Guid(); // TODO: Initialize to an appropriate value
            DocumentBO expected = null; // TODO: Initialize to an appropriate value
            DocumentBO actual;
            actual = target.GetDocument(guid);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetDocuments
        ///</summary>
        [TestMethod()]
        public void GetDocumentsTest()
        {
            DocumentService target = new DocumentService(); // TODO: Initialize to an appropriate value
            IEnumerable<DocumentBO> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<DocumentBO> actual;
            actual = target.GetDocuments();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SaveDocument
        ///</summary>
        [TestMethod()]
        public void SaveDocumentTest()
        {
            DocumentService target = new DocumentService(); // TODO: Initialize to an appropriate value
            DocumentBO doc = null; // TODO: Initialize to an appropriate value
            DocumentBO expected = null; // TODO: Initialize to an appropriate value
            DocumentBO actual;
            actual = target.SaveDocument(doc);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
