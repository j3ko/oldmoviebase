﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Foundation.Repository.Linq;
using J3ko.Foundation;

namespace J3ko.Common.Docs.Datasources
{
    public class DocumentContextFactory : IDataContextFactory<DocumentData>
    {
        #region IDataContextFactory<DocumentData> Members

        public DocumentData Create()
        {
            return new DocumentData(BootStrapper.DefaultConnectionString);
        }

        #endregion
    }
}
