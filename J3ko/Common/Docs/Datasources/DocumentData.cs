using J3ko.Foundation;
using System.Data.Linq;

namespace J3ko.Common.Docs.Datasources
{
    partial class DocumentData
    {
        public override void SubmitChanges(ConflictMode failureMode)
        {
            AuditUtility.ProcessAuditFields(GetChangeSet().Updates);
            AuditUtility.ProcessAuditFields(GetChangeSet().Inserts);
            base.SubmitChanges(failureMode);
        }
    }
}
