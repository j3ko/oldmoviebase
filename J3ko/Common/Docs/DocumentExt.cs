﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace J3ko.Common.Docs
{
    public static class DocumentExt
    {
        public static IEnumerable<DocumentBO> WithFilename(this IEnumerable<DocumentBO> docs, string filename)
        {
            string name = Path.GetFileNameWithoutExtension(filename);
            string ext = Path.GetExtension(filename);

            return from d in docs
                   where d.FileName == name && d.FileExt == ext
                   select d;
        }

        public static DocumentBO WithId(this IEnumerable<DocumentBO> docs, Guid id)
        {
            return (from d in docs
                    where d.DocumentId == id
                    select d).SingleOrDefault();
        }
    }
}
