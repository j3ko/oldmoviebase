﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Common.Docs.Datasources;
using System.Data.Linq;
using J3ko.Common.Utils;

namespace J3ko.Common.Docs.Repositories.Linq
{
    public class LinqDocumentRepository : DocumentRepository<DocumentBO>
    {
        protected override void OnSave(DocumentBO entity, Foundation.Repository.Linq.LinqWorkUnit<Datasources.DocumentData> work)
        {
            document d = (from i in work.Context.documents
                          where i.document_id == entity.DocumentId
                          select i).SingleOrDefault() ?? new document();

            d.file_name = entity.FileName;
            d.file_size = entity.FileSize;
            d.file_ext = entity.FileExt;
            d.mime_type = entity.MimeType;
            d.source = entity.Source;

            if (entity.FileContent != null && entity.FileContent.Length > 0)
                d.file_content = new Binary(entity.FileContent.ToByteArray());

            if (d.document_id == Guid.Empty)
                work.Context.documents.InsertOnSubmit(d);

            work.Context.SubmitChanges();

            entity.DocumentId = d.document_id;
        }

        protected override void OnDelete(DocumentBO entity, Foundation.Repository.Linq.LinqWorkUnit<Datasources.DocumentData> work)
        {
            throw new NotImplementedException();
        }

        protected override IQueryable<DocumentBO> OnGetAll(Foundation.Repository.Linq.LinqWorkUnit<Datasources.DocumentData> work)
        {
            return from d in work.Context.documents
                   select new DocumentBO
                   {
                       DocumentId = d.document_id,
                       Bytes = d.file_content.ToArray(),
                       FileName = d.file_name,
                       FileExt = d.file_ext,
                       FileSize = d.file_size,
                       MimeType = d.mime_type,
                       Source = d.source,
                   };
        }
    }
}
