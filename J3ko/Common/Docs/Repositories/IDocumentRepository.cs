﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace J3ko.Common.Docs.Repositories
{
    public interface IDocumentRepository
    {
        DocumentBO GetDocument(Guid id);
        IQueryable<DocumentBO> GetDocuments();
        DocumentBO SaveDocument(DocumentBO doc);
    }
}
