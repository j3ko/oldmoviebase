﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Common.Docs.Datasources;
using J3ko.Foundation.Repository.Linq;

namespace J3ko.Common.Docs.Repositories
{
    public abstract class DocumentRepository<TEntity> : LinqRepositoryBase<TEntity, DocumentData>
        where TEntity : class
    {
        public DocumentRepository()
            : base(new DocumentContextFactory())
        { }


        protected abstract override void OnSave(TEntity entity, LinqWorkUnit<DocumentData> work);

        protected abstract override void OnDelete(TEntity entity, LinqWorkUnit<DocumentData> work);

        protected abstract override IQueryable<TEntity> OnGetAll(LinqWorkUnit<DocumentData> work);
    }
}
