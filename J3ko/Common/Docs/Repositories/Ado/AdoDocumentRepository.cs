﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Foundation;
using System.Data.SqlClient;
using System.IO;
using System.Data.SqlTypes;

namespace J3ko.Common.Docs.Repositories.Ado
{
    internal class AdoDocumentRepository : IDocumentRepository
    {
        private string _con;

        public AdoDocumentRepository()
            : this(BootStrapper.DefaultConnectionString)
        { }

        public AdoDocumentRepository(string connectionString)
        {
            _con = connectionString;
        }

        #region IDocumentRepository Members

        public DocumentBO GetDocument(Guid id)
        {
            //MemoryStream result = new MemoryStream();

            //using (SqlConnection connection = new SqlConnection(_con))
            //{
            //    connection.Open();
            //    SqlCommand command = new SqlCommand("", connection);

            //    SqlTransaction tran = connection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
            //    command.Transaction = tran;

            //    command.CommandText = "select top(1) local_path, transaction_context from common.document where document_id = @docid";
            //    command.Parameters.AddWithValue("docid", id);

            //    using (SqlDataReader reader = command.ExecuteReader())
            //    {
            //        while (reader.Read())
            //        {
            //            // Get the pointer for the file
            //            string path = reader.GetString(0);
            //            byte[] transactionContext = reader.GetSqlBytes(1).Buffer;

            //            // Create the SqlFileStream
            //            SqlFileStream fileStream = new SqlFileStream(path, (byte[])reader.GetValue(1), FileAccess.Read, FileOptions.SequentialScan, 0);

            //            fileStream.CopyTo(result);
            //            fileStream.Close();
            //        }
            //    }
            //    tran.Commit();
            //}

            //result.Position = 0;
            //return result;
            throw new NotImplementedException();
        }

        public IQueryable<DocumentBO> GetDocuments()
        {
            throw new NotImplementedException();
        }

        public DocumentBO SaveDocument(DocumentBO doc)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
