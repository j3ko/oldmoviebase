﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Web.Movies
{
    public enum MovieSource
    {
        IMDb,
        RottenTomatoes
    }

    public class MovieService : IMovieService
    {
        private MovieSource _source = MovieSource.IMDb;
        private string _apiKey = null;

        public MovieService(MovieSource source, string apiKey)
        {
            _apiKey = apiKey;
            _source = source;
        }

        private IMovieService Service
        {
            get
            {
                if (_source == MovieSource.IMDb && !string.IsNullOrEmpty(_apiKey))
                    return new IMDbMovieService(_apiKey);
                if (_source == MovieSource.RottenTomatoes && !string.IsNullOrEmpty(_apiKey))
                    return new RottenTomatoesMovieService(_apiKey);

                return new IMDbScraperService();
            }
        }

        #region IMovieService Members

        public IEnumerable<MovieBO> Search(string term)
        {
            return Service.Search(term);
        }

        public MovieBO GetMovie(string imdbId)
        {
            return Service.GetMovie(imdbId);
        }

        #endregion
    }
}
