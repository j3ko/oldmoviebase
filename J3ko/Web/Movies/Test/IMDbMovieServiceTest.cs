﻿using J3ko.Web.Movies;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace J3ko.Web.Movies.Test
{
    
    
    /// <summary>
    ///This is a test class for IMDbMovieServiceTest and is intended
    ///to contain all IMDbMovieServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IMDbMovieServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Search
        ///</summary>
        [TestMethod()]
        public void SearchTest()
        {
            IMovieService target = new MovieService(MovieSource.IMDb, "");
            string term = "Transformers";
            IEnumerable<MovieBO> expected = null;
            IEnumerable<MovieBO> actual;
            actual = target.Search(term);
            Assert.AreNotEqual(expected, actual);
        }
    }
}
