﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Web.Movies
{
    public interface IMovieService
    {
        IEnumerable<MovieBO> Search(string term);
        MovieBO GetMovie(string imdbId);
    }
}
