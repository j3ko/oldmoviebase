﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Web.Movies
{
    public enum MoviePersonRole
    {
        Actor,
        Director,
        Producer,
        Other,
    }
    public class MoviePersonBO
    {
        public int? TmdbId { get; set; }
        public string Name { get; set; }
        public string CharacterName { get; set; }
        public string ThumbnailUrl { get; set; }
        public int Ordinal { get; set; }
        public MoviePersonRole Role { get; set; }
    }
}
