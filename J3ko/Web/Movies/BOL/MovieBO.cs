﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Web.Movies
{
    public class MovieBO
    {
        public string MovieUrl { get; set; }
        public string Title { get; set; }
        public string Plot { get; set; }
        public string ImdbId { get; set; }
        public DateTime Released { get; set; }

        private IList<MovieGenreBO> _genres;
        public IList<MovieGenreBO> Genres
        {
            get
            {
                if (_genres == null)
                    _genres = new List<MovieGenreBO>();
                return _genres;
            }
            set
            {
                _genres = value;
            }
        }

        private IList<string> _altTitles;
        public IList<string> AltTitles
        {
            get
            {
                if (_altTitles == null)
                    _altTitles = new List<string>();
                return _altTitles;
            }
            set
            {
                _altTitles = value;
            }
        }
        
        private IList<MoviePostersBO> _posters;
        public IList<MoviePostersBO> Posters
        {
            get
            {
                if (_posters == null)
                    _posters = new List<MoviePostersBO>();
                return _posters;
            }
            set
            {
                _posters = value;
            }
        }

        private IList<MoviePersonBO> _people;
        public IList<MoviePersonBO> People
        {
            get
            {
                if (_people == null)
                    _people = new List<MoviePersonBO>();
                return _people;
            }
            set
            {
                _people = value;
            }
        }

        #region object overrides
        public override bool Equals(object obj)
        {
            if (obj is MovieBO)
                return ((MovieBO)obj).ImdbId == this.ImdbId;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + ImdbId.GetHashCode();
            return hash;
        }
        #endregion
    }
}
