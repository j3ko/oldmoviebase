﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Web.Movies
{
    public class MoviePostersBO
    {
        public string PosterUrl { get; set; }
        public string SmallPosterUrl { get; set; }
    }
}
