﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Web.Movies
{
    public class MovieGenreBO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
