﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Web.Movies.RottenTomatoes;
using J3ko.Diagnostics;

namespace J3ko.Web.Movies
{
    internal class RottenTomatoesMovieService : IMovieService
    {
        RottenTomatoesReader _reader;

        public RottenTomatoesMovieService(string apiKey)
        {
            _reader = RottenTomatoesReader.GetInstance(apiKey);
        }

        #region IMovieService Members

        public IEnumerable<MovieBO> Search(string term)
        {
            List<MovieBO> result = new List<MovieBO>();

            try
            {
                IEnumerable<RottenTomatoMovie> searchResult = _reader.Search(term);

                foreach (RottenTomatoMovie entry in searchResult)
                {
                    MovieBO temp = MapMovieBO(entry);
                    result.Add(temp);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }

            return result;
        }

        public MovieBO GetMovie(string imdbId)
        {
            throw new NotImplementedException();
        }
        #endregion

        private MovieBO MapMovieBO(RottenTomatoMovie movie)
        {
            MovieBO result = new MovieBO();

            return result;
        }
    }
}
