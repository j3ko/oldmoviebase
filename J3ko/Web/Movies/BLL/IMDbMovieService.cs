﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Web.Movies.IMDb;
using J3ko.Diagnostics;

namespace J3ko.Web.Movies
{
    internal class IMDbMovieService : IMovieService
    {
        private IMDbReader _reader;

        public IMDbMovieService(string apiKey)
        {
            //_reader = IMDbAPIv2Reader.GetInstance(apiKey);
            _reader = IMDbAPIv3Reader.GetInstance(apiKey);
        }

        #region IMovieService Members

        public IEnumerable<MovieBO> Search(string term)
        {
            List<MovieBO> result = new List<MovieBO>();
            try
            {
                result = _reader.Search(term).ToList();
            }
            catch (Exception ex)
            {
                Logger.Log(ex, LogSeverity.Warn, LogType.Database);
            }

            return result;
        }

        public MovieBO GetMovie(string imdbId)
        {
            MovieBO result = null;

            try
            {
                result = _reader.GetMovie(imdbId);
            }
            catch (Exception ex)
            {
                Logger.Log(ex, LogSeverity.Warn, LogType.Database);
            }

            return result;
        }

        #endregion
    }
}
