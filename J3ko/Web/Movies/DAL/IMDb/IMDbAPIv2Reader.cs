﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheMovieDB;
using System.Web;
using J3ko.Diagnostics;

namespace J3ko.Web.Movies.IMDb
{
    internal class IMDbAPIv2Reader : IMDbReader
    {
        private TmdbAPI _api;
        private static volatile IMDbAPIv2Reader _reader;
        private static object syncRoot = new Object();

        private IMDbAPIv2Reader(string apiKey)
        {
            _api = new TmdbAPI(apiKey);
        }

        public static IMDbAPIv2Reader GetInstance(string apiKey)
        {
            lock (syncRoot)
            {
                if (_reader == null)
                    _reader = new IMDbAPIv2Reader(apiKey);
            }
            return _reader;
        }


        #region IIMDbReader Members

        public override IEnumerable<MovieBO> Search(string title)
        {
            List<MovieBO> result = new List<MovieBO>();

            IEnumerable<TmdbMovie> movies = GetMovies(title);
            foreach (TmdbMovie movie in movies)
            {
                result.Add(ReadEntry(movie));
            }

            return result;
        }

        public override MovieBO GetMovie(string imdbId)
        {
            TmdbMovie mov = GetMovieByImdbId(imdbId);
            return ReadEntry(mov);
        }

        #endregion

        private MovieBO ReadEntry(TmdbMovie entry)
        {
            TmdbMovie movieInfo = GetMovieById(entry.Id);
            TmdbMovie movieImages = GetMovieImages(entry);

            MovieBO result = new MovieBO()
            {
                ImdbId = entry.ImdbId,
                MovieUrl = entry.Url,
                Released = entry.Released.GetValueOrDefault(DateTime.Now),
                Posters = GetPosters(movieImages),
                Genres = GetGenres(movieInfo),
                AltTitles = GetAltNames(movieInfo),
                People = GetPeople(movieInfo),
                Title = HttpUtility.HtmlDecode(entry.Name),
                Plot = HttpUtility.HtmlDecode(entry.Overview),
            };

            return result;
        }

        private List<MoviePersonBO> GetPeople(TmdbMovie movie)
        {
            List<MoviePersonBO> result = new List<MoviePersonBO>();

            if (movie == null || movie.Cast == null)
                return result;

            int i = 0;
            foreach (TmdbCastPerson c in movie.Cast)
            {
                result.Add(new MoviePersonBO()
                {
                    TmdbId = c.Id,
                    Name = c.Name,
                    CharacterName = c.Character,
                    ThumbnailUrl = c.Thumb,
                    Ordinal = i++,
                    Role = GetRole(c.Department, c.Job),
                });
            }

            return result;
        }

        private MoviePersonRole GetRole(string department, string job)
        {
            MoviePersonRole result = MoviePersonRole.Other;

            if (job == null)
                return result;

            switch (job.ToLower())
            {
                case "actor":
                    result = MoviePersonRole.Actor;
                    break;
                case "director":
                    result = MoviePersonRole.Director;
                    break;
                case "producer":
                    result = MoviePersonRole.Producer;
                    break;
                case "executive producer":
                    result = MoviePersonRole.Producer;
                    break;

            }

            return result;
        }

        private List<string> GetAltNames(TmdbMovie movie)
        {
            List<string> result = new List<string>();

            if (movie == null)
                return result;

            if(!string.IsNullOrWhiteSpace(movie.AlternativeName))
                result.Add(HttpUtility.HtmlDecode(movie.AlternativeName));

            return result;
        }

        private List<MovieGenreBO> GetGenres(TmdbMovie movie)
        {
            List<MovieGenreBO> result = new List<MovieGenreBO>();

            if (movie == null || movie.Categories == null)
                return result;

            IEnumerable<TmdbCategory> cats = movie.Categories.Where(x => !string.IsNullOrWhiteSpace(x.Type) 
                && x.Type.StartsWith("genre", StringComparison.CurrentCultureIgnoreCase));

            foreach (TmdbCategory cat in cats)
            {
                if (!string.IsNullOrWhiteSpace(cat.Name))
                    result.Add(new MovieGenreBO()
                    {
                        Id = cat.Id,
                        Name = HttpUtility.HtmlDecode(cat.Name),
                    });
            }
            return result;
        }

        private List<MoviePostersBO> GetPosters(TmdbMovie movie)
        {
            List<MoviePostersBO> result = new List<MoviePostersBO>();

            if (movie == null || movie.Images == null)
                return result;

            foreach (TmdbImage thumb in movie.Images.Where(x => x.Type == TmdbImageType.poster && x.Size == TmdbImageSize.w154))
            {
                TmdbImage smallCover = thumb;
                TmdbImage cover = movie.Images.Where(x => x.Id == thumb.Id && x.Size == TmdbImageSize.w342).FirstOrDefault();

                if (smallCover == null || cover == null)
                    continue;

                result.Add(new MoviePostersBO()
                {
                    SmallPosterUrl = smallCover.Url,
                    PosterUrl = cover.Url,
                });
            }

            return result;
        }

        #region Service Methods
        private TmdbMovie[] GetMovies(string title)
        {            
            string term = HttpUtility.UrlEncode(title);
            TmdbMovie[] result = new TmdbMovie[0];

            try
            {
                result = _api.MovieSearch(term);
            }
            catch (Exception ex)
            {
                Logger.Log(ex, LogSeverity.Warn, LogType.Database);
            }

            return result;
        }

        private TmdbMovie GetMovieById(int tmdbId)
        {
            TmdbMovie result = new TmdbMovie();
            try
            {
                result = _api.GetMovieInfo(tmdbId);
            }
            catch (Exception ex)
            {
                Logger.Log(ex, LogSeverity.Warn, LogType.Database);
            }
            return result;
        }

        private TmdbMovie GetMovieByImdbId(string imdbId)
        {
            TmdbMovie result = new TmdbMovie();
            try
            {
                result = _api.MovieSearchByImdb(imdbId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logger.Log(ex, LogSeverity.Warn, LogType.Database);
            }
            return result;
        }

        private TmdbMovie GetMovieImages(TmdbMovie movie)
        {
            TmdbMovie result = new TmdbMovie();
            try
            {
                result = _api.GetMovieImages(movie.ImdbId);
                List<TmdbImage> images = new List<TmdbImage>(result.Images);

                foreach (TmdbImage img in movie.Images)
                {
                    if (!images.Where(x => x.Url == img.Url).Any())
                        images.Add(img);
                }

                result.Images = images.ToArray();
            }
            catch (Exception ex)
            {
                Logger.Log(ex, LogSeverity.Warn, LogType.Database);
            }
            return result;
        }
        #endregion
    }
}
