﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WatTmdb.V3;
using System.Web;

namespace J3ko.Web.Movies
{
    internal class IMDbAPIv3Reader : IMDbReader
    {
        private Tmdb _api;
        private static volatile IMDbAPIv3Reader _reader;
        private static object syncRoot = new Object();
        private string BASE_URL = "http://cf2.imgobject.com/t/p/";
        private string SMALL_POSTER_SIZE = "w154";
        private string POSTER_SIZE = "w342";
        private string PROFILE_SIZE = "w45";

        private IMDbAPIv3Reader(string apiKey)
        {
            _api = new Tmdb(apiKey);
            TmdbConfiguration config = _api.GetConfiguration();

            BASE_URL = config.images.base_url;
        }

        public static IMDbAPIv3Reader GetInstance(string apiKey)
        {
            lock (syncRoot)
            {
                if (_reader == null)
                    _reader = new IMDbAPIv3Reader(apiKey);
            }
            return _reader;
        }

        public override IEnumerable<MovieBO> Search(string title)
        {
            List<MovieBO> result = new List<MovieBO>();

            TmdbMovieSearch movies = _api.SearchMovie(title, 1);

            foreach (MovieResult movie in movies.results)
            {
                result.Add(ReadEntry(movie));
            }

            return result;
        }

        public override MovieBO GetMovie(string imdbId)
        {
            TmdbMovie temp = _api.GetMovieByIMDB(imdbId);
            return BindMovie(temp);
        }

        private MovieBO ReadEntry(MovieResult entry)
        {
            TmdbMovie temp = _api.GetMovieInfo(entry.id);
            return BindMovie(temp);
        }

        private MovieBO BindMovie(TmdbMovie entry)
        {
            DateTime released = DateTime.Now;
            DateTime.TryParse(entry.release_date, out released);

            MovieBO result = new MovieBO()
            {
                ImdbId = entry.imdb_id,
                MovieUrl = entry.homepage,
                Released = released,
                Posters = GetPosters(entry),
                Genres = GetGenres(entry.genres),
                AltTitles = GetAltNames(entry),
                People = GetPeople(entry),
                Title = HttpUtility.HtmlDecode(entry.title),
                Plot = HttpUtility.HtmlDecode(entry.overview),
            };

            return result;
        }

        private List<MoviePersonBO> GetPeople(TmdbMovie entry)
        {
            List<MoviePersonBO> result = new List<MoviePersonBO>();
            TmdbMovieCast temp = _api.GetMovieCast(entry.id);

            int order = 0;

            foreach (Cast c in temp.cast.OrderBy(x => x.order))
            {
                result.Add(new MoviePersonBO()
                {
                    Name = c.name,
                    Ordinal = order++,
                    CharacterName = c.character,
                    Role = MoviePersonRole.Actor,
                    TmdbId = c.id,
                    ThumbnailUrl = string.IsNullOrWhiteSpace(c.profile_path) ? null : string.Format("{0}{1}{2}", BASE_URL, PROFILE_SIZE, c.profile_path),
                });
            }

            foreach (Crew c in temp.crew)
            {
                result.Add(new MoviePersonBO()
                {
                    Name = c.name,
                    Ordinal = order++,
                    Role = GetRole(c.department, c.job),
                    TmdbId = c.id,
                    ThumbnailUrl = string.IsNullOrWhiteSpace(c.profile_path) ? null : string.Format("{0}{1}{2}", BASE_URL, PROFILE_SIZE, c.profile_path),
                });
            }

            return result;
        }

        private MoviePersonRole GetRole(string department, string job)
        {
            MoviePersonRole result = MoviePersonRole.Other;

            if (job == null)
                return result;

            switch (job.ToLower())
            {
                case "actor":
                    result = MoviePersonRole.Actor;
                    break;
                case "director":
                    result = MoviePersonRole.Director;
                    break;
                case "producer":
                    result = MoviePersonRole.Producer;
                    break;
                case "executive producer":
                    result = MoviePersonRole.Producer;
                    break;

            }

            return result;
        }

        private List<MoviePostersBO> GetPosters(TmdbMovie entry)
        {
            List<MoviePostersBO> result = new List<MoviePostersBO>();
            TmdbMovieImages temp = _api.GetMovieImages(entry.id);

            foreach (Poster p in temp.posters)
            {
                if (string.IsNullOrWhiteSpace(p.file_path))
                    continue;

                result.Add(new MoviePostersBO()
                {
                    PosterUrl = string.Format("{0}{1}{2}", BASE_URL, POSTER_SIZE, p.file_path),
                    SmallPosterUrl = string.Format("{0}{1}{2}", BASE_URL, SMALL_POSTER_SIZE, p.file_path),
                });
            }

            return result;
        }

        private List<string> GetAltNames(TmdbMovie entry)
        {
            List<string> result = new List<string>();

            TmdbMovieAlternateTitles temp = _api.GetMovieAlternateTitles(entry.id, "US");

            foreach (AlternateTitle t in temp.titles)
            {
                result.Add(t.title);
            }

            return result;
        }

        private List<MovieGenreBO> GetGenres(IEnumerable<MovieGenre> entries)
        {
            List<MovieGenreBO> result = new List<MovieGenreBO>();

            if (entries == null)
                return result;

            foreach (MovieGenre g in entries)
            {
                result.Add(new MovieGenreBO
                {
                    Id = g.id,
                    Name = g.name,
                });
            }

            return result;
        }
    }
}
