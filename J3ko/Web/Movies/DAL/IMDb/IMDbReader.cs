﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Web.Movies
{
    internal abstract class IMDbReader
    {
        public abstract IEnumerable<MovieBO> Search(string title);
        public abstract MovieBO GetMovie(string imdbId);
    }
}
