﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Net;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

/*******************************************************************************
 * Free ASP.net IMDb Scraper API for the new IMDb Template.
 * Author: Abhinay Rathore
 * Website: http://www.AbhinayRathore.com
 * Blog: http://web3o.blogspot.com
 * More Info: http://web3o.blogspot.com/2010/11/aspnetc-imdb-scraping-api.html
 * Last Updated: Aug 15, 2011
 *******************************************************************************/

namespace IMDb_Scraper
{
    public class IMDbEntry
    {
        public bool status { get; set; }
        public string Id { get; set; }
        public string Title { get; set; }
        public string OriginalTitle { get; set; }
        public string Year { get; set; }
        public string Rating { get; set; }
        public ArrayList Genres { get; set; }
        public ArrayList Directors { get; set; }
        public ArrayList Writers { get; set; }
        public ArrayList Stars { get; set; }
        public ArrayList Cast { get; set; }
        public string MpaaRating { get; set; }
        public string ReleaseDate { get; set; }
        public string Plot { get; set; }
        public string Poster { get; set; }
        public string PosterLarge { get; set; }
        public string PosterSmall { get; set; }
        public string PosterFull { get; set; }
        public string Runtime { get; set; }
        public string Top250 { get; set; }
        public string Oscars { get; set; }
        public string Awards { get; set; }
        public string Nominations { get; set; }
        public string Storyline { get; set; }
        public string Tagline { get; set; }
        public string Votes { get; set; }
        public ArrayList Languages { get; set; }
        public ArrayList Countries { get; set; }
        public ArrayList ReleaseDates { get; set; }
        public ArrayList MediaImages { get; set; }
        public string ImdbURL { get; set; }
    }

    public class IMDbScraper
    {


        //Search Engine URLs
        private string GoogleSearch = "http://www.google.com/search?q=imdb+";
        private string BingSearch = "http://www.bing.com/search?q=imdb+";
        private string AskSearch = "http://www.ask.com/web?q=imdb+";

        //Constructor
        public IEnumerable<IMDbEntry> Scrape(string MovieName, bool GetExtraInfo = true)
        {
            List<IMDbEntry> result = new List<IMDbEntry>();
            string[] imdbUrl = getIMDbUrl(System.Uri.EscapeUriString(MovieName));

            foreach (string str in imdbUrl)
            {
                if (!(imdbUrl == null) && imdbUrl.Length > 0)
                {
                    string html = getUrlData(str);
                    result.Add(parseIMDbPage(html, GetExtraInfo));
                }
            }

            return result;
        }

        //Get IMDb URL from search results
        private string[] getIMDbUrl(string MovieName, string searchEngine = "google")
        {
            string url = GoogleSearch + MovieName; //default to Google search
            if (searchEngine.ToLower().Equals("bing")) url = BingSearch + MovieName;
            if (searchEngine.ToLower().Equals("ask")) url = AskSearch + MovieName;
            string html = getUrlData(url);
            string[] imdbUrls = (string[])matchAll(@"<a href=""(http://www.imdb.com/title/tt\d{7}/)"".*?>.*?</a>", html).ToArray(typeof(string));
            if (imdbUrls.Length > 0)
                return imdbUrls; //return first IMDb result
            else if (searchEngine.ToLower().Equals("google")) //if Google search fails
                return getIMDbUrl(MovieName, "bing"); //search using Bing
            else if (searchEngine.ToLower().Equals("bing")) //if Bing search fails
                return getIMDbUrl(MovieName, "ask"); //search using Ask
            else //search fails
                return new string[0];
        }

        //Parse IMDb page data
        private IMDbEntry parseIMDbPage(string html, bool GetExtraInfo)
        {
            IMDbEntry result = new IMDbEntry();

            result.Id = match(@"<link rel=""canonical"" href=""http://www.imdb.com/title/(tt\d{7})/"" />", html);
            if (!string.IsNullOrEmpty(result.Id))
            {
                result.status = true;
                result.Title = match(@"<title>(IMDb \- )*(.*?) \(.*?</title>", html, 2);
                result.OriginalTitle = match(@"title-extra"">(.*?)<", html);
                result.Year = match(@"<title>.*?\(.*?(\d{4}).*?\).*?</title>", html);
                result.Rating = match(@"ratingValue"">(\d.\d)<", html);
                result.Genres = new ArrayList();
                result.Genres = matchAll(@"<a.*?>(.*?)</a>", match(@"Genres:</h4>(.*?)</div>", html));
                result.Directors = new ArrayList();
                result.Directors = matchAll(@"<a.*?>(.*?)</a>", match(@"Directors?:[\n\r\s]*</h4>(.*?)(</div>|>.?and )", html));
                result.Writers = matchAll(@"<a.*?>(.*?)</a>", match(@"Writers?:[\n\r\s]*</h4>(.*?)(</div>|>.?and )", html));
                result.Stars = matchAll(@"<a.*?>(.*?)</a>", match(@"Stars?:(.*?)</div>", html));
                result.Cast = matchAll(@"class=""name"">[\n\r\s]*<a.*?>(.*?)</a>", html);
                result.Plot = match(@"<p itemprop=""description"">(.*?)</p>", html);
                result.ReleaseDate = match(@"Release Date:</h4>.*?(\d{1,2} (January|February|March|April|May|June|July|August|September|October|November|December) (19|20)\d{2}).*(\(|<span)", html);
                result.Runtime = match(@"Runtime:</h4>[\s]*.*?(\d{1,4}) min[\s]*.*?\<\/div\>", html);
                if (String.IsNullOrEmpty(result.Runtime)) result.Runtime = match(@"infobar.*?([0-9]+) min.*?</div>", html);
                result.Top250 = match(@"Top 250 #(\d{1,3})<", html);
                result.Oscars = match(@"Won (\d{1,2}) Oscars\.", html);
                result.Awards = match(@"(\d{1,4}) wins", html);
                result.Nominations = match(@"(\d{1,4}) nominations", html);
                result.Storyline = match(@"Storyline</h2>[\s]*<p>(.*?)[\s]*(<em|</p>)", html);
                result.Tagline = match(@"Taglines?:</h4>(.*?)(<span|</div)", html);
                result.MpaaRating = match(@"infobar"">.*?<img.*?alt=""(.*?)"" src="".*?certificates.*?"".*?>", html);
                result.Votes = match(@"href=""ratings"".*?>(\d+,?\d*)</span> votes</a>", html);
                result.Languages = new ArrayList();
                result.Languages = matchAll(@"<a.*?>(.*?)</a>", match(@"Language.?:(.*?)(</div>|>.?and )", html));
                result.Countries = new ArrayList();
                result.Countries = matchAll(@"<a.*?>(.*?)</a>", match(@"Country:(.*?)(</div>|>.?and )", html));
                result.Poster = match(@"img_primary"">[\n\r\s]*?<a.*?><img src=""(.*?)"".*?</td>", html);
                if (!string.IsNullOrEmpty(result.Poster) && result.Poster.IndexOf("nopicture") < 0)
                {
                    result.PosterSmall = result.Poster.Substring(0, result.Poster.IndexOf("_V1.")) + "_V1._SY150.jpg";
                    result.PosterLarge = result.Poster.Substring(0, result.Poster.IndexOf("_V1.")) + "_V1._SY500.jpg";
                    result.PosterFull = result.Poster.Substring(0, result.Poster.IndexOf("_V1.")) + "_V1._SY0.jpg";
                }
                else
                {
                    result.Poster = string.Empty;
                    result.PosterSmall = string.Empty;
                    result.PosterLarge = string.Empty;
                    result.PosterFull = string.Empty;
                }
                result.ImdbURL = "http://www.imdb.com/title/" + result.Id + "/";
                if (GetExtraInfo)
                {
                    result.ReleaseDates = getReleaseDates(result.Id);
                    result.MediaImages = getMediaImages(result.Id);
                }
            }

            return result;
        }

        //Get all release dates
        private ArrayList getReleaseDates(string id)
        {
            ArrayList list = new ArrayList();
            string releasehtml = getUrlData("http://www.imdb.com/title/" + id + "/releaseinfo");
            foreach (string r in matchAll(@"<tr>(.*?)</tr>", match(@"Date</th></tr>\n*?(.*?)</table>", releasehtml)))
            {
                Match rd = new Regex(@"<td>(.*?)</td>\n*?.*?<td align=""right"">(.*?)</td>", RegexOptions.Multiline).Match(r);
                list.Add(StripHTML(rd.Groups[1].Value.Trim()) + " = " + StripHTML(rd.Groups[2].Value.Trim()));
            }
            return list;
        }

        //Get all media images
        private ArrayList getMediaImages(string id)
        {
            ArrayList list = new ArrayList();
            string mediaurl = "http://www.imdb.com/title/" + id + "/mediaindex";
            string mediahtml = getUrlData(mediaurl);
            int pagecount = matchAll(@"<a href=""\?page=(.*?)"">", match(@"<span style=""padding: 0 1em;"">(.*?)</span>", mediahtml)).Count;
            for (int p = 1; p <= pagecount + 1; p++)
            {
                mediahtml = getUrlData(mediaurl + "?page=" + p);
                foreach (Match m in new Regex(@"src=""(.*?)""", RegexOptions.Multiline).Matches(match(@"<div class=""thumb_list"" style=""font-size: 0px;"">(.*?)</div>", mediahtml)))
                {
                    String image = m.Groups[1].Value;
                    image = image.Substring(0, image.IndexOf("_V1.")) + "_V1._SY500.jpg";
                    list.Add(image);
                }
            }
            return list;
        }

        //Match single instance
        private string match(string regex, string html, int i = 1)
        {
            return new Regex(regex, RegexOptions.Multiline).Match(html).Groups[i].Value.Trim();
        }

        //Match all instances and return as ArrayList
        private ArrayList matchAll(string regex, string html, int i = 1)
        {
            ArrayList list = new ArrayList();
            foreach (Match m in new Regex(regex, RegexOptions.Multiline).Matches(html))
                list.Add(m.Groups[i].Value.Trim());
            return list;
        }

        //Strip HTML Tags
        private string StripHTML(string inputString)
        {
            return Regex.Replace(inputString, @"<.*?>", string.Empty);
        }

        //Get URL Data
        private string getUrlData(string url)
        {
            WebClient client = new WebClient();
            Random r = new Random();
            //Random IP Address
            client.Headers["X-Forwarded-For"] = r.Next(0, 255) + "." + r.Next(0, 255) + "." + r.Next(0, 255) + "." + r.Next(0, 255);
            //Random User-Agent
            client.Headers["User-Agent"] = "Mozilla/" + r.Next(3, 5) + ".0 (Windows NT " + r.Next(3, 5) + "." + r.Next(0, 2) + "; rv:2.0.1) Gecko/20100101 Firefox/" + r.Next(3, 5) + "." + r.Next(0, 5) + "." + r.Next(0, 5);
            Stream datastream = client.OpenRead(url);
            StreamReader reader = new StreamReader(datastream);
            StringBuilder sb = new StringBuilder();
            while (!reader.EndOfStream)
                sb.Append(reader.ReadLine());
            return sb.ToString();
        }
    }
}