﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMDb_Scraper;
using J3ko.Common.Utils;
using System.Web;

namespace J3ko.Web.Movies.IMDb
{
    internal class IMDbScrapeReader : IMDbReader
    {
        private static volatile IMDbScrapeReader _reader;
        private static object syncRoot = new Object();

        private IMDbScrapeReader()
        { }

        public static IMDbScrapeReader Instance
        {
            get
            {
                lock (syncRoot)
                {
                    if (_reader == null)
                        _reader = new IMDbScrapeReader();
                }
                return _reader;
            }
        }


        #region IIMDbEngine Members

        public override IEnumerable<MovieBO> Search(string title)
        {
            List<MovieBO> result = new List<MovieBO>();

            title = SanitizeSearchString(title);

            if (string.IsNullOrWhiteSpace(title))
                return result;

            lock (syncRoot)
            {                
                IMDbScraper scraper = new IMDbScraper();
                List<IMDbEntry> entries = scraper.Scrape(title, true).ToList();

                foreach (IMDbEntry entry in entries)
                {
                    result.Add(ReadEntry(entry));
                }
            }

            return result;
        }

        public override MovieBO GetMovie(string imdbId)
        {
            throw new NotImplementedException();
        }

        #endregion

        private MovieBO ReadEntry(IMDbEntry entry)
        {
            MovieBO result = new MovieBO()
            {
                ImdbId = entry.Id,
                MovieUrl = entry.ImdbURL,
                Title = HttpUtility.HtmlDecode(entry.Title),
                Posters = new List<MoviePostersBO>() 
                { 
                    new MoviePostersBO() 
                    { 
                        SmallPosterUrl = entry.PosterSmall,
                        PosterUrl = entry.Poster,
                    }
                },
                Plot = HttpUtility.HtmlDecode(entry.Plot),
            };

            return result;
        }

        private string SanitizeSearchString(string str)
        {
            if (str != null)
                str = HttpUtility.UrlEncodeUnicode(str);
            return str;            
        }        
    }
}
