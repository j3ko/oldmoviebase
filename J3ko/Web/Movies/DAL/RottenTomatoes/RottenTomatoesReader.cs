﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;

namespace J3ko.Web.Movies.RottenTomatoes
{
    internal class RottenTomatoesReader
    {
        private const string BASE_URL = @"http://api.rottentomatoes.com/api/public/v1.0";

        private static volatile RottenTomatoesReader _reader;
        private static volatile string _apiKey;
        private static object syncRoot = new Object();


        private RottenTomatoesReader() { }

        public static RottenTomatoesReader GetInstance(string apiKey)
        {
            lock (syncRoot)
            {
                if (_reader == null)
                {
                    _reader = new RottenTomatoesReader();
                    _apiKey = apiKey;
                }
            }
            return _reader;
        }

        public IEnumerable<RottenTomatoMovie> Search(string term)
        {
            List<RottenTomatoMovie> result = new List<RottenTomatoMovie>();
            RestClient client = new RestClient(BASE_URL);
            RestRequest request = new RestRequest("movies.json");
            request.AddParameter(@"apikey", _apiKey);
            request.AddParameter(@"q", term);
            request.AddParameter(@"page_limit", 10);
            request.AddParameter(@"page", 1);

            var response = client.Execute(request);

            return result;
        }
    }
}
