﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Foundation.Repository.Linq;
using J3ko.Foundation;

namespace J3ko.Diagnostics.Datasources
{
    public class DiagnosticsContextFactory : IDataContextFactory<DiagnosticsData>
    {
        #region IDataContextFactory<DiagnosticsData> Members

        public DiagnosticsData Create()
        {
            return new DiagnosticsData(BootStrapper.DefaultConnectionString);
        }

        #endregion
    }
}
