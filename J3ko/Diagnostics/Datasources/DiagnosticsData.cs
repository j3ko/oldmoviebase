using J3ko.Foundation;
using System.Data.Linq;

namespace J3ko.Diagnostics.Datasources
{
    partial class DiagnosticsData
    {
        public DiagnosticsData()
            : this(BootStrapper.DefaultConnectionString)
        { }

        public override void SubmitChanges(ConflictMode failureMode)
        {
            AuditUtility.ProcessAuditFields(GetChangeSet().Updates);
            AuditUtility.ProcessAuditFields(GetChangeSet().Inserts);
            base.SubmitChanges(failureMode);
        }
    }
}
