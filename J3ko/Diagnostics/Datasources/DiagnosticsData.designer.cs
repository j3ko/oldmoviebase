﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.235
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace J3ko.Diagnostics.Datasources
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="j3ko")]
	public partial class DiagnosticsData : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void Insertlog(log instance);
    partial void Updatelog(log instance);
    partial void Deletelog(log instance);
    #endregion
		
		public DiagnosticsData(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DiagnosticsData(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DiagnosticsData(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DiagnosticsData(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<log> logs
		{
			get
			{
				return this.GetTable<log>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="diagnostics.[log]")]
	public partial class log : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _log_id;
		
		private string _app_name;
		
		private string _app_version;
		
		private string _source;
		
		private string _message;
		
		private string _stacktrace;
		
		private int _severity;
		
		private System.DateTime _modified;
		
		private string _modified_by;
		
		private System.Data.Linq.Binary _row_version;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void Onlog_idChanging(int value);
    partial void Onlog_idChanged();
    partial void Onapp_nameChanging(string value);
    partial void Onapp_nameChanged();
    partial void Onapp_versionChanging(string value);
    partial void Onapp_versionChanged();
    partial void OnsourceChanging(string value);
    partial void OnsourceChanged();
    partial void OnmessageChanging(string value);
    partial void OnmessageChanged();
    partial void OnstacktraceChanging(string value);
    partial void OnstacktraceChanged();
    partial void OnseverityChanging(int value);
    partial void OnseverityChanged();
    partial void OnmodifiedChanging(System.DateTime value);
    partial void OnmodifiedChanged();
    partial void Onmodified_byChanging(string value);
    partial void Onmodified_byChanged();
    partial void Onrow_versionChanging(System.Data.Linq.Binary value);
    partial void Onrow_versionChanged();
    #endregion
		
		public log()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_log_id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true, UpdateCheck=UpdateCheck.Never)]
		public int log_id
		{
			get
			{
				return this._log_id;
			}
			set
			{
				if ((this._log_id != value))
				{
					this.Onlog_idChanging(value);
					this.SendPropertyChanging();
					this._log_id = value;
					this.SendPropertyChanged("log_id");
					this.Onlog_idChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_app_name", DbType="NVarChar(256) NOT NULL", CanBeNull=false, UpdateCheck=UpdateCheck.Never)]
		public string app_name
		{
			get
			{
				return this._app_name;
			}
			set
			{
				if ((this._app_name != value))
				{
					this.Onapp_nameChanging(value);
					this.SendPropertyChanging();
					this._app_name = value;
					this.SendPropertyChanged("app_name");
					this.Onapp_nameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_app_version", DbType="VarChar(50) NOT NULL", CanBeNull=false, UpdateCheck=UpdateCheck.Never)]
		public string app_version
		{
			get
			{
				return this._app_version;
			}
			set
			{
				if ((this._app_version != value))
				{
					this.Onapp_versionChanging(value);
					this.SendPropertyChanging();
					this._app_version = value;
					this.SendPropertyChanged("app_version");
					this.Onapp_versionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_source", DbType="NVarChar(MAX)", UpdateCheck=UpdateCheck.Never)]
		public string source
		{
			get
			{
				return this._source;
			}
			set
			{
				if ((this._source != value))
				{
					this.OnsourceChanging(value);
					this.SendPropertyChanging();
					this._source = value;
					this.SendPropertyChanged("source");
					this.OnsourceChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_message", DbType="NVarChar(MAX) NOT NULL", CanBeNull=false, UpdateCheck=UpdateCheck.Never)]
		public string message
		{
			get
			{
				return this._message;
			}
			set
			{
				if ((this._message != value))
				{
					this.OnmessageChanging(value);
					this.SendPropertyChanging();
					this._message = value;
					this.SendPropertyChanged("message");
					this.OnmessageChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_stacktrace", DbType="NVarChar(MAX)", UpdateCheck=UpdateCheck.Never)]
		public string stacktrace
		{
			get
			{
				return this._stacktrace;
			}
			set
			{
				if ((this._stacktrace != value))
				{
					this.OnstacktraceChanging(value);
					this.SendPropertyChanging();
					this._stacktrace = value;
					this.SendPropertyChanged("stacktrace");
					this.OnstacktraceChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_severity", DbType="Int NOT NULL", UpdateCheck=UpdateCheck.Never)]
		public int severity
		{
			get
			{
				return this._severity;
			}
			set
			{
				if ((this._severity != value))
				{
					this.OnseverityChanging(value);
					this.SendPropertyChanging();
					this._severity = value;
					this.SendPropertyChanged("severity");
					this.OnseverityChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_modified", DbType="DateTime NOT NULL", UpdateCheck=UpdateCheck.Never)]
		public System.DateTime modified
		{
			get
			{
				return this._modified;
			}
			set
			{
				if ((this._modified != value))
				{
					this.OnmodifiedChanging(value);
					this.SendPropertyChanging();
					this._modified = value;
					this.SendPropertyChanged("modified");
					this.OnmodifiedChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_modified_by", DbType="NVarChar(256) NOT NULL", CanBeNull=false, UpdateCheck=UpdateCheck.Never)]
		public string modified_by
		{
			get
			{
				return this._modified_by;
			}
			set
			{
				if ((this._modified_by != value))
				{
					this.Onmodified_byChanging(value);
					this.SendPropertyChanging();
					this._modified_by = value;
					this.SendPropertyChanged("modified_by");
					this.Onmodified_byChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_row_version", AutoSync=AutoSync.Always, DbType="rowversion NOT NULL", CanBeNull=false, IsDbGenerated=true, IsVersion=true, UpdateCheck=UpdateCheck.Never)]
		public System.Data.Linq.Binary row_version
		{
			get
			{
				return this._row_version;
			}
			set
			{
				if ((this._row_version != value))
				{
					this.Onrow_versionChanging(value);
					this.SendPropertyChanging();
					this._row_version = value;
					this.SendPropertyChanged("row_version");
					this.Onrow_versionChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
