﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Diagnostics.Repositories;
using J3ko.Diagnostics.Repositories.Linq;

namespace J3ko.Diagnostics
{
    [Flags]
    public enum LogType
    {
        Console,
        Database,
    }

    public enum LogSeverity
    {
        Info = 1,
        Debug = 2,
        Warn = 3,
        Error = 4,
        Fatal = 5,
    }

    public static class Logger
    {
        public static void Log(Exception ex)
        {
            string name = System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
            string ver = System.Reflection.Assembly.GetCallingAssembly().GetName().Version.ToString();

            Log(name, ver, ex.Source, ex.Message, ex.StackTrace, LogSeverity.Error, LogType.Database);
        }

        public static void Log(string message)
        {
            string name = System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
            string ver = System.Reflection.Assembly.GetCallingAssembly().GetName().Version.ToString();

            Log(name, ver, name, message, Environment.StackTrace, LogSeverity.Error, LogType.Database);
        }

        public static void Log(Exception ex, LogType type)
        {
            string name = System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
            string ver = System.Reflection.Assembly.GetCallingAssembly().GetName().Version.ToString();

            Log(name, ver, ex.Source, ex.Message, ex.StackTrace, LogSeverity.Error, type);
        }

        public static void Log(Exception ex, LogSeverity severity, LogType type)
        {
            string name = System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
            string ver = System.Reflection.Assembly.GetCallingAssembly().GetName().Version.ToString();

            Log(name, ver, ex.Source, ex.Message, ex.StackTrace, severity, type);
        }

        public static void Log(string message, LogSeverity severity, LogType type)
        {
            string name = System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
            string ver = System.Reflection.Assembly.GetCallingAssembly().GetName().Version.ToString();

            Log(name, ver, name, message, Environment.StackTrace, severity, type);
        }

        public static void Log(string source, string message, string stackTrace, LogSeverity severity, LogType type)
        {
            string name = System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
            string ver = System.Reflection.Assembly.GetCallingAssembly().GetName().Version.ToString();

            Log(name, ver, source, message, stackTrace, severity, type);
        }


        private static void Log(string name, string ver, string source, string message, string stackTrace, LogSeverity severity, LogType type)
        {
            LogBO log = new LogBO
            {
                AppName = name,
                AppVersion = ver,
                Source = source,
                Message = message,
                StackTrace = stackTrace,
                Severity = severity,
            };

            if ((int)severity >= 2 && System.Diagnostics.Debugger.IsAttached)
                System.Diagnostics.Debugger.Break();

            using (LinqLogRepository lrepo = new LinqLogRepository())
            {
                lrepo.Save(log);
            }
        }
    }
}
