﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Diagnostics.Datasources;
using J3ko.Foundation.Repository.Linq;
using J3ko.Foundation;

namespace J3ko.Diagnostics.Repositories.Linq
{
    public class LinqLogRepository : LinqRepositoryBase<LogBO, DiagnosticsData>
    {
        public LinqLogRepository()
            : base(new DiagnosticsContextFactory())
        { }

        protected override void OnSave(LogBO entity, LinqWorkUnit<DiagnosticsData> work)
        {
            log l = new log();

            l.app_name = entity.AppName;
            l.app_version = entity.AppVersion;
            l.message = entity.Message;
            l.stacktrace = entity.StackTrace;
            l.source = entity.Source;
            l.severity = (int)entity.Severity;

            work.Context.logs.InsertOnSubmit(l);
            work.Context.SubmitChanges();
        }

        protected override void OnDelete(LogBO entity, LinqWorkUnit<DiagnosticsData> work)
        {
            throw new NotImplementedException();
        }

        protected override IQueryable<LogBO> OnGetAll(LinqWorkUnit<DiagnosticsData> work)
        {
            throw new NotImplementedException();
        }
    }
}
