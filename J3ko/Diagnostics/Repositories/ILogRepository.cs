﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Diagnostics.Repositories
{
    public interface ILogRepository
    {
        void SaveLog(LogBO log);
    }
}
