﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Diagnostics
{
    public class LogBO
    {
        public int LogId { get; set; }
        public string AppName { get; set; }
        public string AppVersion { get; set; }
        public string Source { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public LogSeverity Severity { get; set; }
    }
}
