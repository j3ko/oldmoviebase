﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BLL;
using J3ko.Moviebase.BOL;
using System.Threading;
using System.ComponentModel;
using System.Configuration;
using J3ko.Foundation;

namespace J3ko.Moviebase.SyncTool
{
    class Program
    {
        static BackgroundWorker _bw;

        static void Main(string[] args)
        {
            Console.WriteLine(string.Format("Configured connection: {0}", ConfigurationManager.ConnectionStrings["J3ko.Foundation.Properties.Settings.j3koConnectionString"]));
            Console.WriteLine("Press enter to begin");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Starting Sync...");
            Begin();
        }

        static void Begin()
        {
            _bw = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            _bw.DoWork += bw_DoWork;
            _bw.ProgressChanged += bw_ProgressChanged;
            _bw.RunWorkerCompleted += bw_RunWorkerCompleted;

            Console.WriteLine("Press c to cancel");
            Console.Write("0% complete");

            _bw.RunWorkerAsync();

            while (_bw.IsBusy)
            {
                ConsoleKeyInfo key = new ConsoleKeyInfo();

                if (Console.KeyAvailable)
                    key = Console.ReadKey(true);

                if (key.Key == ConsoleKey.C)
                    _bw.CancelAsync();
            }
            Console.ReadLine();
        }

        static void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            int count = 0;
            int progress = 0;

            AuditUtility.SetCurrentUser("SyncTool");

            using (ICatalogService cserve = new CatalogService())
            {
                int[] ids = (from i in cserve.GetTitles()
                             select i.Id).ToArray();

                count = ids.Length;

                for (int i = 0; i < count; i++)
                {
                    if (_bw.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }

                    cserve.SyncTitleData(ids[i]);

                    progress = (int)(((double)i / (double)count) * 100);
                    _bw.ReportProgress(progress);
                }
            }

            e.Result = count;    // This gets passed to RunWorkerCompleted
        }

        static void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                Console.WriteLine();
                Console.WriteLine("Sync cancelled!");
            }
            else if (e.Error != null)
            {
                Console.WriteLine();
                Console.WriteLine("Worker exception: " + e.Error.ToString());
            }
            else
            {
                Console.Write("\r100% complete");
                Console.WriteLine();
                Console.WriteLine(string.Format("{0} titles synced", e.Result));      // from DoWork
            }
            Console.Write("Press enter to exit");
        }

        static void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Console.Write("\r{0}% complete", e.ProgressPercentage);
        }
    }
}
