﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using J3ko.Common.MVC.ModelBinders;
using J3ko.Common.Utils;
using System.Reflection;
using J3ko.Diagnostics;

namespace J3ko.Moviebase.MVC
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    "AddEpisode",
            //    "Detail/AddEpisode/{containerPrefix}",
            //    new { controller = "Detail", action = "AddEpisode", containerPrefix = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    "AddGroup",
            //    "Detail/AddGroup",
            //    new { controller = "Detail", action = "AddGroup" }
            //);

            routes.MapRoute(
                "Detail",
                "Detail/{id}",
                new { controller = "Detail", action = "Index" },
                new { id = @"\d+" }
            );

            routes.MapRoute(
                "SlotList",
                "Detail/SlotList/{containerId}/{include}",
                new { controller = "Detail", action = "SlotList", include = UrlParameter.Optional },
                new { containerId = @"\d+", include = @"\d+" }
            );

            //routes.MapRoute(
            //    "Save",
            //    "Detail/Save/{id}",
            //    new { controller = "Detail", action = "Save", id = "0" }
            //);

            routes.MapRoute(
                "Matches",
                "Shared/GetMatches/{term}",
                new { controller = "Shared", action = "GetMatches" }
            );


            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }


        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            ModelBinders.Binders.DefaultBinder = new ClassValidatingBinder();
            ModelBinders.Binders[typeof(ShortGuid)] = new ShortGuidBinder();
            ModelBinders.Binders[typeof(ShortGuid?)] = new ShortGuidBinder();

            try
            {
                LogApplicationStartup();
            }
            catch { }
        }

        protected void Application_End()
        {
            try
            {
                LogApplicationShutdown();
            } catch { }
        }

        private void LogApplicationStartup()
        {
            Logger.Log(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, "Application Start", Environment.StackTrace, LogSeverity.Info, LogType.Database);
        }

        private void LogApplicationShutdown()
        {
            HttpRuntime runtime = (HttpRuntime)typeof(System.Web.HttpRuntime).InvokeMember("_theRuntime",
                                                                                BindingFlags.NonPublic
                                                                                | BindingFlags.Static
                                                                                | BindingFlags.GetField,
                                                                                null,
                                                                                null,
                                                                                null);

            if (runtime == null)
                return;

            string shutDownMessage = (string)runtime.GetType().InvokeMember("_shutDownMessage",
                                                                             BindingFlags.NonPublic
                                                                             | BindingFlags.Instance
                                                                             | BindingFlags.GetField,
                                                                             null,
                                                                             runtime,
                                                                             null);

            string shutDownStack = (string)runtime.GetType().InvokeMember("_shutDownStack",
                                                                           BindingFlags.NonPublic
                                                                           | BindingFlags.Instance
                                                                           | BindingFlags.GetField,
                                                                           null,
                                                                           runtime,
                                                                           null);

            Logger.Log(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, shutDownMessage, shutDownStack, LogSeverity.Info, LogType.Database);
        }
    }
}