﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using J3ko.Common.Utils;
using AutoMapper;

namespace J3ko.Moviebase.MVC.Converters
{
    public class ShortGuidConverter : TypeConverter<Guid, ShortGuid>
    {
        protected override ShortGuid ConvertCore(Guid source)
        {
            return (ShortGuid)source;
        }
    }
}