﻿$(function () {

    $("input:submit, input:button").livequery(function () {
        $(this).button().click(function () {
            $(this).removeClass("ui-state-hover ui-state-focus");
        });
    });

    DisplayNotification();
});

function DisplayNotification() {
    var msg = $("#notification");

    if (msg.length > 0) {
        $.jGrowl.defaults.closer = false;
        $.jGrowl(msg.html(),
        {
            position: "center"
            , theme: "jGrowlUI"
            , closeTemplate: ""
        });
    }
}