﻿function PopulateRecord(elem) {
    var title = elem.closest(".imdbContainer").data("title");
    var plot = elem.closest(".imdbContainer").data("plot");
    var imdb = elem.closest(".imdbContainer").data("imdbid");
    //var newSmallPosterUrl = elem.data("poster");
    var smallPosterUrl = elem.attr("src");
    var posterUrl = elem.data("poster");

    title = $("<div/>").html(title).text();
    plot = $("<div/>").html(plot).text();

    $("#imdbIdInput").val(imdb);
    $("#titleNameInput").val(title);
    $("#plotNameInput").val(plot);
    $("#posterImage").attr("src", smallPosterUrl);
    $("#newPosterSmallUrl").val(smallPosterUrl);
    $("#newPosterUrl").val(posterUrl);
}

function AddEpisode(elem) {
    var group = elem.closest(".groupRow");
    var container = group.find(".episodeContainer");
    var href = elem.data("href");

    $.ajax({
        global: false,
        url: href,
        cache: false,
        success: function (html) {
            container.append(html);
            //get the relevant form
            var form = $("#form0")
            //need to remove the validator data so that the validator picks up new validation.
            form.removeData('validator');
            $.validator.unobtrusive.parse(form);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
    return false;
}

function AddGroup(elem) {
    var href = elem.data("href");

    $.ajax({
        global: false,
        url: href,
        cache: false,
        success: function (html) {
            $(".groupContainer").append(html);
            //get the relevant form
            var form = $("#form0")
            //need to remove the validator data so that the validator picks up new validation.
            form.removeData('validator');
            $.validator.unobtrusive.parse(form);

            FixSlotsOnDiscAdd();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
    return false;
}

function FixSlotsOnDiscAdd() {
    var lastSlot = $(".slotSelector").last();

    // remove used slots
    $.each($(".slotSelector"), function () {
        if (lastSlot.attr("id") != $(this).attr("id")) {
            removeOption(lastSlot, $(this).val());
            if (lastSlot.val() == $(this).val()) {
                lastSlot.find('option:selected', 'select').removeAttr('selected').next('option').attr('selected', 'selected');
            }
        }
    });

    var lastValue = lastSlot.val();
    // remove selected slot
    $.each($(".slotSelector"), function () {
        if (lastSlot.attr("id") != $(this).attr("id")) {
            removeOption($(this), lastValue);
        }
    });
}

function RefreshSlots(elem) {
    var slot = elem.closest(".groupRow").find(".slotSelector:first");

    $.ajax({
        global: false,
        url: "/Detail/SlotList",
        data: { containerId: elem.val() },
        cache: false,
        success: function (html) {
            populateDropdown(slot, html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
    return false;
}

function RefreshMatches() {
    var text = jQuery.trim($("#titleNameInput").val());

    if (!text) return false;

    var href = $("#matchContainer").data("href") + "?term=" + escape(text);

    $.ajax({
        url: href,
        dataType: "html",
        cache: false,
        success: function (html) {
            if ($.isEmptyObject(html) || html == null || html == "" || html.length > 0)
                $("#matchContainer").empty();
            $("#matchContainer").append(html);
            $("#accordion").accordion({ collapsible: true, fillSpace: true, clearStyle: true, autoheight: false });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
    return false;
}

function throttle(f, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function () {
            f.apply(context, args);
        },
        delay || 500);
    };
}

function fixedEncodeURI(str) {
    return encodeURI(str).replace(/%5B/g, '[').replace(/%5D/g, ']');
}

function populateDropdown(select, data) {
    select.html('');
    $.each(data, function (id, option) {
        select.append($('<option></option>').val(option.Text).html(option.Value));
    });
}

function addOption(select, value) {
    //if (select.find("option[value='" + key + "']").length <= 0) {
        //select.append($('<option></option>').val(value).html(key));
        select.find("option[value='" + value + "']").removeAttr('disabled');
    //}
}

function removeOption(select, value) {
    //select.find("option[value='" + key + "']").remove();
    select.find("option[value='" + value + "']").attr('disabled','disabled');
}

$(function () {
    var previous;

    $(".slotSelector").live("focus", function () {
        previous = $(this).val();
    });

    $(".slotSelector").live("change", function () {
        var val, currentId;

        val = $(this).val();
        currentId = $(this).attr('id');

        $.each($(".slotSelector"), function () {
            if ($(this).attr("id") != currentId) {
                removeOption($(this), val);
            }
        });

        $.each($(".slotSelector"), function () {
            if ($(this).attr("id") != currentId) {
                addOption($(this), previous);
            }
        });
    });

    $("#imdbProgress").ajaxStart(function () {
        $(this).show();
    }).ajaxStop(function () {
        $(this).hide();
    });

    $(".addEpisodeDetail").live("click", function () {
        return AddEpisode($(this));
    });

    $("a.deleteEpisode").live("click", function () {
        $(this).parents("div.episodeRow:first").remove();
        return false;
    });

    $("#addGroupDetail").live("click", function () {
        return AddGroup($(this));
    });

    $("a.deleteGroup").live("click", function () {
        var disc = $(this).parents("div.groupRow:first");
        var removed = disc.find(".slotSelector");
        $.each($(".slotSelector"), function () {
            addOption($(this), removed.val());
        });
        disc.remove();
        return false;
    });

    $("#titleNameInput").keypress(throttle(function () {
        return RefreshMatches();
    }, 1000));

    $("img.imdbResult").live("click", function () {
        PopulateRecord($(this));
    });

    $(".containerSelector").live("change", function () {
        RefreshSlots($(this));
    });
});