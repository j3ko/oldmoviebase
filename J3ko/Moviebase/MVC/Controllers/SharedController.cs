﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using J3ko.Common.Docs;
using J3ko.Common.Utils;
using J3ko.Moviebase.BLL;
using J3ko.Moviebase.MVC.Mappers;
using J3ko.Web.Movies;
using J3ko.Moviebase.MVC.Models.Shared;
using System.Threading.Tasks;

namespace J3ko.Moviebase.MVC.Controllers
{
    public class SharedController : BaseController
    {
        private ICatalogService _cserve;
        private IImageService _iserve;
        private IMatchService _mserve;

        public SharedController()
            : this(new CatalogService(), new ImageService(), new MatchService())
        { }

        public SharedController(ICatalogService cserve, IImageService iserve, IMatchService mserve)
        {
            _cserve = cserve;
            _iserve = iserve;
            _mserve = mserve;
        }

        [OutputCache(Duration = 3600, VaryByParam = "id")]
        public void PosterAsync(ShortGuid id)
        {
            AsyncManager.OutstandingOperations.Increment();

            Task.Factory.StartNew(() =>
            {
                DocumentBO doc = _iserve.GetImage(id, ImageSize.LargePoster);
                AsyncManager.Parameters["doc"] = doc;
                AsyncManager.OutstandingOperations.Decrement();
            });
        }

        public ActionResult PosterCompleted(DocumentBO doc)
        {
            FileStreamResult result = new FileStreamResult(doc.FileContent, doc.MimeType);
            return result;
        }

        [OutputCache(Duration = 36000, VaryByParam = "id")]
        public void SmallPosterAsync(ShortGuid id)
        {
            AsyncManager.OutstandingOperations.Increment();

            Task.Factory.StartNew(() =>
            {
                DocumentBO doc = _iserve.GetImage(id, ImageSize.SmallPoster);
                AsyncManager.Parameters["doc"] = doc;
                AsyncManager.OutstandingOperations.Decrement();
            });
        }

        public ActionResult SmallPosterCompleted(DocumentBO doc)
        {
            FileStreamResult result = new FileStreamResult(doc.FileContent, doc.MimeType);
            return result;
        }

        [OutputCache(Duration = 36000, VaryByParam = "id")]
        public ActionResult Profile(ShortGuid id)
        {
            DocumentBO doc = _iserve.GetImage(id, ImageSize.Profile);
            FileStreamResult result = new FileStreamResult(doc.FileContent, doc.MimeType);
            return result;
        }


        [ValidateInput(false)]
        public ActionResult GetMatches(string term)
        {
            List<MovieBO> temp = _mserve.Search(term).ToList();

            IEnumerable<MatchModel> result =
                from e in temp
                select new MatchModel
                {
                    ImdbId = e.ImdbId,
                    Plot = e.Plot,
                    Title = e.Title,
                    Posters = GetPosters(e.Posters),
                };

            base.MovieInfo = temp;

            return PartialView("_MatchResultSet", result.ToList());
        }

        private List<MatchPosterModel> GetPosters(IEnumerable<MoviePostersBO> posters)
        {
            return (from p in posters
                    select new MatchPosterModel
                    {
                        PosterUrl = p.PosterUrl,
                        SmallPosterUrl = p.SmallPosterUrl,
                    }).ToList();
        }

        protected override void Dispose(bool disposing)
        {
            if (_iserve != null)
            {
                _iserve.Dispose();
                _iserve = null;
            }
            if (_cserve != null)
            {
                _cserve.Dispose();
                _cserve = null;
            }
            if (_mserve != null)
            {
                _mserve = null;
            }

            base.Dispose(disposing);
        }
    }
}
