﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using J3ko.Moviebase.BLL;
using J3ko.Moviebase.BOL;
using AutoMapper;
using J3ko.Common.Linq;
using J3ko.Moviebase.MVC.Models.Home;
using J3ko.Common.Utils;
using J3ko.Moviebase.MVC.Converters;

namespace J3ko.Moviebase.MVC.Controllers
{
    public class HomeController : BaseController
    {
        private ICatalogService _cserve;
        private IFilterService _fserve;
        private const int PAGE_SIZE = 30;


        public HomeController()
            : this(new CatalogService(), new FilterService())
        { }

        public HomeController(ICatalogService cserve, IFilterService fserve)
        {
            _cserve = cserve;
            _fserve = fserve;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p">page</param>
        /// <param name="s">sort</param>
        /// <param name="t">search term</param>
        /// <param name="f">filter</param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult Index(int p = 1, int s = (int)SortOrder.ReleasedDateDesc, string t = null, string[] fr = null, string[] fg = null)
        {
            IEnumerable<IFilterableBO> filters = ParseFilters(fr, fg);
            SortOrder order = ParseSortOrder(s);

            IEnumerable<TitleBO> titles = _cserve.GetTitles(t, order, filters);
            PagedList<TitleBO> paged = Pagination.ToPagedList<TitleBO>(titles.AsQueryable(), p, PAGE_SIZE);

            //if (paged.Count == 1)
            //    return RedirectToAction("Index", "Detail", new { id = paged.FirstOrDefault().Id });

            IndexHomeModel model = MapIndexModel(paged);
            model.FilterBarModel = new FilterBarModel()
            {
                SearchTerm = t,
                SortOrder = s,
                Filters = filters,
            };

            return View(model);
        }

//#if !DEBUG // bug in enableOutputCache="false" for partial views: 
//           // http://thenullreference.com/blog/fixing-the-asp-net-mvc-3-outputcacheattribute-for-partial-views-to-honor-some-web-config-settings/
//        [OutputCache(Duration = 3600)]
//#endif
        public ActionResult GetAvailableGenres()
        {
            List<IFilterableBO> result = _fserve.GetAvailableGenres().ToList();

            return PartialView("_GenreFilterList", result);
        }

//#if !DEBUG // bug in enableOutputCache="false" for partial views: 
//           // http://thenullreference.com/blog/fixing-the-asp-net-mvc-3-outputcacheattribute-for-partial-views-to-honor-some-web-config-settings/
//        [OutputCache(Duration = 3600)]
//#endif
        public ActionResult GetAvailableYears()
        {
            List<IFilterableBO> result = _fserve.GetAvailableReleasedYears().ToList();

            return PartialView("_ReleasedYearFilterList", result);
        }

//#if !DEBUG // bug in enableOutputCache="false" for partial views: 
//           // http://thenullreference.com/blog/fixing-the-asp-net-mvc-3-outputcacheattribute-for-partial-views-to-honor-some-web-config-settings/
//        [OutputCache(Duration = 3600)]
//#endif
        public ActionResult GetSortOrders()
        {
            List<ICodeBO> result = _fserve.GetSortOrders().Cast<ICodeBO>().ToList();

            return PartialView("_SortList", result);
        }

        #region Private Helpers
        private IndexHomeModel MapIndexModel(PagedList<TitleBO> titles)
        {
            Mapper.CreateMap<TitleBO, TitleHomeModel>();
            Mapper.CreateMap<Guid, ShortGuid>().ConvertUsing(new ShortGuidConverter());
            List<TitleHomeModel> modelTitles = Mapper.Map<PagedList<TitleBO>, List<TitleHomeModel>>(titles);

            IndexHomeModel model = new IndexHomeModel()
            {
                Titles = modelTitles,
                PageIndex = titles.PageIndex,
                PageSize = titles.PageSize,
                HasNextPage = titles.HasNextPage,
                HasPreviousPage = titles.HasPreviousPage,
                TotalCount = titles.TotalCount,
                TotalPages = titles.TotalPages,
            };

            return model;
        }
        private SortOrder ParseSortOrder(int i)
        {
            SortOrder result = SortOrder.TitleNameAlphaAsc;
            if (Enum.IsDefined(typeof(SortOrder), i))
            {
                result = (SortOrder)i;
            }
            return result;
        }
        private List<IFilterableBO> ParseFilters(string[] fr, string[] fg)
        {
            List<IFilterableBO> result = new List<IFilterableBO>();

            if (fg != null)
                result.AddRange(ParseGenreFilters(fg));
            if (fr != null)
                result.AddRange(ParseReleasedYearFilters(fr));
            
            return result;
        }

        private List<IFilterableBO> ParseReleasedYearFilters(string[] values)
        {
            List<IFilterableBO> result = new List<IFilterableBO>();
            List<IFilterableBO> years = _fserve.GetAvailableReleasedYears().ToList();

            result = (from i in years
                        where values.Contains(i.Key)
                        select i).ToList();

            return result;
        }

        private List<IFilterableBO> ParseGenreFilters(string[] values)
        {
            List<IFilterableBO> result = new List<IFilterableBO>();
            List<IFilterableBO> genres = _fserve.GetAvailableGenres().ToList();

            result = (from i in genres
                        where values.Contains(i.Key)
                        select i).ToList();

            return result;
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (_cserve != null)
            {
                _cserve.Dispose();
                _cserve = null;
            }
            if (_fserve != null)
            {
                _fserve.Dispose();
                _fserve = null;
            }
            base.Dispose(disposing);
        }
    }
}