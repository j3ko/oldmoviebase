﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using J3ko.Diagnostics;
using J3ko.Web.Movies;

namespace J3ko.Moviebase.MVC.Controllers
{
    public class BaseController : AsyncController
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            Logger.Log(filterContext.Exception);
        }

        protected void SetNotification(string message)
        {
            TempData["Notification"] = message;
        }

        protected List<MovieBO> MovieInfo
        {
            get
            {
                var info = Session["MovieInfo"] as List<MovieBO>;
                if (info == null)
                {
                    info = new List<MovieBO>();
                }

                return info;
            }
            set
            {
                Session["MovieInfo"] = value;
            }
        }
    }
}
