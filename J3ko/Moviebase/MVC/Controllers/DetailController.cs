﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.BLL;
using J3ko.Moviebase.MVC.Mappers;
using J3ko.Common.Docs;
using System.ComponentModel;
using J3ko.Common.MVC.ModelBinders;
using J3ko.Web.Movies;
using J3ko.Moviebase.MVC.Models.Detail;

namespace J3ko.Moviebase.MVC.Controllers
{
    public class DetailController : BaseController
    {
        private ICatalogService _catserve;
        private ICodeService _codserve;
        private ISuggestionService _sugserve;

        public DetailController()
        {
            _catserve = new CatalogService();
            _codserve = new CodeService();
            _sugserve = SuggestionService.Instance;
        }

        public ActionResult Index(int id)
        {
            TitleBO title = _catserve.GetTitle(id);

            if (title == null || title.Id <= 0)
                throw new HttpException(404, "Title not found");

            DetailMapper map = new DetailMapper(_codserve, _catserve, _sugserve);
            TitleDetailModel model = map.MapTitleDetailModel(title);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult SyncTitle(int id)
        {
            TitleBO title = null;
            try
            {
                title = _catserve.SyncTitleData(id);
                SetNotification("Title Synced Successfully");
            }
            catch (ApplicationException apex)
            {
                SetNotification(apex.Message);
            }

            return RedirectToAction("Index", id);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Save(int id = 0)
        {
            TitleBO title = _catserve.GetTitle(id) ?? new TitleBO();

            if (title.Id <= 0)
            {
                GroupBO grp = new GroupBO();
                title.Add(grp);
            }

            DetailMapper map = new DetailMapper(_codserve, _catserve, _sugserve);
            TitleDetailEditModel model = map.MapTitleDetailEditModel(title);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Save(TitleDetailEditModel model)
        {
            DetailMapper map = new DetailMapper(_codserve, _catserve, _sugserve);
            List<MovieBO> movies = MovieInfo;

            try
            {
                if (ModelState.IsValid)
                {
                    TitleBO bo = map.MapTitleBO(model);
                    MovieBO mov = MovieInfo.Find(x => x.ImdbId == bo.ImdbId);

                    if (mov != null)
                        bo = _catserve.SyncTitleData(bo, mov, model.NewPosterUrl, model.NewSmallPosterUrl);
                    else
                        bo = _catserve.SaveTitle(bo, model.NewPosterUrl, model.NewSmallPosterUrl);
 
                    SetNotification("Saved Successfully");
                    return RedirectToAction("Save", new { id = bo.Id });
                }
            }
            catch (ApplicationException ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                SetNotification("Save Failed");
            }

            map.PopulateSelectLists(model);
            return View(model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddEpisode(string containerPrefix)
        {
            ViewData["ContainerPrefix"] = containerPrefix;
            DetailMapper map = new DetailMapper(_codserve, _catserve, _sugserve);
            EpisodeDetailEditModel model = map.MapEpisodeDetailEditModel(new EpisodeBO());
            map.PopulateEpisodeSelectLists(model);

            return PartialView("_EpisodeDetailEditRow", model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddGroup()
        {
            DetailMapper map = new DetailMapper(_codserve, _catserve, _sugserve);
            GroupBO group = new GroupBO();
            group.Add(new EpisodeBO());
            GroupDetailEditModel model = map.MapGroupDetailEditModel(group);
            map.PopulateGroupSelectLists(model);

            return PartialView("_GroupDetailEditRow", model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult SlotList(int containerId, int include = 0)
        {
            IEnumerable<SlotBO> slots = _codserve.GetAvailableSlots(containerId, include);
            SelectList result = new SelectList(slots, "Label", "Id");

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (_catserve != null)
            {
                _catserve.Dispose();
                _catserve = null;
            }
            if (_codserve != null)
            {
                _codserve.Dispose();
                _codserve = null;
            }
            if (_sugserve != null)
            {
                _sugserve.Dispose();
                _sugserve = null;
            }
            base.Dispose(disposing);
        }
    }
}
