﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.BLL;
using J3ko.Moviebase.MVC.Models.Detail;
using J3ko.Common.Utils;

namespace J3ko.Moviebase.MVC.Mappers
{
    public class DetailMapper
    {
        private ICodeService _codserve;
        private ICatalogService _catserve;
        private ISuggestionService _sugserve;

        public DetailMapper(ICodeService codserve, ICatalogService catserve, ISuggestionService sugserve)
        {
            _codserve = codserve;
            _catserve = catserve;
            _sugserve = sugserve;
        }

        #region Detail Mappers
        public TitleDetailModel MapTitleDetailModel(TitleBO title)
        {
            TitleDetailModel result = new TitleDetailModel();

            result.TitleId = title.Id;
            result.PosterId = (ShortGuid)title.CoverId;
            result.Name = title.Name;
            result.Plot = title.Plot;
            result.Category = title.Category.ToString();
            result.PosterId = (ShortGuid)title.CoverId;
            result.ImdbId = title.ImdbId;

            foreach (GenreBO g in title.Genres)
                result.Genres.Add(g);

            foreach (GroupBO g in title)
                result.Groups.Add(MapGroupDetailModel(g));

            foreach (TitlePersonBO p in title.TitlePeople.Where(x => x.Role == TitleRole.Actor))
                result.Actors.Add(p);

            foreach (TitlePersonBO p in title.TitlePeople.Where(x => x.Role == TitleRole.Director))
                result.Directors.Add(p);

            foreach (TitlePersonBO p in title.TitlePeople.Where(x => x.Role == TitleRole.Producer))
                result.Producers.Add(p);

            result.Suggestions = GetSuggestions(title);

            return result;
        }

        private List<TitleBO> GetSuggestions(TitleBO title)
        {
            return _sugserve.GetSimilar(title);
        }

        public GroupDetailModel MapGroupDetailModel(GroupBO group)
        {
            GroupDetailModel result = new GroupDetailModel();

            result.Description = group.Description;
            result.MediaFormatId = group.Media.Format.MediaFormatId;
            result.Slot = group.Slot.Label;
            result.Container = group.Slot.Container.Name;

            foreach (EpisodeBO e in group)
                result.Episodes.Add(MapEpisodeDetailModel(e));

            return result;
        }

        public EpisodeDetailModel MapEpisodeDetailModel(EpisodeBO episode)
        {
            EpisodeDetailModel result = new EpisodeDetailModel();

            result.Name = episode.Name;
            result.Instance = episode.Instance.ToString();

            return result;
        }
        #endregion

        #region Edit Mappers
        public TitleDetailEditModel MapTitleDetailEditModel(TitleBO title)
        {
            TitleDetailEditModel result = new TitleDetailEditModel();

            result.TitleId = title.Id;
            result.Name = title.Name;
            result.Plot = title.Plot;
            result.CategoryId = title.Category.Id;          
            result.PosterSmallId = (ShortGuid)title.CoverSmallId;
            result.ImdbId = title.ImdbId;

            foreach (GroupBO g in title)
                result.Groups.Add(MapGroupDetailEditModel(g));

            PopulateSelectLists(result);

            return result;
        }


        public GroupDetailEditModel MapGroupDetailEditModel(GroupBO group)
        {
            GroupDetailEditModel result = new GroupDetailEditModel();

            result.GroupId = group.Id;
            result.SlotId = group.Slot.Id;
            result.MediaId = group.Media.Id;
            result.ContainerId = group.Slot.Container.Id;
            result.Description = group.Description;

            foreach (EpisodeBO e in group)
                result.Episodes.Add(MapEpisodeDetailEditModel(e));

            return result;
        }

        public EpisodeDetailEditModel MapEpisodeDetailEditModel(EpisodeBO episode)
        {
            EpisodeDetailEditModel result = new EpisodeDetailEditModel();

            result.Name = episode.Name;
            result.InstanceId = episode.Instance.Id;

            return result;
        }

        public TitleDetailEditModel PopulateSelectLists(TitleDetailEditModel model)
        {
            TitleDetailEditModel result = model ?? new TitleDetailEditModel();
            
            IEnumerable<CategoryBO> categories = _codserve.GetCategories();

            result.Categories = categories;
 
            foreach (GroupDetailEditModel g in result.Groups)
                PopulateGroupSelectLists(g);

            return result;
        }

        public GroupDetailEditModel PopulateGroupSelectLists(GroupDetailEditModel model)
        {
            GroupDetailEditModel result = model ?? new GroupDetailEditModel();

            IEnumerable<MediaBO> media = _codserve.GetMedia();
            IEnumerable<SlotBO> slots = _codserve.GetAvailableSlots(model.ContainerId, model.SlotId);
            IEnumerable<ContainerBO> containers = _codserve.GetContainers();

            result.Media = media;
            result.Slots = slots;
            result.Containers = containers;

            foreach (EpisodeDetailEditModel e in result.Episodes)
                PopulateEpisodeSelectLists(e);

            return result;
        }

        public EpisodeDetailEditModel PopulateEpisodeSelectLists(EpisodeDetailEditModel model)
        {
            EpisodeDetailEditModel result = model ?? new EpisodeDetailEditModel();

            IEnumerable<InstanceBO> instances = _codserve.GetInstances();

            result.Instances = instances;

            return result;
        }
        #endregion

        #region BO Mappers
        public TitleBO MapTitleBO(TitleDetailEditModel title)
        {
            TitleBO result = _catserve.GetTitle(title.TitleId);

            result.Name = title.Name;
            result.Plot = title.Plot;
            result.Category = CategoryBO.Create(title.CategoryId);
            result.CoverId = title.PosterId;
            result.CoverSmallId = title.PosterSmallId;
            result.ImdbId = title.ImdbId;

            List<GroupBO> toBeSaved = new List<GroupBO>();

            foreach (GroupDetailEditModel group in title.Groups)
                toBeSaved.Add(MapGroupBO(result, group));

            result.Clear();

            foreach (GroupBO group in toBeSaved)
                result.Add(group);

            return result;
        }

        private GroupBO MapGroupBO(TitleBO title, GroupDetailEditModel group)
        {
            GroupBO result = title.Where(x => x.Id == group.GroupId && group.GroupId != 0).SingleOrDefault() ?? new GroupBO();

            result.Description = group.Description;
            result.Media = MediaBO.Create(group.MediaId);
            result.Slot = SlotBO.Create(group.SlotId);

            if (group.GroupId == 0)
                title.Add(result);

            List<EpisodeBO> toBeSaved = new List<EpisodeBO>();

            foreach (EpisodeDetailEditModel episode in group.Episodes)
                toBeSaved.Add(MapEpisodeBO(result, episode));

            result.Clear();

            foreach (EpisodeBO episode in toBeSaved)
                result.Add(episode);

            return result;
        }

        private EpisodeBO MapEpisodeBO(GroupBO group, EpisodeDetailEditModel episode)
        {
            EpisodeBO result = group.Where(x => x.Instance.Id == episode.InstanceId).SingleOrDefault() ?? new EpisodeBO();

            result.Name = episode.Name;
            result.Instance = InstanceBO.Create(episode.InstanceId);

            return result;
        }
        #endregion
    }
}