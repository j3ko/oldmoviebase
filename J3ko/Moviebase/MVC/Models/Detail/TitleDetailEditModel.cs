﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using J3ko.Moviebase.BOL;
using J3ko.Common.Utils;

namespace J3ko.Moviebase.MVC.Models.Detail
{
    //[Cardinality(PropertyName="Groups", MinCount = 1)]
    public class TitleDetailEditModel
    {
        [HiddenInput(DisplayValue = false)]
        public int TitleId { get; set; }

        public bool IsNew 
        {
            get
            {
                return TitleId <= 0;
            }
        }

        [StringLength(255, ErrorMessage = "{0} is too long")]
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [StringLength(4000, ErrorMessage = "{0} is too long")]
        [Required(AllowEmptyStrings = false)]
        public string Plot { get; set; }

        public ShortGuid PosterId { get; set; }

        public ShortGuid PosterSmallId { get; set; }

        public string NewPosterUrl { get; set; }

        public string NewSmallPosterUrl { get; set; }

        [StringLength(32, ErrorMessage = "{0} is too long")]
        public string ImdbId { get; set; }

        public IEnumerable<CategoryBO> Categories { get; set; }

        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        private List<GroupDetailEditModel> _groups;

        public List<GroupDetailEditModel> Groups
        {
            get
            {
                if (_groups == null)
                    _groups = new List<GroupDetailEditModel>();
                return _groups;
            }
            set
            {
                _groups = value;
            }
        }
    }
}