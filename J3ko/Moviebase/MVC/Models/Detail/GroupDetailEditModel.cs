﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using J3ko.Moviebase.BOL;

namespace J3ko.Moviebase.MVC.Models.Detail
{
    public class GroupDetailEditModel
    {
        [HiddenInput(DisplayValue = false)]
        public int GroupId { get; set; }

        [StringLength(255, ErrorMessage = "{0} is too long")]
        [Required(AllowEmptyStrings = false)]
        public string Description { get; set; }

        public IEnumerable<MediaBO> Media { get; set; }

        [Display(Name = "Media")]
        public int MediaId { get; set; }

        public IEnumerable<SlotBO> Slots { get; set; }

        [Display(Name = "Slot")]
        public int SlotId { get; set; }

        public IEnumerable<ContainerBO> Containers { get; set; }

        [Display(Name = "Container")]
        public int ContainerId { get; set; }

        private List<EpisodeDetailEditModel> _episodes;

        public List<EpisodeDetailEditModel> Episodes
        {
            get
            {
                if (_episodes == null)
                    _episodes = new List<EpisodeDetailEditModel>();
                return _episodes;
            }
            set
            {
                _episodes = value;
            }
        }
    }
}