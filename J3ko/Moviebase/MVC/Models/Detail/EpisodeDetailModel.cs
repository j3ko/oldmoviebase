﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace J3ko.Moviebase.MVC.Models.Detail
{
    public class EpisodeDetailModel
    {
        public string Name { get; set; }

        public string Instance { get; set; }
    }
}