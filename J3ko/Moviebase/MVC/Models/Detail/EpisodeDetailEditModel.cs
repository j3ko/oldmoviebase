﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using J3ko.Moviebase.BOL;

namespace J3ko.Moviebase.MVC.Models.Detail
{
    public class EpisodeDetailEditModel
    {
        [StringLength(255, ErrorMessage = "{0} is too long")]
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        public IEnumerable<InstanceBO> Instances { get; set; }

        [Display(Name = "Instance")]
        public int InstanceId { get; set; }
    }
}