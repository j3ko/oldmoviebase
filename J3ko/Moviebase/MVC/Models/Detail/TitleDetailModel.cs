﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using J3ko.Moviebase.BOL;
using J3ko.Common.Utils;

namespace J3ko.Moviebase.MVC.Models.Detail
{
    public class TitleDetailModel
    {
        [HiddenInput(DisplayValue = false)]
        public int TitleId { get; set; }

        public ShortGuid PosterId { get; set; }

        public string Name { get; set; }

        public string Plot { get; set; }

        public string Category { get; set; }

        [UIHint("ImdbLink")]
        public string ImdbId { get; set; }

        private List<TitleBO> _suggestions;
        [UIHint("SuggestionList")]
        public List<TitleBO> Suggestions
        {
            get
            {
                if (_suggestions == null)
                    _suggestions = new List<TitleBO>();
                return _suggestions;
            }
            set
            {
                _suggestions = value;
            }
        }


        private List<GenreBO> _genres;
        [UIHint("GenreList")]
        public List<GenreBO> Genres
        {
            get
            {
                if (_genres == null)
                    _genres = new List<GenreBO>();
                return _genres;
            }
            set
            {
                _genres = value;
            }
        }

        private List<GroupDetailModel> _groups;
        [UIHint("GroupDetailList")]
        public List<GroupDetailModel> Groups
        {
            get
            {
                if (_groups == null)
                    _groups = new List<GroupDetailModel>();
                return _groups;
            }
            set
            {
                _groups = value;
            }
        }

        private List<TitlePersonBO> _actors;
        [UIHint("ActorList")]
        public List<TitlePersonBO> Actors
        {
            get
            {
                if (_actors == null)
                    _actors = new List<TitlePersonBO>();
                return _actors;
            }
            set
            {
                _actors = value;
            }
        }

        private List<TitlePersonBO> _directors;
        [UIHint("DirectorList")]
        public List<TitlePersonBO> Directors
        {
            get
            {
                if (_directors == null)
                    _directors = new List<TitlePersonBO>();
                return _directors;
            }
            set
            {
                _directors = value;
            }
        }

        private List<TitlePersonBO> _producers;
        [UIHint("ProducerList")]
        public List<TitlePersonBO> Producers
        {
            get
            {
                if (_producers == null)
                    _producers = new List<TitlePersonBO>();
                return _producers;
            }
            set
            {
                _producers = value;
            }
        }
    }
}