﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace J3ko.Moviebase.MVC.Models.Detail
{
    public class GroupDetailModel
    {
        [HiddenInput(DisplayValue = false)]
        public int GroupId { get; set; }

        public string Description { get; set; }

        public int MediaFormatId { get; set; }

        public string Slot { get; set; }

        public string Container { get; set; }

        private List<EpisodeDetailModel> _episodes;

        public List<EpisodeDetailModel> Episodes
        {
            get
            {
                if (_episodes == null)
                    _episodes = new List<EpisodeDetailModel>();
                return _episodes;
            }
            set
            {
                _episodes = value;
            }
        }
    }
}