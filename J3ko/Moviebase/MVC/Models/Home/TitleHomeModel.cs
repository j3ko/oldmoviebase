﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using J3ko.Common.Utils;

namespace J3ko.Moviebase.MVC.Models.Home
{
    public class TitleHomeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ShortGuid CoverSmallId { get; set; }
        public ShortGuid CoverId { get; set; }
    }
}