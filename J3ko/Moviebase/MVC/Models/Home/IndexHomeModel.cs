﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using J3ko.Common.Linq;

namespace J3ko.Moviebase.MVC.Models.Home
{
    public class IndexHomeModel : IPagedList
    {
        private List<TitleHomeModel> _titles;
        public List<TitleHomeModel> Titles
        {
            get
            {
                if (_titles == null)
                    _titles = new List<TitleHomeModel>();
                return _titles;
            }
            set
            {
                _titles = value;
            }
        }


        private FilterBarModel _filterBarModel;
        public FilterBarModel FilterBarModel
        {
            get
            {
                if (_filterBarModel == null)
                    _filterBarModel = new FilterBarModel();
                return _filterBarModel;
            }
            set
            {
                _filterBarModel = value;
            }
        }

        #region IPagedList Members
        public int TotalPages { get; set; }
        public int TotalCount { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }
        #endregion
    }
}