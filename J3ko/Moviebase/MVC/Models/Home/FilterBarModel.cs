﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using J3ko.Common.Linq;
using J3ko.Moviebase.BOL;
using System.ComponentModel.DataAnnotations;

namespace J3ko.Moviebase.MVC.Models.Home
{
    public class FilterBarModel
    {
        // search term
        [UIHint("SearchTerm")]
        public string SearchTerm { get; set; }
        public int SortOrder { get; set; }
        public IEnumerable<IFilterableBO> Filters { get; set; }

    }
}