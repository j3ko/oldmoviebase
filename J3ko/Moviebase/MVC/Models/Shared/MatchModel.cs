﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace J3ko.Moviebase.MVC.Models.Shared
{
    public class MatchModel
    {
        public string Title { get; set; }
        public string Plot { get; set; }
        public string ImdbId { get; set; }
        private List<MatchPosterModel> _posters;
        public List<MatchPosterModel> Posters
        {
            get
            {
                if (_posters == null)
                    _posters = new List<MatchPosterModel>();
                return _posters;
            }
            set
            {
                _posters = value;
            }
        }
    }
}