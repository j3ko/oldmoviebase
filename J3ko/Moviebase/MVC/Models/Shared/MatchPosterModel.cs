﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace J3ko.Moviebase.MVC.Models.Shared
{
    public class MatchPosterModel
    {
        public string SmallPosterUrl { get; set; }
        public string PosterUrl { get; set; }
    }
}