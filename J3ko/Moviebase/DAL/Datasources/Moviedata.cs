using System.Data.Linq;
using J3ko.Foundation;
namespace J3ko.Moviebase.DAL.Datasources
{
    partial class Moviedata
    {
        public override void SubmitChanges(ConflictMode failureMode)
        {
            AuditUtility.ProcessAuditFields(GetChangeSet().Updates);
            AuditUtility.ProcessAuditFields(GetChangeSet().Inserts);
            base.SubmitChanges(failureMode);
        }
    }
}
