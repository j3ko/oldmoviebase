﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Foundation.Repository.Linq;
using J3ko.Foundation;

namespace J3ko.Moviebase.DAL.Datasources
{
    public class MoviebaseContextFactory : IDataContextFactory<Moviedata>
    {
        #region IDataContextFactory<Moviedata> Members

        public Moviedata Create()
        {
            return new Moviedata(BootStrapper.DefaultConnectionString);
        }

        #endregion
    }
}
