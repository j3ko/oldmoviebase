﻿using J3ko.Moviebase.DAL.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using J3ko.Moviebase.BOL;
using System.Collections.Generic;
using System.Linq;
using J3ko.Foundation.Repository;
using J3ko.Moviebase.DAL.Datasources;
using J3ko.Foundation.Repository.Linq;

namespace J3ko.Moviebase.DAL.Test
{
    
    
    /// <summary>
    ///This is a test class for AltTitleNameRepositoryTest and is intended
    ///to contain all AltTitleNameRepositoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AltTitleNameRepositoryTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AltTitleNameRepository Constructor
        ///</summary>
        [TestMethod()]
        public void AltTitleNameRepositoryConstructorTest()
        {
            LinqAltTitleNameRepository target = new LinqAltTitleNameRepository();
            Assert.IsNotNull(target);
        }

        [TestMethod()]
        public void GetAllExecutes()
        {
            LinqAltTitleNameRepository target = new LinqAltTitleNameRepository();
            IEnumerable<AltTitleNameBO> result = target.GetAll().ToList();
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void SaveGetDelete()
        {
            LinqAltTitleNameRepository target = new LinqAltTitleNameRepository();
            AltTitleNameBO temp = null;

            using (IWorkUnit work = target.CreateWorkUnit())
            {
                temp = target.Find(x => x.TitleId == 1 && x.Name == "Test", work).FirstOrDefault();

                Assert.IsNull(temp);

                target.Save(new AltTitleNameBO()
                {
                    Name = "Test",
                    TitleId = 1
                }, work);

                temp = target.Find(x => x.TitleId == 1 && x.Name == "Test", work).FirstOrDefault();

                Assert.IsNotNull(temp);

                target.Delete(temp, work);

                work.Rollback();
            }

            temp = target.Find(x => x.TitleId == 1 && x.Name == "Test").FirstOrDefault();
            Assert.IsNull(temp);
        }
    }
}
