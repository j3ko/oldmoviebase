﻿using J3ko.Moviebase.DAL.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using J3ko.Moviebase.BOL;
using J3ko.Foundation.Repository;

namespace J3ko.Moviebase.DAL.Test
{
    
    
    /// <summary>
    ///This is a test class for TitleRepositoryTest and is intended
    ///to contain all TitleRepositoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TitleRepositoryTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for TitleRepository Constructor
        ///</summary>
        [TestMethod()]
        public void TitleRepositoryConstructorTest()
        {
            LinqTitleRepository target = new LinqTitleRepository();
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void GetAllExecutes()
        {
            LinqTitleRepository target = new LinqTitleRepository();
            TitleBO result = target.Find(x => x.Id == 29).FirstOrDefault();
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void SaveGetDelete()
        {
            LinqTitleRepository target = new LinqTitleRepository();
            TitleBO temp = null;

            using (IWorkUnit work = target.CreateWorkUnit())
            {
                Assert.IsNull(temp);

                temp = GetSimpleTitle("SaveGetDelete");

                target.Save(temp, work);

                temp = target.Find(x => x.Id == temp.Id, work).FirstOrDefault();

                Assert.IsNotNull(temp);

                target.Delete(temp, work);

                work.Rollback();
            }

            temp = target.Find(x => x.Id == temp.Id).FirstOrDefault();
            Assert.IsNull(temp);
        }

        #region Private Helpers
        private TitleBO GetSimpleTitle(string method)
        {
            TitleBO title = new TitleBO()
            {
                Name = method,
                Plot = "Test Plot (" + method + ")",
                Category = new CategoryBO()
                {
                    Id = 1
                },
            };

            GenreBO genre1 = new GenreBO()
            {
                Id = 4
            };
            GenreBO genre2 = new GenreBO()
            {
                Id = 6
            };
            GenreBO genre3 = new GenreBO()
            {
                Id = 2
            };

            title.Genres.Add(genre1);
            title.Genres.Add(genre2);
            title.Genres.Add(genre3);

            GroupBO group = new GroupBO()
            {
                Description = "Test Group (" + method + ")",
                DateAdded = DateTime.Now,
                Media = new MediaBO()
                {
                    Id = 1
                },
                Slot = new SlotBO()
                {
                    Id = 465,
                    Container = new ContainerBO()
                    {
                        Id = 1
                    },
                }
            };

            EpisodeBO groupedEpisode = new EpisodeBO()
            {
                Name = "Test Episode (" + method + ")",
                Instance = new InstanceBO()
                {
                    Id = 1,
                }
            };

            EpisodeBO grouplessEpisode = new EpisodeBO()
            {
                Name = "Test Groupless Episode (" + method + ")",
                Instance = new InstanceBO()
                {
                    Id = 2,
                }
            };

            group.Add(groupedEpisode);
            title.Add(group);
            title.AddEpisode(grouplessEpisode);

            return title;
        }
        #endregion
    }
}
