﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.DAL.Datasources;
using J3ko.Foundation.Repository.Linq;
using J3ko.Foundation.Repository;

namespace J3ko.Moviebase.DAL.Repositories
{
    public abstract class MoviebaseRepository<TEntity> : LinqRepositoryBase<TEntity, Moviedata>
        where TEntity : class
    {
        public MoviebaseRepository()
            : base(new MoviebaseContextFactory())
        { }


        protected abstract override void OnSave(TEntity entity, LinqWorkUnit<Moviedata> work);

        protected abstract override void OnDelete(TEntity entity, LinqWorkUnit<Moviedata> work);

        protected abstract override IQueryable<TEntity> OnGetAll(LinqWorkUnit<Moviedata> work);

        //public virtual void DeleteAll(IWorkUnit work)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
