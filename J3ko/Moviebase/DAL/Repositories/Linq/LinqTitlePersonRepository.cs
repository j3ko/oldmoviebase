﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Datasources;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqTitlePersonRepository : MoviebaseRepository<TitlePersonBO>
    {

        protected override void OnSave(TitlePersonBO entity, Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            title_person a = (from i in work.Context.title_persons
                              where i.title_id == entity.Id && i.person_id == entity.Id && i.ordinal == entity.Ordinal
                              select i).SingleOrDefault();

            if (a == null)
            {
                a = new title_person();
                a.title_id = entity.TitleId;
                a.person_id = entity.Id;
                a.ordinal = entity.Ordinal;
                work.Context.title_persons.InsertOnSubmit(a);
            }

            a.character_name = entity.CharacterName;
            a.title_role_id = (int)entity.Role;

            work.Context.SubmitChanges();
        }

        protected override void OnDelete(TitlePersonBO entity, Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            title_person p = (from i in work.Context.title_persons
                              where i.title_id == entity.TitleId && i.person_id == entity.Id && i.ordinal == entity.Ordinal
                              select i).SingleOrDefault();

            work.Context.title_persons.DeleteOnSubmit(p);
            work.Context.SubmitChanges();
        }

        protected override IQueryable<TitlePersonBO> OnGetAll(Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            var result = from i in work.Context.title_persons
                         from p in GetPeople(work).Where(x => x.Id == i.person_id).DefaultIfEmpty()
                         select new TitlePersonBO
                         {
                             Id = p.Id,
                             FullName = p.FullName,
                             TmdbId = p.TmdbId,
                             ThumbnailId = p.ThumbnailId,

                             CharacterName = i.character_name,
                             Ordinal = i.ordinal,
                             Role = (TitleRole)i.title_role_id,
                             TitleId = i.title_id,
                         };

            return result;
        }


        private IQueryable<PersonBO> GetPeople(Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            LinqPersonRepository prepo = new LinqPersonRepository();
            return prepo.GetAll(work);
        }
    }
}
