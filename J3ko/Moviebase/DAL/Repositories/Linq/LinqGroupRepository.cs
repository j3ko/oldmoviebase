﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Datasources;
using J3ko.Common.Utils;
using J3ko.Foundation.Repository;
using J3ko.Foundation.Repository.Linq;
using J3ko.Common.Linq;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqGroupRepository : MoviebaseRepository<GroupBO>
    {
        protected override void OnSave(GroupBO entity, LinqWorkUnit<Moviedata> work)
        {
            title_group a = (from i in work.Context.title_groups
                             where i.title_group_id == entity.Id
                             select i).SingleOrDefault() ?? new title_group();

            a.description = entity.Description;
            a.date_burned = (entity.DateBurned.IsValidSqlDateTime()) ? entity.DateBurned : DateTime.Now;
            a.slot_id = entity.Slot.Id;
            a.media_id = entity.Media.Id;
            a.title_id = entity.TitleId;

            if (a.title_group_id <= 0)
            {
                a.date_added = (entity.DateAdded.IsValidSqlDateTime()) ? entity.DateAdded : DateTime.Now;
                work.Context.title_groups.InsertOnSubmit(a);
            }

            work.Context.SubmitChanges();

            entity.ToList().ForEach(x => { x.GroupId = a.title_group_id; x.TitleId = a.title_id; });
            SaveEpisodesByGroup(entity, work);

            entity.Id = a.title_group_id;
        }

        protected override void OnDelete(GroupBO entity, LinqWorkUnit<Moviedata> work)
        {
            title_group a = (from i in work.Context.title_groups
                             where i.title_group_id == entity.Id
                             select i).SingleOrDefault();

            work.Context.title_groups.DeleteOnSubmit(a);
            work.Context.SubmitChanges();
        }

        protected override IQueryable<GroupBO> OnGetAll(LinqWorkUnit<Moviedata> work)
        {
            var result = from i in work.Context.title_groups
                         from m in GetMedia(work).Where(x => x.Id == i.media_id).DefaultIfEmpty()
                         from s in GetSlots(work).Where(x => x.Id == i.slot_id).DefaultIfEmpty()
                         //from c in GetContainers().Where(x => x.Id == i.container_id).DefaultIfEmpty()
                         let eps = GetEpisodes(i.title_group_id, work)
                         select new GroupBO
                         {
                             Id = i.title_group_id,
                             Description = i.description,
                             DateBurned = i.date_burned,
                             DateAdded = i.date_added,
                             TitleId = i.title_id,
                             Media = m,
                             Slot = s,
                             //Container = c,

                             Episodes = LazyList<EpisodeBO>.Create(eps)
                         };

            return result;
        }

        private IQueryable<MediaBO> GetMedia(LinqWorkUnit<Moviedata> work)
        {
            LinqMediaRepository mrepo = new LinqMediaRepository();
            return mrepo.GetAll(work);
        }

        private IQueryable<SlotBO> GetSlots(LinqWorkUnit<Moviedata> work)
        {
            LinqSlotRepository srepo = new LinqSlotRepository();
            return srepo.GetAll(work);
        }

        private IQueryable<EpisodeBO> GetEpisodes(int groupId, LinqWorkUnit<Moviedata> work)
        {
            LinqEpisodeRepository erepo = new LinqEpisodeRepository();
            return from e in erepo.GetAll(work)
                   where e.GroupId == groupId
                   select e;
        }

        private void SaveEpisodesByGroup(IEnumerable<EpisodeBO> episodes, LinqWorkUnit<Moviedata> work)
        {
            LinqEpisodeRepository erepo = new LinqEpisodeRepository();

            foreach (EpisodeBO e in episodes)
            {
                erepo.Save(e, work);
            }
        }
    }
}
