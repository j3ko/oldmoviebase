﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Datasources;
using J3ko.Foundation.Repository;
using J3ko.Foundation.Repository.Linq;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqSlotRepository : MoviebaseRepository<SlotBO>
    {
        protected override void OnSave(SlotBO entity, LinqWorkUnit<Moviedata> work)
        {
            throw new NotImplementedException();
        }

        protected override void OnDelete(SlotBO entity, LinqWorkUnit<Moviedata> work)
        {
            throw new NotImplementedException();
        }

        protected override IQueryable<SlotBO> OnGetAll(LinqWorkUnit<Moviedata> work)
        {
            return from s in work.Context.vw_available_slots
                   from c in GetContainers(work).Where(x => x.Id == s.container_id).DefaultIfEmpty()
                   select new SlotBO
                   {
                       Id = s.slot_id,
                       Label = s.slot_label,
                       Container = c,
                       IsAvailable = s.is_available.GetValueOrDefault(),
                   };
        }
        private IQueryable<ContainerBO> GetContainers(LinqWorkUnit<Moviedata> work)
        {
            LinqContainerRepository crepo = new LinqContainerRepository();
            return crepo.GetAll(work);
        }
    }
}
