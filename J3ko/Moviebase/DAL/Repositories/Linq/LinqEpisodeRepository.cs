﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Datasources;
using J3ko.Foundation.Repository;
using J3ko.Foundation.Repository.Linq;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqEpisodeRepository : MoviebaseRepository<EpisodeBO>
    {
        protected override void OnSave(EpisodeBO entity, LinqWorkUnit<Moviedata> work)
        {
            episode e = (from i in work.Context.episodes
                         where i.instance_id == entity.Instance.Id && i.title_id == entity.TitleId
                         select i).SingleOrDefault() ?? new episode();

            e.episode_name = entity.Name;
            e.title_group_id = (entity.IsGrouped) ? entity.GroupId : (int?)null;

            if (e.instance_id <= 0 && e.title_id <= 0)
            {
                e.instance_id = entity.Instance.Id;
                e.title_id = entity.TitleId;
                work.Context.episodes.InsertOnSubmit(e);
            }

            work.Context.SubmitChanges();
        }

        protected override void OnDelete(EpisodeBO entity, LinqWorkUnit<Moviedata> work)
        {
            episode e = (from i in work.Context.episodes
                         where i.instance_id == entity.Instance.Id && i.title_id == entity.TitleId
                         select i).SingleOrDefault();

            work.Context.episodes.DeleteOnSubmit(e);
            work.Context.SubmitChanges();
        }

        protected override IQueryable<EpisodeBO> OnGetAll(LinqWorkUnit<Moviedata> work)
        {
            var result = from e in work.Context.episodes
                         from inst in GetInstances(work).Where(x => x.Id == e.instance_id).DefaultIfEmpty()
                         select new EpisodeBO
                         {
                             Name = e.episode_name,
                             Instance = inst,
                             TitleId = e.title_id,
                             GroupId = e.title_group_id.GetValueOrDefault(int.MinValue),
                         };

            return result;
        }

        private IQueryable<InstanceBO> GetInstances(LinqWorkUnit<Moviedata> work)
        {
            LinqInstanceRepository irepo = new LinqInstanceRepository();
            return irepo.GetAll(work);
        }
    }
}
