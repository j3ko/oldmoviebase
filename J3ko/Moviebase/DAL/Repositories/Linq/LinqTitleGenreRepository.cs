﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Datasources;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqTitleGenreRepository : MoviebaseRepository<TitleGenreMapBO>
    {
        protected override void OnSave(TitleGenreMapBO entity, Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            genre_title g = (from i in work.Context.genre_titles
                             where i.genre_id == entity.GenreId && i.title_id == entity.TitleId
                             select i).SingleOrDefault();

            if (g == null)
            {
                g = new genre_title();
                g.genre_id = entity.GenreId;
                g.title_id = entity.TitleId;
                work.Context.genre_titles.InsertOnSubmit(g);
                work.Context.SubmitChanges();
            }
        }

        protected override void OnDelete(TitleGenreMapBO entity, Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            genre_title g = (from i in work.Context.genre_titles
                             where i.genre_id == entity.GenreId && i.title_id == entity.TitleId
                             select i).SingleOrDefault();

            work.Context.genre_titles.DeleteOnSubmit(g);
            work.Context.SubmitChanges();
        }

        protected override IQueryable<TitleGenreMapBO> OnGetAll(Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            return from i in work.Context.genre_titles
                   select new TitleGenreMapBO
                   {
                       TitleId = i.title_id,
                       GenreId = i.genre_id,
                   };
        }
    }
}
