﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Datasources;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqPersonRepository : MoviebaseRepository<PersonBO>
    {
        protected override void OnSave(PersonBO entity, Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            person a = (from i in work.Context.persons
                        where i.person_id == entity.Id
                        select i).SingleOrDefault() ?? new person();

            if (a.person_id <= 0)
            {
                work.Context.persons.InsertOnSubmit(a);
            }

            a.full_name = entity.FullName;
            a.thumbnail_id = (entity.ThumbnailId == Guid.Empty) ? (Guid?)null : entity.ThumbnailId;
            a.tmdb_id = entity.TmdbId <= 0 ? (int?)null : entity.TmdbId;

            work.Context.SubmitChanges();

            entity.Id = a.person_id;
        }

        protected override void OnDelete(PersonBO entity, Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            throw new NotImplementedException();
        }

        protected override IQueryable<PersonBO> OnGetAll(Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            var result = from i in work.Context.persons
                         select new PersonBO
                         {
                             Id = i.person_id,
                             FullName = i.full_name,
                             TmdbId = i.tmdb_id.GetValueOrDefault(),
                             ThumbnailId = i.thumbnail_id.GetValueOrDefault(),
                         };


            return result;
        }
    }
}
