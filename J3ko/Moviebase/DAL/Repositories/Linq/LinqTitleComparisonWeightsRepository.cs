﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqTitleComparisonWeightsRepository : MoviebaseRepository<TitleComparisonWeightsBO>
    {
        protected override void OnSave(TitleComparisonWeightsBO entity, Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            throw new NotImplementedException();
        }

        protected override void OnDelete(TitleComparisonWeightsBO entity, Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            throw new NotImplementedException();
        }

        protected override IQueryable<TitleComparisonWeightsBO> OnGetAll(Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            return from i in work.Context.vw_comparison_weights
                   select new TitleComparisonWeightsBO
                   {
                       TitleId = i.title_id,
                       Weights = i.weights.Split(new char[] { ',' }),
                   };
        }
    }
}
