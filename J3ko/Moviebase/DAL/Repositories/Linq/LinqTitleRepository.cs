﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Datasources;
using J3ko.Common.Utils;
using J3ko.Foundation.Repository;
using J3ko.Foundation.Repository.Linq;
using J3ko.Common.Linq;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqTitleRepository : MoviebaseRepository<TitleBO>
    {
        protected override void OnSave(TitleBO entity, LinqWorkUnit<Moviedata> work)
        {
            title a = (from i in work.Context.titles
                       where i.title_id == entity.Id
                       select i).SingleOrDefault() ?? new title();

            a.title_name = entity.Name;
            a.category_id = entity.Category.Id;
            a.cover_id = (entity.CoverId == Guid.Empty) ? (Guid?)null : entity.CoverId;
            a.small_cover_id = (entity.CoverSmallId == Guid.Empty) ? (Guid?)null : entity.CoverSmallId;
            a.plot = entity.Plot;
            a.imdb_id = entity.ImdbId;
            a.released = (entity.Released.IsValidSqlDateTime()) ? entity.Released : DateTime.Now;
            a.synced = entity.Synced;

            if (a.title_id <= 0)
                work.Context.titles.InsertOnSubmit(a);

            work.Context.SubmitChanges();

            // Expand Lists
            // todo: find some better way to handle this
            entity.Genres.ToList();
            entity.AltNames.ToList().ForEach(x => x.TitleId = a.title_id);
            entity.ToList().ForEach(x => x.TitleId = a.title_id);
            entity.DefaultGroup.ToList().ForEach(x => { x.TitleId = a.title_id; x.GroupId = int.MinValue; });
            entity.TitlePeople.ToList();

            // Save Genres
            SaveTitleGenres(a.title_id, entity.Genres, work);
            
            // Save Alt. names
            SaveAltTitles(entity.AltNames, work);

            // Save Groups
            SaveGroupsByTitle(entity.Where(x => !(x is DefaultGroupBO)), work);

            // Save Episodes
            SaveEpisodesByTitle(entity.DefaultGroup, work);

            // Save Title People
            SaveTitlePeople(a.title_id, entity.TitlePeople, work);

            work.Context.SubmitChanges();

            entity.Id = a.title_id;
        }

        protected override void OnDelete(TitleBO entity, LinqWorkUnit<Moviedata> work)
        {
            title a = (from i in work.Context.titles
                       where i.title_id == entity.Id
                       select i).SingleOrDefault();

            // Delete Genres
            DeleteTitleGenres(entity.Id, work);

            // Save Alt. names
            DeleteAltTitles(entity.Id, work);

            // Delete Episodes
            DeleteEpisodes(entity.Id, work);

            // Delete Groups
            DeleteGroups(entity.Id, work);

            // Delete Title People
            DeleteTitlePeople(entity.Id, work);

            work.Context.titles.DeleteOnSubmit(a);
        }

        protected override IQueryable<TitleBO> OnGetAll(LinqWorkUnit<Moviedata> work)
        {
            var result = from i in work.Context.titles
                         from c in GetCategories(work).Where(x => x.Id == i.category_id).DefaultIfEmpty()
                         let grps = GetGroups(i.title_id, work)
                         let eps = GetEpisodes(i.title_id, work).WithNoGroup()
                         let genres = GetGenres(i.title_id, work)
                         let ppl = GetTitlePeople(i.title_id, work)
                         select new TitleBO
                         {
                             Id = i.title_id,
                             Name = i.title_name,
                             Category = c,
                             CoverId = i.cover_id ?? Guid.Empty,
                             CoverSmallId = i.small_cover_id ?? Guid.Empty,
                             ImdbId = i.imdb_id,
                             Plot = i.plot,
                             Released = i.released,
                             Synced = i.synced,
                             Modified = i.modified,
                             DefaultGroupEpisodes = LazyList<EpisodeBO>.Create(eps),
                             Groups = LazyList<GroupBO>.Create(grps),
                             Genres = LazyList<GenreBO>.Create(genres),
                             TitlePeople = LazyList<TitlePersonBO>.Create(ppl),
                         };

            return result;
        }

        #region Get Helpers

        private IQueryable<CategoryBO> GetCategories(LinqWorkUnit<Moviedata> work)
        {
            LinqCategoryRepository crepo = new LinqCategoryRepository();
            return crepo.GetAll(work);
        }

        private IQueryable<GroupBO> GetGroups(int titleId, LinqWorkUnit<Moviedata> work)
        {
            LinqGroupRepository grepo = new LinqGroupRepository();

            return from i in grepo.GetAll(work)
                   where i.TitleId == titleId
                   select i;
        }

        private IQueryable<EpisodeBO> GetEpisodes(int titleId, LinqWorkUnit<Moviedata> work)
        {
            LinqEpisodeRepository erepo = new LinqEpisodeRepository();

            return from i in erepo.GetAll(work)
                   where i.TitleId == titleId
                   select i;
        }

        private IQueryable<GenreBO> GetGenres(int titleId, LinqWorkUnit<Moviedata> work)
        {
            LinqTitleGenreRepository tgrepo = new LinqTitleGenreRepository();

            return from tg in tgrepo.GetAll(work).Where(x => x.TitleId == titleId)
                   join g in GetGenres(work) on tg.GenreId equals g.Id
                   select g;
        }

        private IQueryable<TitlePersonBO> GetTitlePeople(int titleId, LinqWorkUnit<Moviedata> work)
        {
            LinqTitlePersonRepository tprepo = new LinqTitlePersonRepository();

            return from i in tprepo.GetAll(work)
                   where i.TitleId == titleId
                   orderby i.Ordinal
                   select i;
        }

        private IQueryable<GenreBO> GetGenres(LinqWorkUnit<Moviedata> work)
        {
            LinqGenreRepository grepo = new LinqGenreRepository();
            return grepo.GetAll(work);
        }
        #endregion

        #region Save Helpers
        private void SaveGroupsByTitle(IEnumerable<GroupBO> groups, LinqWorkUnit<Moviedata> work)
        {
            LinqGroupRepository grepo = new LinqGroupRepository();

            foreach(GroupBO g in groups)
            {
                grepo.Save(g, work);
            }

        }

        private void SaveEpisodesByTitle(IEnumerable<EpisodeBO> episodes, LinqWorkUnit<Moviedata> work)
        {
            LinqEpisodeRepository erepo = new LinqEpisodeRepository();

            foreach (EpisodeBO e in episodes)
            {
                erepo.Save(e, work);
            }
        }

        private void SaveTitleGenres(int titleId, IEnumerable<GenreBO> genres, LinqWorkUnit<Moviedata> work)
        {
            LinqTitleGenreRepository tgrepo = new LinqTitleGenreRepository();

            IEnumerable<TitleGenreMapBO> titleGenres = from i in tgrepo.GetAll(work)
                                                       where i.TitleId == titleId
                                                       select i;

            foreach (TitleGenreMapBO tg in titleGenres)
            {
                tgrepo.Delete(tg, work);
            }

            foreach (GenreBO g in genres)
            {
                tgrepo.Save(new TitleGenreMapBO 
                { 
                    TitleId = titleId,
                    GenreId = g.Id 
                }, work);
            }
        }

        private void SaveTitlePeople(int titleId, IEnumerable<TitlePersonBO> people, LinqWorkUnit<Moviedata> work)
        {
            LinqTitlePersonRepository tprepo = new LinqTitlePersonRepository();

            IEnumerable<TitlePersonBO> ppl = from i in tprepo.GetAll(work)
                                             where i.TitleId == titleId
                                             select i;

            foreach (TitlePersonBO tp in ppl)
            {
                tprepo.Delete(tp, work);
            }

            foreach (TitlePersonBO p in people)
            {
                p.TitleId = titleId;
                tprepo.Save(p, work);
            }
        }

        private void SaveAltTitles(IEnumerable<AltTitleNameBO> names, LinqWorkUnit<Moviedata> work)
        {
            LinqAltTitleNameRepository arepo = new LinqAltTitleNameRepository();

            foreach (AltTitleNameBO a in names)
            {
                arepo.Save(a, work);
            }
        }
        #endregion

        #region Delete Helpers
        private void DeleteTitlePeople(int titleId, LinqWorkUnit<Moviedata> work)
        {
            LinqTitlePersonRepository prepo = new LinqTitlePersonRepository();

            IEnumerable<TitlePersonBO> ppl = from i in prepo.GetAll(work)
                                             where i.TitleId == titleId
                                             select i;

            foreach (TitlePersonBO p in ppl)
            {
                prepo.Delete(p, work);
            }
        }

        private void DeleteEpisodes(int titleId, LinqWorkUnit<Moviedata> work)
        {
            LinqEpisodeRepository erepo = new LinqEpisodeRepository();

            IEnumerable<EpisodeBO> episodes = from i in erepo.GetAll(work)
                                              where i.TitleId == titleId
                                              select i;

            foreach (EpisodeBO e in episodes)
            {
                erepo.Delete(e, work);
            }
        }

        private void DeleteGroups(int titleId, LinqWorkUnit<Moviedata> work)
        {
            LinqGroupRepository grepo = new LinqGroupRepository();

            IEnumerable<GroupBO> titleGroups = from i in grepo.GetAll(work)
                                               where i.TitleId == titleId
                                               select i;

            foreach (GroupBO g in titleGroups)
            {
                grepo.Delete(g, work);
            }
        }

        private void DeleteTitleGenres(int titleId, LinqWorkUnit<Moviedata> work)
        {
            LinqTitleGenreRepository tgrepo = new LinqTitleGenreRepository();

            IEnumerable<TitleGenreMapBO> genreTitles = from i in tgrepo.GetAll(work)
                                                       where i.TitleId == titleId
                                                       select i;

            foreach (TitleGenreMapBO tg in genreTitles)
            {
                tgrepo.Delete(tg, work);
            }
        }

        private void DeleteAltTitles(int titleId, LinqWorkUnit<Moviedata> work)
        {
            LinqAltTitleNameRepository arepo = new LinqAltTitleNameRepository();

            IEnumerable<AltTitleNameBO> altNames = from i in arepo.GetAll(work)
                                                   where i.TitleId == titleId
                                                   select i;

            foreach (AltTitleNameBO a in altNames)
            {
                arepo.Delete(a, work);
            }
        }
        #endregion

    }
}
