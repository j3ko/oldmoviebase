﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Datasources;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqInstanceRepository : MoviebaseRepository<InstanceBO>
    {
        protected override void OnSave(InstanceBO entity, Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            instance g = (from i in work.Context.instances
                          where i.instance_id == entity.Id
                          select i).SingleOrDefault() ?? new instance();

            g.instance_id = entity.Id;
            g.instance_desc = entity.Description;

            if (g.instance_id <= 0)
                work.Context.instances.InsertOnSubmit(g);

            work.Context.SubmitChanges();
        }

        protected override void OnDelete(InstanceBO entity, Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            instance g = (from i in work.Context.instances
                          where i.instance_id == entity.Id
                          select i).SingleOrDefault() ?? new instance();

            work.Context.instances.DeleteOnSubmit(g);
            work.Context.SubmitChanges();
        }

        protected override IQueryable<InstanceBO> OnGetAll(Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            return from i in work.Context.instances
                   select new InstanceBO
                   {
                       Id = i.instance_id,
                       Description = i.instance_desc,
                   };
        }
    }
}
