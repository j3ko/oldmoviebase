﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Datasources;
using J3ko.Common.Linq;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqGenreRepository : MoviebaseRepository<GenreBO>
    {
        protected override void OnSave(GenreBO entity, Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            genre g = (from i in work.Context.genres
                       where i.genre_id == entity.Id
                       select i).SingleOrDefault() ?? new genre();

            g.genre_id = entity.Id;
            g.genre_name = entity.Name;

            if (g.genre_id <= 0)
                work.Context.genres.InsertOnSubmit(g);

            work.Context.SubmitChanges();
        }

        protected override void OnDelete(GenreBO entity, Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            genre g = (from i in work.Context.genres
                       where i.genre_id == entity.Id
                       select i).SingleOrDefault() ?? new genre();

            work.Context.genres.DeleteOnSubmit(g);
            work.Context.SubmitChanges();
        }

        protected override IQueryable<GenreBO> OnGetAll(Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            return from i in work.Context.vw_genres
                   let tids = GetTmdbIds(i.genre_id, work)
                   select new GenreBO
                   {
                       Id = i.genre_id,
                       Name = i.genre_name,
                       IsUsed = i.is_used.GetValueOrDefault(),
                       TmdbIds = LazyList<int>.Create(tids),
                   };
        }

        private IQueryable<int> GetTmdbIds(int genreId, J3ko.Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            return from t in work.Context.genre_tmdbs
                   where t.genre_id == genreId
                   select t.tmdb_id;
        }
    }
}
