﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Datasources;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqAltTitleNameRepository : MoviebaseRepository<AltTitleNameBO>
    {
        protected override void OnSave(AltTitleNameBO entity, Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            alt_title_name a = (from i in work.Context.alt_title_names
                                where i.title_id == entity.TitleId && i.name == entity.Name
                                select i).SingleOrDefault();

            if (a != null)
            {
                work.Context.alt_title_names.DeleteOnSubmit(a);
                work.Context.SubmitChanges();
            }

            a = new alt_title_name();
            a.title_id = entity.TitleId;
            a.name = entity.Name;

            work.Context.alt_title_names.InsertOnSubmit(a);

            work.Context.SubmitChanges();
        }

        protected override void OnDelete(AltTitleNameBO entity, Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            alt_title_name a = (from i in work.Context.alt_title_names
                                where i.title_id == entity.TitleId && i.name == entity.Name
                                select i).SingleOrDefault();

            work.Context.alt_title_names.DeleteOnSubmit(a);
            work.Context.SubmitChanges();
        }

        protected override IQueryable<AltTitleNameBO> OnGetAll(Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            return from i in work.Context.alt_title_names
                   select new AltTitleNameBO
                   {
                       Name = i.name,
                       TitleId = i.title_id,
                   };
        }
    }
}
