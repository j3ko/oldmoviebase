﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqContainerRepository : MoviebaseRepository<ContainerBO>
    {
        protected override void OnSave(ContainerBO entity, Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            throw new NotImplementedException();
        }

        protected override void OnDelete(ContainerBO entity, Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            throw new NotImplementedException();
        }

        protected override IQueryable<ContainerBO> OnGetAll(Foundation.Repository.Linq.LinqWorkUnit<Datasources.Moviedata> work)
        {
            return from c in work.Context.containers
                   select new ContainerBO
                   {
                       Id = c.container_id,
                       Name = c.container_name,
                   };
        }
    }
}
