﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Foundation.Repository.Linq;
using J3ko.Moviebase.DAL.Datasources;
using J3ko.Foundation.Repository;
using System.Data.Linq.Mapping;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqTitleSuggestionRepository : MoviebaseRepository<TitleSuggestionMapBO>
    {
        protected override void OnSave(TitleSuggestionMapBO entity, LinqWorkUnit<Moviedata> work)
        {
            title_suggestion t = (from i in work.Context.title_suggestions
                                  where i.title_id == entity.TitleId && i.suggestion_id == entity.SuggestionId
                                  select i).SingleOrDefault();

            if (t == null)
            {
                t = new title_suggestion();
                t.title_id = entity.TitleId;
                t.suggestion_id = entity.SuggestionId;
                t.distance = entity.Distance;
                work.Context.title_suggestions.InsertOnSubmit(t);
            }
            else
            {
                t.distance = entity.Distance;
            }
            work.Context.SubmitChanges();
        }

        protected override void OnDelete(TitleSuggestionMapBO entity, LinqWorkUnit<Moviedata> work)
        {
            title_suggestion t = (from i in work.Context.title_suggestions
                                  where i.title_id == entity.TitleId && i.suggestion_id == entity.SuggestionId
                                  select i).SingleOrDefault();

            work.Context.title_suggestions.DeleteOnSubmit(t);
            work.Context.SubmitChanges();
        }

        //public override void DeleteAll(IWorkUnit work)
        //{
        //    LinqWorkUnit<Moviedata> mwork = work as LinqWorkUnit<Moviedata>;

        //    if (work == null)
        //        throw new ArgumentNullException("work");
        //    if (mwork == null)
        //        throw new ArgumentException(string.Format("Expecting IWorkUnit of type LinqWorkUnit<Moviedata> but {0} was found", work.GetType().FullName));

        //    MetaTable t = mwork.Context.Mapping.GetTable(typeof(title_suggestion));
        //    mwork.Context.ExecuteCommand(string.Format("DELETE FROM {0}", t.TableName));
        //}

        protected override IQueryable<TitleSuggestionMapBO> OnGetAll(LinqWorkUnit<Moviedata> work)
        {
            return from i in work.Context.title_suggestions
                   select new TitleSuggestionMapBO
                   {
                       TitleId = i.title_id,
                       SuggestionId = i.suggestion_id,
                       Distance = i.distance
                   };
        }
    }
}
