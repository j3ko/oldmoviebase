﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Datasources;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqMediaRepository : MoviebaseRepository<MediaBO>
    {
        protected override void OnSave(MediaBO entity, Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            throw new NotImplementedException();
        }

        protected override void OnDelete(MediaBO entity, Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            throw new NotImplementedException();
        }

        protected override IQueryable<MediaBO> OnGetAll(Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            return from m in work.Context.medias
                   from f in GetMediaFormats(work).Where(x => x.MediaFormatId == m.media_format_id).DefaultIfEmpty()
                   select new MediaBO
                   {
                       Id = m.media_id,
                       Name = m.media_name,
                       Format = f,
                   };
        }
        private IQueryable<MediaFormatBO> GetMediaFormats(J3ko.Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            return from e in work.Context.media_formats
                   select new MediaFormatBO
                   {
                       MediaFormatId = e.media_format_id,
                       Description = e.description,
                   };
        }
    }
}
