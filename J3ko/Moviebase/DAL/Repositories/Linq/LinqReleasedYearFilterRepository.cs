﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Datasources;
using J3ko.Foundation.Repository.Linq;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqReleasedYearFilterRepository : MoviebaseRepository<ReleasedYearFilterBO>
    {
        protected override void OnSave(ReleasedYearFilterBO entity, LinqWorkUnit<Moviedata> work)
        {
            throw new NotImplementedException();
        }

        protected override void OnDelete(ReleasedYearFilterBO entity, LinqWorkUnit<Moviedata> work)
        {
            throw new NotImplementedException();
        }

        protected override IQueryable<ReleasedYearFilterBO> OnGetAll(LinqWorkUnit<Moviedata> work)
        {
            return from i in work.Context.vw_available_released_years
                   select new ReleasedYearFilterBO()
                   {
                       Year = i.released_year.GetValueOrDefault(DateTime.Now.Year),
                   };
        }
    }
}
