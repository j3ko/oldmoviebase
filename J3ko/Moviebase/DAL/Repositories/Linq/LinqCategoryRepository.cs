﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Datasources;

namespace J3ko.Moviebase.DAL.Repositories
{
    public class LinqCategoryRepository : MoviebaseRepository<CategoryBO>
    {
        protected override void OnSave(CategoryBO entity, Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            throw new NotImplementedException();
        }

        protected override void OnDelete(CategoryBO entity, Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            throw new NotImplementedException();
        }

        protected override IQueryable<CategoryBO> OnGetAll(Foundation.Repository.Linq.LinqWorkUnit<Moviedata> work)
        {
            return from i in work.Context.categories
                   select new CategoryBO
                   {
                       Id = i.category_id,
                       Name = i.category_name
                   };
        }
    }
}
