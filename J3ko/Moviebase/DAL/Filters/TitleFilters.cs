﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using J3ko.Moviebase.BOL;

namespace J3ko.Moviebase.DAL
{
    public static class TitleFilters
    {
        public static IQueryable<TitleBO> WithCatagoryId(this IQueryable<TitleBO> qry, int id)
        {
            return from i in qry
                   where i.Category.Id == id
                   select i;
        }

        public static IQueryable<TitleBO> Search(this IQueryable<TitleBO> qry, string term)
        {
            return from i in qry
                   where i.Name.Contains(term)
                   select i;
        }

        public static TitleBO WithTitleId(this IQueryable<TitleBO> qry, int id)
        {
            return (from i in qry
                   where i.Id == id
                   select i).SingleOrDefault();
        }

        public static GenreBO WithGenreId(this IQueryable<GenreBO> qry, int id)
        {
            return (from i in qry
                   where i.Id == id
                   select i).SingleOrDefault();
        }

        public static CategoryBO WithCategoryId(this IQueryable<CategoryBO> qry, int id)
        {
            return (from i in qry
                   where i.Id == id
                   select i).SingleOrDefault();
        }

        public static IQueryable<TitleBO> OrderByNameAscending(this IQueryable<TitleBO> qry)
        {
            return from i in qry
                   orderby i.Name
                   select i;
        }
    }
}
