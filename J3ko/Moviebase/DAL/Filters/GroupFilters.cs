﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using J3ko.Moviebase.BOL;

namespace J3ko.Moviebase.DAL
{
    public static class GroupFilters
    {
        public static GroupBO WithGroupId(this IQueryable<GroupBO> qry, int groupId)
        {
            return (from q in qry
                   where q.Id == groupId
                   select q).SingleOrDefault();
        }

        public static IQueryable<GroupBO> WithTitleId(this IQueryable<GroupBO> qry, int titleId)
        {
            return (from q in qry
                    where q.TitleId == titleId
                    select q).AsQueryable();
        }
    }
}
