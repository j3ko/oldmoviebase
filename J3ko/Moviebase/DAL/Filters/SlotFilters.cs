﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;

namespace J3ko.Moviebase.DAL
{
    public static class SlotFilters
    {
        public static IQueryable<SlotBO> IsAvailable(this IQueryable<SlotBO> qry)
        {
            return from q in qry
                   where q.IsAvailable
                   select q;
        }

        public static IQueryable<SlotBO> WithContainerId(this IQueryable<SlotBO> qry, int containerId)
        {
            return from q in qry
                   where q.Container.Id == containerId || containerId == 0
                   select q;
        }

        public static SlotBO WithId(this IQueryable<SlotBO> qry, int id)
        {
            return (from q in qry
                    where q.Id == id
                    select q).SingleOrDefault();
        }
    }
}
