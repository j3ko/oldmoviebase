﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using J3ko.Moviebase.BOL;

namespace J3ko.Moviebase.DAL
{
    public static class EpisodeFilters
    {
        public static IQueryable<EpisodeBO> WithGroupId(this IQueryable<EpisodeBO> qry, int groupId)
        {
            return from q in qry
                   where q.GroupId == groupId
                   select q;
        }

        public static IQueryable<EpisodeBO> WithTitleId(this IQueryable<EpisodeBO> qry, int titleId)
        {
            return from q in qry
                   where q.TitleId == titleId
                   select q;
        }

        public static IQueryable<EpisodeBO> WithNoGroup(this IQueryable<EpisodeBO> qry)
        {
            return from q in qry
                   where q.GroupId == int.MinValue
                   select q;
        }

        public static EpisodeBO WithTitleAndInstance(this IQueryable<EpisodeBO> qry, int titleId, int instanceId)
        {
            return (from q in qry
                    where q.TitleId == titleId && q.Instance.Id == instanceId
                    select q).SingleOrDefault();
        }
    }
}
