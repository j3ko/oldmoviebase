﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using J3ko.Moviebase.WFA.ViewModels;
using J3ko.Moviebase.BOL;

namespace J3ko.Moviebase.WFA
{
    public partial class MoviebaseMain : Form, IMainView
    {
        public MoviebaseMain()
        {
            InitializeComponent();
        }

        private void tsBtnTitle_Click(object sender, EventArgs e)
        {
        }

        #region IMainView Members

        public void OpenTitleView(TitleViewModel vm)
        {
            frmTitleView titleView = new frmTitleView();
            titleView.ViewModel = vm;
            titleView.MdiParent = this;
            titleView.Show();
        }
        #endregion
    }
}
