﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.WFA.ViewModels;

namespace J3ko.Moviebase.WFA
{
    public partial class frmTitleView : Form, ITitleView
    {
        public frmTitleView()
        {
            InitializeComponent();
        }

        #region ITitleView Members

        public TitleViewModel ViewModel
        {
            set 
            { 
                Title = value.Title; 
            }
        }

        #endregion

        private TitleBO Title
        {
            set
            {
                titleBOBindingSource.DataSource = value;
            }
        }
    }
}
