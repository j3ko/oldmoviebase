﻿namespace J3ko.Moviebase.WFA
{
    partial class frmTitleView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label idLabel;
            System.Windows.Forms.Label nameLabel;
            System.Windows.Forms.Label plotLabel;
            System.Windows.Forms.Label shortNameLabel;
            System.Windows.Forms.Label updatedByLabel;
            System.Windows.Forms.Label updatedDateLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.titleBOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.plotTextBox = new System.Windows.Forms.TextBox();
            this.shortNameTextBox = new System.Windows.Forms.TextBox();
            this.updatedByTextBox = new System.Windows.Forms.TextBox();
            this.updatedDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.tcTitle = new System.Windows.Forms.TabControl();
            this.tabEpisodes = new System.Windows.Forms.TabPage();
            this.episodesDataGridView = new System.Windows.Forms.DataGridView();
            this.episodesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabGroups = new System.Windows.Forms.TabPage();
            this.groupsDataGridView = new System.Windows.Forms.DataGridView();
            this.groupsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.coverSmallPictureBox = new System.Windows.Forms.PictureBox();
            idLabel = new System.Windows.Forms.Label();
            nameLabel = new System.Windows.Forms.Label();
            plotLabel = new System.Windows.Forms.Label();
            shortNameLabel = new System.Windows.Forms.Label();
            updatedByLabel = new System.Windows.Forms.Label();
            updatedDateLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.titleBOBindingSource)).BeginInit();
            this.tcTitle.SuspendLayout();
            this.tabEpisodes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.episodesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.episodesBindingSource)).BeginInit();
            this.tabGroups.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coverSmallPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // idLabel
            // 
            idLabel.AutoSize = true;
            idLabel.Location = new System.Drawing.Point(257, 15);
            idLabel.Name = "idLabel";
            idLabel.Size = new System.Drawing.Size(19, 13);
            idLabel.TabIndex = 5;
            idLabel.Text = "Id:";
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.Location = new System.Drawing.Point(257, 41);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(38, 13);
            nameLabel.TabIndex = 7;
            nameLabel.Text = "Name:";
            // 
            // plotLabel
            // 
            plotLabel.AutoSize = true;
            plotLabel.Location = new System.Drawing.Point(257, 67);
            plotLabel.Name = "plotLabel";
            plotLabel.Size = new System.Drawing.Size(28, 13);
            plotLabel.TabIndex = 9;
            plotLabel.Text = "Plot:";
            // 
            // shortNameLabel
            // 
            shortNameLabel.AutoSize = true;
            shortNameLabel.Location = new System.Drawing.Point(257, 93);
            shortNameLabel.Name = "shortNameLabel";
            shortNameLabel.Size = new System.Drawing.Size(66, 13);
            shortNameLabel.TabIndex = 11;
            shortNameLabel.Text = "Short Name:";
            // 
            // updatedByLabel
            // 
            updatedByLabel.AutoSize = true;
            updatedByLabel.Location = new System.Drawing.Point(257, 119);
            updatedByLabel.Name = "updatedByLabel";
            updatedByLabel.Size = new System.Drawing.Size(66, 13);
            updatedByLabel.TabIndex = 13;
            updatedByLabel.Text = "Updated By:";
            // 
            // updatedDateLabel
            // 
            updatedDateLabel.AutoSize = true;
            updatedDateLabel.Location = new System.Drawing.Point(257, 146);
            updatedDateLabel.Name = "updatedDateLabel";
            updatedDateLabel.Size = new System.Drawing.Size(77, 13);
            updatedDateLabel.TabIndex = 15;
            updatedDateLabel.Text = "Updated Date:";
            // 
            // titleBOBindingSource
            // 
            this.titleBOBindingSource.DataSource = typeof(J3ko.Moviebase.BOL.TitleBO);
            // 
            // idTextBox
            // 
            this.idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.titleBOBindingSource, "Id", true));
            this.idTextBox.Location = new System.Drawing.Point(340, 12);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.Size = new System.Drawing.Size(200, 20);
            this.idTextBox.TabIndex = 6;
            // 
            // nameTextBox
            // 
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.titleBOBindingSource, "Name", true));
            this.nameTextBox.Location = new System.Drawing.Point(340, 38);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(200, 20);
            this.nameTextBox.TabIndex = 8;
            // 
            // plotTextBox
            // 
            this.plotTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.titleBOBindingSource, "Plot", true));
            this.plotTextBox.Location = new System.Drawing.Point(340, 64);
            this.plotTextBox.Name = "plotTextBox";
            this.plotTextBox.Size = new System.Drawing.Size(200, 20);
            this.plotTextBox.TabIndex = 10;
            // 
            // shortNameTextBox
            // 
            this.shortNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.titleBOBindingSource, "ShortName", true));
            this.shortNameTextBox.Location = new System.Drawing.Point(340, 90);
            this.shortNameTextBox.Name = "shortNameTextBox";
            this.shortNameTextBox.Size = new System.Drawing.Size(200, 20);
            this.shortNameTextBox.TabIndex = 12;
            // 
            // updatedByTextBox
            // 
            this.updatedByTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.titleBOBindingSource, "UpdatedBy", true));
            this.updatedByTextBox.Location = new System.Drawing.Point(340, 116);
            this.updatedByTextBox.Name = "updatedByTextBox";
            this.updatedByTextBox.Size = new System.Drawing.Size(200, 20);
            this.updatedByTextBox.TabIndex = 14;
            // 
            // updatedDateDateTimePicker
            // 
            this.updatedDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.titleBOBindingSource, "UpdatedDate", true));
            this.updatedDateDateTimePicker.Location = new System.Drawing.Point(340, 142);
            this.updatedDateDateTimePicker.Name = "updatedDateDateTimePicker";
            this.updatedDateDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.updatedDateDateTimePicker.TabIndex = 16;
            // 
            // tcTitle
            // 
            this.tcTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcTitle.Controls.Add(this.tabEpisodes);
            this.tcTitle.Controls.Add(this.tabGroups);
            this.tcTitle.Location = new System.Drawing.Point(12, 325);
            this.tcTitle.Name = "tcTitle";
            this.tcTitle.SelectedIndex = 0;
            this.tcTitle.Size = new System.Drawing.Size(541, 192);
            this.tcTitle.TabIndex = 17;
            // 
            // tabEpisodes
            // 
            this.tabEpisodes.AutoScroll = true;
            this.tabEpisodes.Controls.Add(this.episodesDataGridView);
            this.tabEpisodes.Location = new System.Drawing.Point(4, 22);
            this.tabEpisodes.Name = "tabEpisodes";
            this.tabEpisodes.Padding = new System.Windows.Forms.Padding(3);
            this.tabEpisodes.Size = new System.Drawing.Size(533, 166);
            this.tabEpisodes.TabIndex = 0;
            this.tabEpisodes.Text = "Episodes";
            this.tabEpisodes.UseVisualStyleBackColor = true;
            // 
            // episodesDataGridView
            // 
            this.episodesDataGridView.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.episodesDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.episodesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.episodesDataGridView.DataSource = this.episodesBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.episodesDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.episodesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.episodesDataGridView.Location = new System.Drawing.Point(3, 3);
            this.episodesDataGridView.Name = "episodesDataGridView";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.episodesDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.episodesDataGridView.Size = new System.Drawing.Size(527, 160);
            this.episodesDataGridView.TabIndex = 0;
            // 
            // episodesBindingSource
            // 
            this.episodesBindingSource.DataMember = "Episodes";
            this.episodesBindingSource.DataSource = this.titleBOBindingSource;
            // 
            // tabGroups
            // 
            this.tabGroups.AutoScroll = true;
            this.tabGroups.Controls.Add(this.groupsDataGridView);
            this.tabGroups.Location = new System.Drawing.Point(4, 22);
            this.tabGroups.Name = "tabGroups";
            this.tabGroups.Padding = new System.Windows.Forms.Padding(3);
            this.tabGroups.Size = new System.Drawing.Size(533, 143);
            this.tabGroups.TabIndex = 1;
            this.tabGroups.Text = "Groups";
            this.tabGroups.UseVisualStyleBackColor = true;
            // 
            // groupsDataGridView
            // 
            this.groupsDataGridView.AutoGenerateColumns = false;
            this.groupsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.groupsDataGridView.DataSource = this.groupsBindingSource;
            this.groupsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupsDataGridView.Location = new System.Drawing.Point(3, 3);
            this.groupsDataGridView.Name = "groupsDataGridView";
            this.groupsDataGridView.Size = new System.Drawing.Size(519, 137);
            this.groupsDataGridView.TabIndex = 0;
            // 
            // groupsBindingSource
            // 
            this.groupsBindingSource.DataMember = "Groups";
            this.groupsBindingSource.DataSource = this.titleBOBindingSource;
            // 
            // coverSmallPictureBox
            // 
            this.coverSmallPictureBox.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.titleBOBindingSource, "CoverSmall", true));
            this.coverSmallPictureBox.Location = new System.Drawing.Point(12, 12);
            this.coverSmallPictureBox.Name = "coverSmallPictureBox";
            this.coverSmallPictureBox.Size = new System.Drawing.Size(239, 307);
            this.coverSmallPictureBox.TabIndex = 18;
            this.coverSmallPictureBox.TabStop = false;
            // 
            // frmTitleView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 537);
            this.Controls.Add(this.coverSmallPictureBox);
            this.Controls.Add(this.tcTitle);
            this.Controls.Add(idLabel);
            this.Controls.Add(this.idTextBox);
            this.Controls.Add(nameLabel);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(plotLabel);
            this.Controls.Add(this.plotTextBox);
            this.Controls.Add(shortNameLabel);
            this.Controls.Add(this.shortNameTextBox);
            this.Controls.Add(updatedByLabel);
            this.Controls.Add(this.updatedByTextBox);
            this.Controls.Add(updatedDateLabel);
            this.Controls.Add(this.updatedDateDateTimePicker);
            this.Name = "frmTitleView";
            this.Text = "TitleView";
            ((System.ComponentModel.ISupportInitialize)(this.titleBOBindingSource)).EndInit();
            this.tcTitle.ResumeLayout(false);
            this.tabEpisodes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.episodesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.episodesBindingSource)).EndInit();
            this.tabGroups.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coverSmallPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource titleBOBindingSource;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox plotTextBox;
        private System.Windows.Forms.TextBox shortNameTextBox;
        private System.Windows.Forms.TextBox updatedByTextBox;
        private System.Windows.Forms.DateTimePicker updatedDateDateTimePicker;
        private System.Windows.Forms.TabControl tcTitle;
        private System.Windows.Forms.TabPage tabEpisodes;
        private System.Windows.Forms.TabPage tabGroups;
        private System.Windows.Forms.DataGridView episodesDataGridView;
        private System.Windows.Forms.BindingSource episodesBindingSource;
        private System.Windows.Forms.DataGridView groupsDataGridView;
        private System.Windows.Forms.BindingSource groupsBindingSource;
        private System.Windows.Forms.PictureBox coverSmallPictureBox;
    }
}