﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using J3ko.Moviebase.WFA.Controllers;

namespace J3ko.Moviebase.WFA
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MoviebaseMain());
        }
    }
}
