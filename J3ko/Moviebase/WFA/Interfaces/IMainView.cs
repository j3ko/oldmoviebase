﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.WFA.ViewModels;

namespace J3ko.Moviebase.WFA
{
    public interface IMainView
    {
        void OpenTitleView(TitleViewModel vm);
    }
}
