﻿using System;
using System.Collections.Generic;
using J3ko.Moviebase.BOL;

namespace J3ko.Moviebase.BLL
{
    public interface ICodeService : IDisposable
    {
        IEnumerable<CategoryBO> GetCategories();
        IEnumerable<ContainerBO> GetContainers();
        IEnumerable<GenreBO> GetGenres();
        IEnumerable<InstanceBO> GetInstances();
        IEnumerable<MediaBO> GetMedia();
        IEnumerable<SlotBO> GetSlots();
        IEnumerable<SlotBO> GetAvailableSlots(int containerId = 0, int include = 0);
    }
}
