﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using System.IO;
using J3ko.Common.Docs;
using J3ko.Web.Movies;

namespace J3ko.Moviebase.BLL
{
    public interface ICatalogService : IDisposable
    {
        TitleBO GetTitle(int id);
        IEnumerable<TitleBO> GetTitles(string term = null, SortOrder sort = SortOrder.TitleNameAlphaAsc, IEnumerable<IFilterableBO> filters = null);

        TitleBO SyncTitleData(int id);
        TitleBO SyncTitleData(TitleBO title, MovieBO movie, string posterPath = null, string smallPosterPath = null);
        TitleBO SaveTitle(TitleBO title, string posterPath = null, string smallPosterPath = null);

        bool DeleteTitle(int titleId);
    }
}
