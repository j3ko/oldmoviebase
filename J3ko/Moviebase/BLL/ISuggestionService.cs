﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;

namespace J3ko.Moviebase.BLL
{
    public interface ISuggestionService : IDisposable
    {
        List<TitleBO> GetSimilar(TitleBO title);
        void AsyncRefreshSuggestions(int suggestCount);
        void RefreshSuggestions(int suggestCount);
    }
}
