﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Web.Movies;

namespace J3ko.Moviebase.BLL
{
    public interface IMatchService
    {
        IEnumerable<MovieBO> Search(string term);
    }
}
