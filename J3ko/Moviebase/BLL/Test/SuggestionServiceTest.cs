﻿using J3ko.Moviebase.BLL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using J3ko.Moviebase.BOL;
using System.Collections.Generic;

namespace J3ko.Moviebase.BLL.Test
{
    
    
    /// <summary>
    ///This is a test class for SuggestionServiceTest and is intended
    ///to contain all SuggestionServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SuggestionServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AsyncRefreshSuggestions
        ///</summary>
        [TestMethod()]
        public void AsyncRefreshSuggestionsTest()
        {
            SuggestionService target = SuggestionService.Instance;
            int suggestCount = 6;
            target.AsyncRefreshSuggestions(suggestCount);
            Assert.IsTrue(true);
        }

        /// <summary>
        ///A test for GetSimilar
        ///</summary>
        [TestMethod()]
        public void GetSimilarTest()
        {
            SuggestionService target = SuggestionService.Instance; // TODO: Initialize to an appropriate value
            TitleBO title = null; // TODO: Initialize to an appropriate value
            List<TitleBO> expected = null; // TODO: Initialize to an appropriate value
            List<TitleBO> actual;
            actual = target.GetSimilar(title);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for RefreshSuggestions
        ///</summary>
        [TestMethod()]
        public void RefreshSuggestionsTest()
        {
            SuggestionService target = SuggestionService.Instance;
            int suggestCount = 6;
            target.RefreshSuggestions(suggestCount);
            Assert.IsTrue(true);
        }
    }
}
