﻿using System.Collections.Generic;
using J3ko.Moviebase.BOL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace J3ko.Moviebase.BLL.Test
{
    
    
    /// <summary>
    ///This is a test class for CatalogServiceTest and is intended
    ///to contain all CatalogServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CatalogServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetTitles
        ///</summary>
        [TestMethod()]
        public void GetTitlesTest()
        {
            CatalogService target = new CatalogService(); // TODO: Initialize to an appropriate value
            IEnumerable<TitleBO> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<TitleBO> actual;
            actual = target.GetTitles();
            Assert.AreNotEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetTitle
        ///</summary>
        [TestMethod()]
        public void GetTitleTest()
        {
            CatalogService target = new CatalogService(); // TODO: Initialize to an appropriate value
            int titleid = 1; // TODO: Initialize to an appropriate value
            TitleBO expected = null; // TODO: Initialize to an appropriate value
            TitleBO actual;
            actual = target.GetTitle(titleid);
            Assert.AreNotEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetTitle
        ///</summary>
        [TestMethod()]
        public void AppendTitleDataTest()
        {
            CatalogService target = new CatalogService();
            int titleid = 89;
            TitleBO expected = null;
            TitleBO actual;
            actual = target.SyncTitleData(titleid);
            Assert.AreNotEqual(expected, actual);
        }
    }
}
