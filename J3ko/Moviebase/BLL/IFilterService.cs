﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;

namespace J3ko.Moviebase.BLL
{
    public interface IFilterService : IDisposable
    {
        IEnumerable<TitleSortBO> GetSortOrders();
        IEnumerable<IFilterableBO> GetAvailableGenres();
        IEnumerable<IFilterableBO> GetAvailableReleasedYears();
    }
}
