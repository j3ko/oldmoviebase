﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;

namespace J3ko.Moviebase.BLL.Engines
{
    internal static class SortEngine
    {
        private static readonly List<TitleSortBO> _sortOrder;

        static SortEngine()
        {
            _sortOrder = new List<TitleSortBO>()
            {
                new TitleSortBO() { Sort = SortOrder.TitleNameAlphaAsc, Description = "Title" },
                new TitleSortBO() { Sort = SortOrder.ReleasedDateDesc, Description = "Release" },
                //new TitleSortBO() { Sort = SortOrder.AddedDateAsc, Description = "Added Date" },
            };
        }

        public static IEnumerable<TitleBO> WithSortOrder(this IEnumerable<TitleBO> titles, SortOrder order)
        {
            switch (order)
            {
                case SortOrder.TitleNameAlphaDesc:
                    return titles.AsQueryable().OrderByDescending(x => x.Name);
                case SortOrder.ReleasedDateAsc:
                    return titles.AsQueryable().OrderBy(x => x.Released);
                case SortOrder.TitleNameAlphaAsc:
                    return titles.AsQueryable().OrderBy(x => x.Name);
                case SortOrder.ReleasedDateDesc:
                default:
                    return titles.AsQueryable().OrderByDescending(x => x.Released);
            }
        }

        public static IEnumerable<TitleSortBO> SortOrders
        {
            get
            {
                return _sortOrder;
            }
        }

    }
}
