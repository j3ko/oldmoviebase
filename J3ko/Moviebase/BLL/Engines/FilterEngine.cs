﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using J3ko.Moviebase.BOL;
using J3ko.Common.Linq;
using J3ko.Foundation.Repository;

namespace J3ko.Moviebase.BLL.Engines
{
    internal static class FilterEngine
    {
        public const int MAX_FILTER_YEAR = 2000;

        public static IEnumerable<TitleBO> WithFilters(this IEnumerable<TitleBO> titles, IEnumerable<IFilterableBO> filters)
        {
            Expression<Func<TitleBO, bool>> lambda = ConstructFilterLambda(filters);
            return titles.AsQueryable().Where(lambda);
        }

        private static Expression<Func<TitleBO, bool>> ConstructFilterLambda(IEnumerable<IFilterableBO> filters)
        {
            Expression<Func<TitleBO, bool>> lambda = PredicateExt.True<TitleBO>();
            lambda = FilterGenres(lambda, filters);
            lambda = FilterReleasedYears(lambda, filters);
            return lambda;
        }

        private static Expression<Func<TitleBO, bool>> FilterGenres(Expression<Func<TitleBO, bool>> lambda, IEnumerable<IFilterableBO> filters)
        {
            if (filters == null || filters.OfType<GenreBO>().Count() <= 0)
                return lambda;

            IEnumerable<GenreBO> temp = filters.OfType<GenreBO>();
            int[] genres = (from i in temp
                            select i.Id).ToArray();

            J3ko.Moviebase.DAL.Repositories.LinqTitleGenreRepository trepo = new J3ko.Moviebase.DAL.Repositories.LinqTitleGenreRepository();
            int[] titleIds = new int[0];

            using (IWorkUnit work = trepo.CreateWorkUnit())
            {
                titleIds = (from i in trepo.GetAll(work)
                            where genres.Contains(i.GenreId)
                            group i by i.TitleId into g
                            where g.Count() >= genres.Length
                            select g.Key).ToArray();
            }

            return lambda.AndAlso<TitleBO>(x => titleIds.Contains(x.Id));
        }

        private static Expression<Func<TitleBO, bool>> FilterReleasedYears(Expression<Func<TitleBO, bool>> lambda, IEnumerable<IFilterableBO> filters)
        {
            if (filters == null || filters.OfType<ReleasedYearFilterBO>().Count() <= 0)
                return lambda;

            IEnumerable<ReleasedYearFilterBO> temp = filters.OfType<ReleasedYearFilterBO>();
            int[] yrs = (from i in temp
                         select i.Year).ToArray();

            if (yrs.Any(x => x < MAX_FILTER_YEAR))
                return lambda.AndAlso<TitleBO>(x => x.Released.Year < MAX_FILTER_YEAR || yrs.Contains(x.Released.Year));
            return lambda.AndAlso<TitleBO>(x => yrs.Contains(x.Released.Year));
        }
    }
}
