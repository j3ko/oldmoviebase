﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Common.Linq;
using System.Linq.Expressions;
using J3ko.Foundation.Repository;

namespace J3ko.Moviebase.BLL.Engines
{
    internal static class SearchEngine
    {
        public static IEnumerable<TitleBO> WithSearchTerm(this IEnumerable<TitleBO> titles, string term)
        {
            if (string.IsNullOrWhiteSpace(term))
                return titles;

            term = term.ToLower();


            int[] titleIds = new int[0];
            
            J3ko.Moviebase.DAL.Repositories.LinqAltTitleNameRepository trepo = new J3ko.Moviebase.DAL.Repositories.LinqAltTitleNameRepository();
            using (IWorkUnit work = trepo.CreateWorkUnit())
            {
                titleIds = (from i in trepo.GetAll(work)
                            where i.Name.ToLower().Contains(term)
                            select i.TitleId).ToArray();
            }

            return from t in titles.AsQueryable()
                   where titleIds.Contains(t.Id)
                   || t.Name.ToLower().Contains(term)
                   select t;
        }

    }
}
