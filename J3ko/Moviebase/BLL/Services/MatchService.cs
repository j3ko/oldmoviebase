﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Web.Movies;

namespace J3ko.Moviebase.BLL
{
    public class MatchService : IMatchService
    {
        private IMovieService _mserve;

        public MatchService()
            : this(new MovieService(MovieSource.IMDb, Properties.Settings.Default.TheMovieDBApiKey))
        { }

        public MatchService(IMovieService mserve)
        {
            _mserve = mserve;
        }

        #region IMatchService Members

        public IEnumerable<MovieBO> Search(string term)
        {
            return _mserve.Search(term);
        }

        #endregion
    }
}
