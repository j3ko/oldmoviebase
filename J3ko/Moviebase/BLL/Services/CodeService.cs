﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Repositories;
using J3ko.Moviebase.DAL;
using J3ko.Common.Utils;

namespace J3ko.Moviebase.BLL
{
    public class CodeService : ICodeService
    {
        private MoviebaseRepository<GenreBO> _grepo;
        private MoviebaseRepository<CategoryBO> _catrepo;
        private MoviebaseRepository<InstanceBO> _irepo;
        private MoviebaseRepository<SlotBO> _srepo;
        private MoviebaseRepository<ContainerBO> _conrepo;
        private MoviebaseRepository<MediaBO> _mrepo;

        private bool _isDisposed = false;

        public CodeService()
            : this(new LinqGenreRepository(), 
            new LinqCategoryRepository(), 
            new LinqInstanceRepository(),
            new LinqSlotRepository(),
            new LinqContainerRepository(),
            new LinqMediaRepository())
        { }

        public CodeService(MoviebaseRepository<GenreBO> grepo, 
            MoviebaseRepository<CategoryBO> catrepo, 
            MoviebaseRepository<InstanceBO> irepo,
            MoviebaseRepository<SlotBO> srepo,
            MoviebaseRepository<ContainerBO> conrepo, 
            MoviebaseRepository<MediaBO> mrepo)
        {
            _grepo = grepo;
            _catrepo = catrepo;
            _irepo = irepo;
            _srepo = srepo;
            _conrepo = conrepo;
            _mrepo = mrepo;
        }

        #region ICodeService Members

        public IEnumerable<GenreBO> GetGenres()
        {
            return _grepo.GetAll().ToList();
        }

        public IEnumerable<CategoryBO> GetCategories()
        {
            return _catrepo.GetAll().ToList();
        }

        public IEnumerable<InstanceBO> GetInstances()
        {
            return (from i in _irepo.GetAll()
                    select i).ToList();
        }

        public IEnumerable<SlotBO> GetSlots()
        {
            return (from i in _srepo.GetAll()
                    select i);
        }

        public IEnumerable<ContainerBO> GetContainers()
        {
            return (from i in _conrepo.GetAll()
                    select i).ToList();
        }

        public IEnumerable<MediaBO> GetMedia()
        {
            return (from i in _mrepo.GetAll()
                    select i).ToList();
        }

        public IEnumerable<SlotBO> GetAvailableSlots(int containerId = 0, int include = 0)
        {
            IEnumerable<SlotBO> result = GetSlots().AsQueryable()
                .Where(x => ((x.Container.Id == containerId || containerId == 0) && x.IsAvailable) || x.Id == include);

            return result.OrderBy(x => x.Label, new NaturalSortComparer<string>());
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~CodeService()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // Code to dispose the managed resources of the class
                    if (_grepo != null)
                    {
                        _grepo.Dispose();
                        _grepo = null;
                    }
                    if (_catrepo != null)
                    {
                        _catrepo.Dispose();
                        _catrepo = null;
                    }
                    if (_irepo != null)
                    {
                        _irepo.Dispose();
                        _irepo = null;
                    }
                    if (_srepo != null)
                    {
                        _srepo.Dispose();
                        _srepo = null;
                    }
                    if (_conrepo != null)
                    {
                        _conrepo.Dispose();
                        _conrepo = null;
                    }
                    if (_mrepo != null)
                    {
                        _mrepo.Dispose();
                        _mrepo = null;
                    }
                }
                // Code to dispose the un-managed resources of the class
            }

            _isDisposed = true;
        }

        #endregion
    }
}
