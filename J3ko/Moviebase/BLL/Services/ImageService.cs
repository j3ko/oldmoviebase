﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Common.Docs;
using System.Reflection;
using System.IO;
using J3ko.Diagnostics;

namespace J3ko.Moviebase.BLL
{
    public enum ImageSize
    {
        LargePoster,
        SmallPoster,
        Profile,
    }

    public class ImageService : IImageService
    {
        private bool _isDisposed = false;

        private static readonly Dictionary<ImageSize, string> _posterMap;
        private const string DEFAULT_POSTER_PATH = "J3ko.Moviebase.BLL.Images";

        private DocumentService _dserve;

        static ImageService()
        {
            _posterMap = new Dictionary<ImageSize, string>()
            {
                {ImageSize.LargePoster, "default_poster.jpg"},
                {ImageSize.SmallPoster, "default_poster_small.jpg"},
                {ImageSize.Profile, "default_profile.png"},
            };
        }


        public ImageService()
        {
            _dserve = new DocumentService();
        }

        #region IImageService Members

        public Guid TryUpdateImage(Guid existingCoverId, string coverPath)
        {
            //string filename = Path.GetFileNameWithoutExtension(coverPath);
            //string ext = Path.GetExtension(coverPath);

            DocumentBO doc = null;

            try
            {
                doc = _dserve.GetDocument(existingCoverId);

                if (!string.IsNullOrWhiteSpace(coverPath) && (doc == null || doc.Source != coverPath))
                {
                    doc = DocumentBO.FromFile(coverPath);
                    doc.Source = coverPath;

                    if (!doc.IsEmpty)
                        doc = _dserve.SaveDocument(doc);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, LogSeverity.Warn, LogType.Database);
            }

            return (doc == null) ? Guid.Empty : doc.DocumentId;
        }

        public DocumentBO GetImage(Guid id, ImageSize size)
        {
            DocumentBO result = _dserve.GetDocument(id);

            if (result == null)
            {
                Assembly myAssembly = Assembly.GetExecutingAssembly();
                string path = string.Format("{0}.{1}", DEFAULT_POSTER_PATH, _posterMap[size]);
                Stream myStream = myAssembly.GetManifestResourceStream(path);
                result = DocumentBO.FromStream(myStream, _posterMap[size]);
            }

            return result;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ImageService()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // Code to dispose the managed resources of the class
                    if (_dserve != null)
                    {
                        _dserve.Dispose();
                        _dserve = null;
                    }
                }
                // Code to dispose the un-managed resources of the class
            }
            _isDisposed = true;
        }

        #endregion
    }
}
