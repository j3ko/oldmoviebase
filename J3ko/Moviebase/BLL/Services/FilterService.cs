﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.DAL.Repositories;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.BLL.Engines;

namespace J3ko.Moviebase.BLL
{
    public class FilterService : IFilterService
    {
        private MoviebaseRepository<GenreBO> _grepo;
        private MoviebaseRepository<ReleasedYearFilterBO> _yrepo;

        private bool _isDisposed = false;

        public FilterService()
        {
            _grepo = new LinqGenreRepository();
            _yrepo = new LinqReleasedYearFilterRepository();
        }

        #region IFilterService Members

        public IEnumerable<IFilterableBO> GetAvailableGenres()
        {
            return _grepo.Find(x => x.IsUsed).OrderBy(x => x.Name);
        }

        public IEnumerable<IFilterableBO> GetAvailableReleasedYears()
        {
            List<ReleasedYearFilterBO> result = _yrepo.GetAll()
                                                    .OrderByDescending(x => x.Year)
                                                    .ToList();

            if (result.Any(x => x.Year < FilterEngine.MAX_FILTER_YEAR))
            {
                result = result.Where(x => x.Year >= FilterEngine.MAX_FILTER_YEAR).ToList();
                result.Add(new ReleasedYearFilterBO
                {
                    Value = string.Format(@"Pre-{0}", FilterEngine.MAX_FILTER_YEAR),
                    Year = FilterEngine.MAX_FILTER_YEAR - 1,
                });
            }

            return result;
        }

        public IEnumerable<TitleSortBO> GetSortOrders()
        {
            return SortEngine.SortOrders;
        }

        #endregion


        
        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~FilterService()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // Code to dispose the managed resources of the class
                    if (_grepo != null)
                    {
                        _grepo.Dispose();
                        _grepo = null;
                    }
                    if (_yrepo != null)
                    {
                        _yrepo.Dispose();
                        _yrepo = null;
                    }
                }
                // Code to dispose the un-managed resources of the class
            }
            _isDisposed = true;
        }

        #endregion
    }
}
