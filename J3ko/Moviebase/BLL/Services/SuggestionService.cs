﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Repositories;
using J3ko.Algorithm;
using J3ko.Foundation.Repository;
using J3ko.Diagnostics;

namespace J3ko.Moviebase.BLL
{
    public class SuggestionService : ISuggestionService
    {
        //private MoviebaseRepository<TitleBO> _trepo;
        //private MoviebaseRepository<GenreBO> _grepo;
        //private MoviebaseRepository<TitleGenreMapBO> _tgrepo;
        //private MoviebaseRepository<TitlePersonBO> _tprepo;
        //private MoviebaseRepository<TitleSuggestionMapBO> _tsrepo;
        //private MoviebaseRepository<TitleComparisonWeightsBO> _tcwrepo;

        private static volatile bool _isRefreshing = false;
        private static volatile SuggestionService _instance;
        private static object syncRoot = new Object();
        private bool _isDisposed = false;

        private Dictionary<int, double[]> _comparisonw;
        private List<int> _titleIds;

        public const double SIMILARITY_THRESHOLD = 0d;

        private SuggestionService()
        {
            //_trepo = new LinqTitleRepository();
            //_grepo = new LinqGenreRepository();
            //_tgrepo = new LinqTitleGenreRepository();
            //_tprepo = new LinqTitlePersonRepository();
            //_tsrepo = new LinqTitleSuggestionRepository();
            //_tcwrepo = new LinqTitleComparisonWeightsRepository();
        }

        public static SuggestionService Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (_instance == null)
                            _instance = new SuggestionService();
                    }
                }

                return _instance;
            }
        }

        private void InitializeComparisonWeights()
        {
            using (MoviebaseRepository<TitleComparisonWeightsBO> tcwrepo = new LinqTitleComparisonWeightsRepository())
            {
                IEnumerable<TitleComparisonWeightsBO> temp = tcwrepo.GetAll().ToList();

                _titleIds = new List<int>();
                _comparisonw = new Dictionary<int, double[]>();

                foreach (TitleComparisonWeightsBO t in temp)
                {
                    if (t.Weights != null)
                    {
                        _titleIds.Add(t.TitleId);
                        _comparisonw.Add(t.TitleId, Array.ConvertAll<string, double>(t.Weights.ToArray(), x => double.Parse(x)));
                    }
                }
            }
        }

        #region ISuggestionService Members

        public List<TitleBO> GetSimilar(TitleBO title)
        {
            List<TitleBO> result = new List<TitleBO>();

            using (MoviebaseRepository<TitleSuggestionMapBO> tsrepo = new LinqTitleSuggestionRepository())
            using (MoviebaseRepository<TitleBO> trepo = new LinqTitleRepository())
            {
                if (title == null)
                    return result;

                int[] temp = tsrepo.Find(x => x.TitleId == title.Id)
                                    .OrderByDescending(x => x.Distance)
                                    .Select(x => x.SuggestionId)
                                    .ToArray();

                List<TitleBO> tempTitles = trepo.Find(x => temp.Contains(x.Id)).ToList();

                result = (from i in temp
                          join j in tempTitles on i equals j.Id
                          select j).ToList();
            }

            return result;
        }

        public void AsyncRefreshSuggestions(int suggestCount)
        {
            RefreshSuggestionDelegate del = new RefreshSuggestionDelegate(RefreshSuggestions);
            del.BeginInvoke(suggestCount, null, null);
        }

        delegate void RefreshSuggestionDelegate(int suggestCount);

        public void RefreshSuggestions(int suggestCount)
        {
            if (_isRefreshing)
                return;
            else
                _isRefreshing = true;

            int titleCount = 0;
            IWorkUnit work = null;

            try
            {
                Logger.Log(string.Format("Suggestion refresh initiated; suggestCount:{0}", suggestCount), LogSeverity.Info, LogType.Database);
                InitializeComparisonWeights();
                Logger.Log("Calculating suggestions", LogSeverity.Info, LogType.Database);

                using (MoviebaseRepository<TitleSuggestionMapBO> tsrepo = new LinqTitleSuggestionRepository())
                {
                    // get list of title ids
                    int[] titleIds = _titleIds.ToArray();
                    titleCount = titleIds.Length;
                    TitleSuggestionMapBO[] tempSugs = null;
                    double[] tempa = null;
                    double[] tempb = null;
                    double distance = 0;
                    PriorityQueue<double, int> priority = null;

                    TitleSuggestionMapBO temp = null;

                    // iterate through
                    for (int i = 0; i < titleIds.Count(); i++)
                    {
                        priority = new PriorityQueue<double, int>();
                        work = null;

                        for (int j = 0; j < titleIds.Count(); j++)
                        {
                            if (titleIds[i] == titleIds[j])
                                continue;

                            // get comparison vectors
                            tempa = GetComparisonVector(titleIds[i]);
                            tempb = GetComparisonVector(titleIds[j]);

                            NormalizeVectors(ref tempa, ref tempb);

                            distance = CosineDistance.Calculate(tempa.ToArray(), tempb.ToArray());

                            if (distance >= SIMILARITY_THRESHOLD)
                            {
                                if (priority.Count >= suggestCount && priority.Peek().Key < distance)
                                {
                                    priority.Dequeue();
                                    priority.Enqueue(distance, titleIds[j]);
                                }
                                else if (priority.Count < suggestCount)
                                {
                                    priority.Enqueue(distance, titleIds[j]);
                                }
                            }
                        }

                        tempSugs = (from t in tsrepo.GetAll()
                                    where t.TitleId == titleIds[i]
                                    select t).ToArray() ?? new TitleSuggestionMapBO[0];

                        work = tsrepo.CreateWorkUnit();

                        foreach (TitleSuggestionMapBO tsbo in tempSugs)
                        {
                            tsrepo.Delete(tsbo, work);
                        }

                        foreach (KeyValuePair<double, int> kvp in priority)
                        {
                            // save value
                            temp = new TitleSuggestionMapBO() { TitleId = titleIds[i], SuggestionId = kvp.Value, Distance = kvp.Key };
                            tsrepo.Save(temp, work);
                        }

                        work.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                if (work != null)
                    work.Rollback();

                Logger.Log(ex);
            }
            finally
            {
                _isRefreshing = false;
                Logger.Log(string.Format("Suggestion refresh finished; titleCount:{0}", titleCount), LogSeverity.Info, LogType.Database);
            }
        }

        #endregion

        private void NormalizeVectors(ref double[] a, ref double[] b)
        {
            List<double> tempa = new List<double>();
            List<double> tempb = new List<double>();

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != 0 || b[i] != 0)
                {
                    tempa.Add(a[i]);
                    tempb.Add(b[i]);
                }
            }

            a = tempa.ToArray();
            b = tempb.ToArray();
        }

        private double[] GetComparisonVector(int titleId)
        {
            return _comparisonw[titleId];
        }

        private IEnumerable<int> GetTitleIds()
        {
            int[] result = new int[0];

            using (MoviebaseRepository<TitleBO> trepo = new LinqTitleRepository())
            {
                result = (from a in trepo.GetAll()
                          select a.Id).ToArray();
            }

            return result;
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~SuggestionService()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // Code to dispose the managed resources of the class
                    //if (_trepo != null)
                    //{
                    //    _trepo.Dispose();
                    //    _trepo = null;
                    //}
                    //if (_grepo != null)
                    //{
                    //    _grepo.Dispose();
                    //    _grepo = null;
                    //}
                    //if (_tgrepo != null)
                    //{
                    //    _tgrepo.Dispose();
                    //    _tgrepo = null;
                    //}
                    //if (_tprepo != null)
                    //{
                    //    _tprepo.Dispose();
                    //    _tprepo = null;
                    //}
                    //if (_tsrepo != null)
                    //{
                    //    _tsrepo.Dispose();
                    //    _tsrepo = null;
                    //}
                    //if (_tcwrepo != null)
                    //{
                    //    _tcwrepo.Dispose();
                    //    _tcwrepo = null;
                    //}
                }
                // Code to dispose the un-managed resources of the class
            }
            _isDisposed = true;
        }

        #endregion
    }
}
