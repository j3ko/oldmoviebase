﻿using System;
using System.Collections.Generic;
using System.Linq;
using J3ko.Diagnostics;
using J3ko.Moviebase.BLL.Engines;
using J3ko.Moviebase.BOL;
using J3ko.Moviebase.DAL.Repositories;
using J3ko.Web.Movies;

namespace J3ko.Moviebase.BLL
{
    public class CatalogService : ICatalogService
    {
        private MoviebaseRepository<TitleBO> _trepo;
        private MoviebaseRepository<TitlePersonBO> _tprepo;
        private MoviebaseRepository<PersonBO> _prepo;

        private IImageService _iserve;
        private ICodeService _cserve;
        private IMovieService _mserve;
        private ISuggestionService _sugserve;

        private List<GenreBO> _genres = null;
        private bool _isDisposed = false;

        private const int SUGGESTION_COUNT = 6;

        public CatalogService()
        {
            _trepo = new LinqTitleRepository();
            _tprepo = new LinqTitlePersonRepository();
            _prepo = new LinqPersonRepository();

            _cserve = new CodeService();
            _iserve = new ImageService();
            _mserve = new MovieService(MovieSource.IMDb, Properties.Settings.Default.TheMovieDBApiKey);
            _sugserve = SuggestionService.Instance;
        }

        #region ICatalogService Members

        public IEnumerable<TitleBO> GetTitles(string term = null, SortOrder sort = SortOrder.TitleNameAlphaAsc, IEnumerable<IFilterableBO> filters = null)
        {
            IEnumerable<TitleBO> titles = _trepo
                .GetAll()
                .WithFilters(filters)
                .WithSearchTerm(term)
                .WithSortOrder(sort);

            return titles;
        }

        public TitleBO GetTitle(int titleid)
        {
            return _trepo.FindSingle(x => x.Id == titleid) ?? new TitleBO();
        }

        public TitleBO SaveTitle(TitleBO title, string posterPath = null, string smallPosterPath = null)
        {
            TitleBO result = GetTitle(title.Id);

            try
            {
                if (title.CoverId == Guid.Empty)
                    title.CoverId = result.CoverId;
                if (!string.IsNullOrWhiteSpace(posterPath))
                    title.CoverId = _iserve.TryUpdateImage(title.CoverId, posterPath);

                if (title.CoverSmallId == Guid.Empty)
                    title.CoverSmallId = result.CoverSmallId;
                if (!string.IsNullOrWhiteSpace(smallPosterPath))
                    title.CoverSmallId = _iserve.TryUpdateImage(title.CoverSmallId, smallPosterPath);

                _trepo.Save(title);

                _sugserve.AsyncRefreshSuggestions(SUGGESTION_COUNT);

                return title;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                Logger.Log(ex);

                if (ex.Message.Contains("IX_title_group"))
                    throw new ApplicationException("Cannot add more than one disc to a slot", ex);
                else if (ex.Message.Contains("IX_title__imdb_id"))
                    throw new ApplicationException("Cannot add duplicate titles", ex);
                else
                    throw new ApplicationException("Error saving title", ex);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw new ApplicationException("Error saving title", ex);
            }
        }

        public TitleBO SyncTitleData(int id)
        {
            TitleBO result = null;

            try
            {
                result = _trepo.FindSingle(x => x.Id == id);

                if (result != null && !string.IsNullOrWhiteSpace(result.ImdbId))
                {
                    MovieBO mov = _mserve.GetMovie(result.ImdbId);
                    result = SyncTitleData(result, mov);
                }
            }
            catch (ApplicationException apex)
            {
                throw apex;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw new ApplicationException("Error syncing title", ex);
            }

            return result ?? new TitleBO();
        }

        public TitleBO SyncTitleData(TitleBO title, MovieBO movie, string posterPath = null, string smallPosterPath = null)
        {
            try
            {
                title.Synced = DateTime.Now;
                //title.Name = movie.Title;
                //title.Plot = movie.Plot;
                title.Released = movie.Released;

                AppendGenres(title, movie);
                AppendAltNames(title, movie);
                AppendTitlePeople(title, movie);

                SaveTitle(title, posterPath, smallPosterPath);
            }
            catch (ApplicationException apex)
            {
                throw apex;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw new ApplicationException("Error syncing title", ex);
            }
            return title;
        }

        public bool DeleteTitle(int titleId)
        {
            TitleBO temp = _trepo.FindSingle(x => x.Id == titleId);
            _trepo.Delete(temp);
            return true;
        }        
        
        #endregion

        #region Private Helpers
        
        private void AppendTitlePeople(TitleBO title, MovieBO movie)
        {
            List<TitlePersonBO> people = new List<TitlePersonBO>();
            PersonBO temp = null;
            TitlePersonBO person = null;

            foreach (MoviePersonBO mp in movie.People)
            {
                temp = null;
                if (mp.TmdbId > 0)
                    temp = _prepo.Find(x => x.TmdbId == mp.TmdbId).FirstOrDefault();
                temp = temp ?? new PersonBO();

                temp.FullName = mp.Name;
                temp.ThumbnailId = _iserve.TryUpdateImage(temp.ThumbnailId, mp.ThumbnailUrl);
                temp.TmdbId = mp.TmdbId.GetValueOrDefault();

                _prepo.Save(temp);

                person = new TitlePersonBO(temp);
                person.CharacterName = mp.CharacterName;
                person.Ordinal = mp.Ordinal;
                person.Role = MapTitleRole(mp.Role);

                people.Add(person);
            }

            title.TitlePeople = people;
        }

        private TitleRole MapTitleRole(MoviePersonRole role)
        {
            TitleRole result = TitleRole.Other;

            switch(role)
            {
                case MoviePersonRole.Other:
                    result = TitleRole.Other;
                    break;
                case MoviePersonRole.Actor:
                    result = TitleRole.Actor;
                    break;
                case MoviePersonRole.Director:
                    result = TitleRole.Director;
                    break;
                case MoviePersonRole.Producer:
                    result = TitleRole.Producer;
                    break;
            }

            return result;
        }

        private void AppendGenres(TitleBO title, MovieBO movie)
        {
            List<GenreBO> genres = new List<GenreBO>();
            GenreBO temp = null;

            foreach (MovieGenreBO g in movie.Genres)
            {
                temp = GetGenres().Where(x => x.TmdbIds.Contains(g.Id)).FirstOrDefault();
                if (temp != null)
                    genres.Add(temp);
            }

            title.Genres = genres.Distinct().ToList();
        }

        private void AppendAltNames(TitleBO title, MovieBO movie)
        {
            List<AltTitleNameBO> names = new List<AltTitleNameBO>();
            AltTitleNameBO temp = null;

            foreach (string n in movie.AltTitles)
            {
                if (!string.IsNullOrWhiteSpace(n))
                {
                    temp = new AltTitleNameBO();
                    temp.Name = n;
                    names.Add(temp);
                }
            }

            title.AltNames = names.Distinct().ToList();
        }

        private IEnumerable<GenreBO> GetGenres()
        {
            if (_genres == null)
                _genres = _cserve.GetGenres().ToList();
            return _genres;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~CatalogService()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // Code to dispose the managed resources of the class
                    if (_trepo != null)
                    {
                        _trepo.Dispose();
                        _trepo = null;
                    }
                    if (_tprepo != null)
                    {
                        _tprepo.Dispose();
                        _tprepo = null;
                    }
                    if (_prepo != null)
                    {
                        _prepo.Dispose();
                        _prepo = null;
                    }
                    if (_cserve != null)
                    {
                        _cserve.Dispose();
                        _cserve = null;
                    }
                    if (_iserve != null)
                    {
                        _iserve.Dispose();
                        _iserve = null;
                    }
                    if (_sugserve != null)
                    {
                        _sugserve.Dispose();
                        _sugserve = null;
                    }
                    //if (_mserve != null)
                    //{
                    //    _mserve.Dispose();
                    //    _mserve = null;
                    //}
                }
                // Code to dispose the un-managed resources of the class
            }
            _isDisposed = true;
        }

        #endregion
    }
}
