﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using J3ko.Common.Docs;

namespace J3ko.Moviebase.BLL
{
    public interface IImageService : IDisposable
    {
        Guid TryUpdateImage(Guid existingCoverId, string coverPath);
        DocumentBO GetImage(Guid id, ImageSize size);
    }
}
