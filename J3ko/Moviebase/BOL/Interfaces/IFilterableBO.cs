﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Moviebase.BOL
{
    public interface IFilterableBO : ICodeBO
    {
        string Identifier { get; }
    }
}
