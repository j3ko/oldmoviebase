﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Moviebase.BOL
{
    public interface ICodeBO
    {
        string Key { get; }
        string Value { get; }
    }
}
