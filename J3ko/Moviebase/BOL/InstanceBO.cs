﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;

namespace J3ko.Moviebase.BOL
{
    public class InstanceBO
    {
        public int Id { get; internal set; }
        public string Description { get; set; }

        public static InstanceBO Create(int id)
        {
            return new InstanceBO() { Id = id };
        }

        #region object overrides
        public override string ToString()
        {
            return Description;
        }
        #endregion
    }
}
