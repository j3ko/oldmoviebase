﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Moviebase.BOL
{
    public enum TitleRole
    {
        Other = 1,
        Actor = 2,
        Director = 3,
        Producer = 4,
    }
    public class TitlePersonBO : PersonBO
    {
        public TitlePersonBO()
            : this(new PersonBO())
        { }

        public TitlePersonBO(PersonBO person)
        {
            if (person == null)
                throw new ArgumentNullException("person");

            Id = person.Id;
            FullName = person.FullName;
            ThumbnailId = person.ThumbnailId;
            TmdbId = person.TmdbId;
        }

        public int TitleId { get; internal set; }
        public int Ordinal { get; set; }
        public string CharacterName { get; set; }
        public TitleRole Role { get; set; }

        #region object overrides
        
        public override bool Equals(object obj)
        {
            if (obj is TitlePersonBO)
                return Id == ((TitlePersonBO)obj).Id
                    && TitleId == ((TitlePersonBO)obj).TitleId
                    && Ordinal == ((TitlePersonBO)obj).Ordinal;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + Id.GetHashCode();
            hash = hash * 23 + TitleId.GetHashCode();
            hash = hash * 23 + Ordinal.GetHashCode();
            return hash;
        }

        public override string ToString()
        {
            return string.Format("{0} ({1})", FullName, CharacterName);
        }

        #endregion
    }
}
