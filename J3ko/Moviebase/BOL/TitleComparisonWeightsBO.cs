﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Moviebase.BOL
{
    public class TitleComparisonWeightsBO
    {
        public int TitleId { get; set; }
        public IEnumerable<string> Weights { get; set; }
    }
}
