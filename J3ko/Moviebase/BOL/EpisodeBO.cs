﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace J3ko.Moviebase.BOL
{
    public class EpisodeBO
    {
        #region Compound Key
        public InstanceBO Instance
        {
            get 
            {
                if (_instance == null)
                    _instance = new InstanceBO();
                return _instance;
            }
            set 
            { 
                _instance = value; 
            }
        }
        public int TitleId { get; internal set; }
        //public TitleBO Title
        //{
        //    get
        //    {
        //        if (_title == null)
        //            _title = new TitleBO();
        //        return _title;
        //    }
        //    internal set
        //    {
        //        _title = value;
        //    }
        //}
        #endregion

        public int GroupId { get; internal set; }
        //public GroupBO Group
        //{
        //    get
        //    {
        //        if (_group == null)
        //            _group = new GroupBO();
        //        return _group;
        //    }
        //    internal set
        //    {
        //        _group = value;
        //    }
        //}

        public string Name 
        {
            get
            {
                if (_name == null)
                    _name =  "Default";
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual bool IsGrouped
        {
            get
            {
                return GroupId != int.MinValue;
            }
        }

        #region Private Members
        private string _name;
        private InstanceBO _instance;
        //private TitleBO _title;
        //private GroupBO _group;
        #endregion

        #region Object Overrides
        public override bool Equals(object obj)
        {
            if (obj is EpisodeBO)
                return Instance.Id == ((EpisodeBO)obj).Instance.Id && TitleId == ((EpisodeBO)obj).TitleId;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + Instance.Id.GetHashCode();
            hash = hash * 23 + TitleId.GetHashCode();
            return hash;
        }
        #endregion
    }
}
