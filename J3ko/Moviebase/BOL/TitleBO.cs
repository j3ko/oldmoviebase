﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using J3ko.Common.Utils;
using System.Runtime.CompilerServices;

namespace J3ko.Moviebase.BOL
{
    public class TitleBO : IList<GroupBO>
    {
        #region Private Members
        private CategoryBO _category;
        private IList<GenreBO> _genres;
        private IList<GroupBO> _groups;
        private IList<AltTitleNameBO> _altNames;
        private IList<TitlePersonBO> _titlePeople;
        private DefaultGroupBO _defaultGroup;

        private void BindGroup(GroupBO item)
        {
            item.TitleId = this.Id;
            foreach (EpisodeBO e in item)
            {
                e.TitleId = this.Id;
            }
        }
        #endregion

        /// <summary>
        /// Primary Key
        /// </summary>
        public int Id { get; internal set; }
        public string Name { get; set; }
        public IList<AltTitleNameBO> AltNames
        {
            get
            {
                if (_altNames == null)
                    _altNames = new List<AltTitleNameBO>();
                return _altNames;
            }
            set
            {
                _altNames = value;
            }
        }
        internal DefaultGroupBO DefaultGroup
        {
            get
            {
                if (_defaultGroup == null)
                    _defaultGroup = new DefaultGroupBO(Id);
                return _defaultGroup;
            }
        }
        internal IList<EpisodeBO> DefaultGroupEpisodes
        {
            set
            {
                DefaultGroup.Episodes = value;
            }
        }

        internal IList<GroupBO> Groups 
        {
            private get
            {
                if (_groups == null)
                    _groups = new List<GroupBO>();
                return _groups;
            }
            set
            {
                _groups = value;
            }
        }
        public IList<EpisodeBO> Episodes
        {
            get
            {
                List<EpisodeBO> result = new List<EpisodeBO>();

                // add groupless episodes
                result.AddRange(DefaultGroup);
                
                // add grouped episodes
                foreach (GroupBO g in Groups)
                {
                    result.AddRange(g);
                }
                
                return result.ToList().AsReadOnly();
            }
        }
        public CategoryBO Category
        {
            get
            {
                if (_category == null)
                    _category = new CategoryBO();
                return _category;
            }
            set
            {
                _category = value;
            }
        }
        public IList<GenreBO> Genres
        {
            get
            {
                if (_genres == null)
                    _genres = new List<GenreBO>();
                return _genres;
            }
            set
            {
                _genres = value;
            }
        }
        public IList<TitlePersonBO> TitlePeople
        {
            get
            {
                if (_titlePeople == null)
                    _titlePeople = new List<TitlePersonBO>();
                return _titlePeople;
            }
            set
            {
                _titlePeople = value;
            }
        }
        public string Plot { get; set; }
        public Guid CoverId { get; set; }
        public Guid CoverSmallId { get; set; }
        public string ImdbId { get; set; }
        public DateTime Released { get; set; }
        public DateTime? Synced { get; set; }
        public DateTime Modified { get; internal set; }

        public void AddEpisode(EpisodeBO item)
        {
            DefaultGroup.Add(item);
        }

        public void RemoveEpisode(EpisodeBO item)
        {
            DefaultGroup.Remove(item);
        }

        #region IList<GroupBO> Members

        public int IndexOf(GroupBO item)
        {
            return Groups.IndexOf(item);
        }

        public void Insert(int index, GroupBO item)
        {
            BindGroup(item);
            Groups.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            Groups.RemoveAt(index);
        }

        public GroupBO this[int index]
        {
            get
            {
                return Groups[index];
            }
            set
            {
                BindGroup(value);
                Groups[index] = value;
            }
        }

        #endregion

        #region ICollection<GroupBO> Members

        public void Add(GroupBO item)
        {
            BindGroup(item);
            Groups.Add(item);
        }

        public void Clear()
        {
            Groups.Clear();
        }

        public bool Contains(GroupBO item)
        {
            return Groups.Contains(item);
        }

        public void CopyTo(GroupBO[] array, int arrayIndex)
        {
            Groups.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return Groups.Count; }
        }

        public bool IsReadOnly
        {
            get { return Groups.IsReadOnly; }
        }

        public bool Remove(GroupBO item)
        {
            return Groups.Remove(item);
        }

        #endregion

        #region IEnumerable<GroupBO> Members

        public IEnumerator<GroupBO> GetEnumerator()
        {
            return Groups.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Groups.GetEnumerator();
        }

        #endregion
    }
}
