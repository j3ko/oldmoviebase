﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Moviebase.BOL
{
    public class DefaultGroupBO : GroupBO
    {
        public DefaultGroupBO(int titleId)
        {
            Id = int.MinValue;
            TitleId = titleId;
            Description = "Default";            
            Media = MediaBO.Create(1);
            Slot = SlotBO.Create(1);
        }

        internal DefaultGroupBO() : this(0)
        { }
    }
}
