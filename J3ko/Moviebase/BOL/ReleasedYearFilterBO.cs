﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Moviebase.BOL
{
    public class ReleasedYearFilterBO : IFilterableBO
    {
        #region IFilterable Members
        
        public string Identifier
        {
            get { return "fr"; }
        }

        public string Key
        {
            get { return Year.ToString(); }
        }

        private string _value;
        public string Value
        {            
            get 
            { 
                if (_value == null)
                    _value = Year.ToString();
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        #endregion

        public int Year { get; set; }

        #region object overrides
        public override string ToString()
        {
            return Value;
        }
        #endregion

    }
}
