﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;

namespace J3ko.Moviebase.BOL
{
    public class GenreBO : IFilterableBO
    {
        public int Id { get; internal set; }
        public string Name { get; set; }
        public bool IsUsed { get; set; }
        private IList<int> _tmdbids;
        public IList<int> TmdbIds
        {
            get
            {
                if (_tmdbids == null)
                    _tmdbids = new List<int>();
                return _tmdbids;
            }
            set
            {
                _tmdbids = value;
            }
        }

        public static GenreBO Create(int id)
        {
            return new GenreBO() { Id = id };
        }

        #region object overrides
        public override string ToString()
        {
            return Name;
        }
        public override bool Equals(object obj)
        {
            if (obj is GenreBO)
                return ((GenreBO)obj).Id == this.Id;
            return false;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + Id.GetHashCode();
            return hash;
        }
        #endregion

        #region IFilterable Members

        public string Key
        {
            get { return Id.ToString(); }
        }

        public string Value
        {
            get { return Name; }
        }

        public string Identifier
        {
            get { return "fg"; }
        }

        #endregion
    }
}
