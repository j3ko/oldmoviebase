﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;

namespace J3ko.Moviebase.BOL
{
    public class CategoryBO
    {
        public int Id { get; internal set; }
        public string Name { get; set; }

        public static CategoryBO Create(int id)
        {
            return new CategoryBO() { Id = id };
        }

        #region object overrides
        public override string ToString()
        {
            return Name;
        }
        #endregion
    }
}
