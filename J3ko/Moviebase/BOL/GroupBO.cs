﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using J3ko.Common.Utils;
using System.Runtime.CompilerServices;

// To enforce proper use of the domain model
namespace J3ko.Moviebase.BOL
{
    public class GroupBO : IList<EpisodeBO>
    {
        public int Id { get; internal set; }
        public string Description 
        {
            get
            {
                if (_description == null)
                    _description = "Default";
                return _description;
            }
            set
            {
                _description = value;
            }
        }
        public DateTime DateAdded { get; set; }
        public DateTime DateBurned { get; set; }
        internal IList<EpisodeBO> Episodes 
        {
            private get
            {
                if (_episodes == null)
                    _episodes = new List<EpisodeBO>();
                return _episodes;
            }
            set
            {
                _episodes = value;
            }
        }
        public MediaBO Media 
        {
            get
            {
                if (_media == null)
                    _media = new MediaBO();
                return _media;
            }
            set
            {
                _media = value;
            }
        }
        public SlotBO Slot 
        {
            get
            {
                if (_slot == null)
                    _slot = new SlotBO();
                return _slot;
            }
            set
            {
                _slot = value;
            }
        }

        public int TitleId { get; internal set; }
        //public TitleBO Title
        //{
        //    get
        //    {
        //        if (_title == null)
        //            _title =  new TitleBO();
        //        return _title;
        //    }
        //    internal set
        //    {
        //        _title = value;
        //    }
        //}

        #region Private Members
        private IList<EpisodeBO> _episodes;
        private MediaBO _media;
        private SlotBO _slot;
        //private ContainerBO _container;
        //private TitleBO _title;
        private string _description;

        private void BindEpisode(EpisodeBO item)
        {
            item.TitleId = TitleId;
            //item.Group = this;
            item.GroupId = Id;
        }
        #endregion

        #region Object Overrides
        public override bool Equals(object obj)
        {
            if (obj is GroupBO)
                return Id == ((GroupBO)obj).Id;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + Id.GetHashCode();
            return hash;
        }
        #endregion

        #region IList<EpisodeBO> Members

        public int IndexOf(EpisodeBO item)
        {
            return Episodes.IndexOf(item);
        }

        public void Insert(int index, EpisodeBO item)
        {
            BindEpisode(item);
            Episodes.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            Episodes.RemoveAt(index);
        }

        public EpisodeBO this[int index]
        {
            get
            {
                return Episodes[index];
            }
            set
            {
                BindEpisode(value);
                Episodes[index] = value;
            }
        }

        #endregion

        #region ICollection<EpisodeBO> Members

        public void Add(EpisodeBO item)
        {
            BindEpisode(item);
            Episodes.Add(item);
        }

        public void Clear()
        {
            Episodes.Clear();
        }

        public bool Contains(EpisodeBO item)
        {
            return Episodes.Contains(item);
        }

        public void CopyTo(EpisodeBO[] array, int arrayIndex)
        {
            Episodes.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return Episodes.Count; }
        }

        public bool IsReadOnly
        {
            get { return Episodes.IsReadOnly; }
        }

        public bool Remove(EpisodeBO item)
        {
            return Episodes.Remove(item);
        }

        #endregion

        #region IEnumerable<EpisodeBO> Members

        public IEnumerator<EpisodeBO> GetEnumerator()
        {
            return Episodes.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
