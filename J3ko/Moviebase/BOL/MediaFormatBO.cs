﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Moviebase.BOL
{
    public class MediaFormatBO
    {
        public int MediaFormatId { get; set; }
        public string Description { get; set; }
    }
}
