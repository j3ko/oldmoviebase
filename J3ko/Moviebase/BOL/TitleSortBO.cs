﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Moviebase.BOL
{
    public enum SortOrder
    {
        TitleNameAlphaAsc,
        TitleNameAlphaDesc,
        ReleasedDateAsc,
        ReleasedDateDesc,
        AddedDateAsc,
        AddedDateDesc,
    }
    public class TitleSortBO : ICodeBO
    {
        public SortOrder Sort { get; set; }
        public string Description { get; set; }

        #region ICodeBO Members

        public string Key
        {
            get { return ((int)Sort).ToString(); }
        }

        public string Value
        {
            get { return Description; }
        }

        #endregion
    }
}
