﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Moviebase.BOL
{
    public class AltTitleNameBO
    {
        public int TitleId { get; internal set; }
        public string Name { get; set; }

        #region object overrides
        public override string ToString()
        {
            return Name;
        }
        public override bool Equals(object obj)
        {
            if (obj is AltTitleNameBO)
                return ((AltTitleNameBO)obj).Name == this.Name;
            return false;

        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + Name.GetHashCode();
            return hash;
        }
        #endregion
    }
}
