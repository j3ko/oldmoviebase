﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;

namespace J3ko.Moviebase.BOL
{
    public class MediaBO
    {
        public int Id { get; internal set; }
        public string Name { get; set; }
        private MediaFormatBO _format;
        public MediaFormatBO Format 
        {
            get
            {
                if (_format == null)
                    _format = new MediaFormatBO();
                return _format;
            }
            set
            {
                _format = value;
            }
        }

        public static MediaBO Create(int id)
        {
            return new MediaBO() { Id = id };
        }

        #region object overrides
        public override string ToString()
        {
            return string.Format("{0}-{1}", Format, Name);
        }
        #endregion
    }
}
