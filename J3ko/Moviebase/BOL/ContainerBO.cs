﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;

namespace J3ko.Moviebase.BOL
{
    public class ContainerBO
    {
        public int Id { get; internal set; }
        public string Name { get; set; }

        public static ContainerBO Create(int id)
        {
            return new ContainerBO() { Id = id };
        }

        #region object overrides
        public override string ToString()
        {
            return Name;
        }
        #endregion
    }
}
