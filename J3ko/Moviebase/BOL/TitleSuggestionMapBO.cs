﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Moviebase.BOL
{
    public class TitleSuggestionMapBO
    {
        public int TitleId { get; set; }
        public int SuggestionId { get; set; }
        public double Distance { get; set; }
    }
}
