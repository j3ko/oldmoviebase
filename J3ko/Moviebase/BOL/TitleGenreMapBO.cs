﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Moviebase.BOL
{
    public class TitleGenreMapBO
    {
        public int TitleId { get; internal set; }
        public int GenreId { get; internal set; }
    }
}
