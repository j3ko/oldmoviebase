﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace J3ko.Moviebase.BOL
{
    public class PersonBO
    {
        public int Id { get; internal set; }
        public string FullName { get; set; }
        public Guid ThumbnailId { get; set; }
        public int TmdbId { get; set; }

        #region object overrides
        public override bool Equals(object obj)
        {
            if (obj is PersonBO)
                return Id == ((PersonBO)obj).Id;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + Id.GetHashCode();
            return hash;
        }

        public override string ToString()
        {
            return FullName;
        }
        #endregion
    }
}
