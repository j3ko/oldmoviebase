﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;

namespace J3ko.Moviebase.BOL
{
    public class SlotBO
    {
        private ContainerBO _container;

        public int Id { get; internal set; }

        public string Label { get; set; }

        public ContainerBO Container
        {
            get
            {
                if (_container == null)
                    _container = new ContainerBO();
                return _container;
            }
            set
            {
                _container = value;
            }
        }

        public bool IsAvailable { get; set; }

        public static SlotBO Create(int id)
        {
            return new SlotBO() { Id = id, Container = ContainerBO.Create(1) };
        }

        #region object overrides
        public override bool Equals(object obj)
        {
            if (obj is SlotBO)
                return Id == ((SlotBO)obj).Id;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + Id.GetHashCode();
            return hash;
        }

        public override string ToString()
        {
            return Label;
        }
        #endregion
    }
}
